declare namespace OL {
	/* tslint:disable */
	/* This file was automatically generated from src/schemas/incident.js.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source and run
	 * $ npm run types
	 */

	export type ID = string;
	export type Type = string;
	export type Name = string;
	export type Description = string;
	export type StartTime = string;
	export type EndTime = string;

	/**
	 * An incident
	 */
	export interface Incident {
		id?: ID;
		type: Type;
		name: Name;
		description?: Description;
		startTime: StartTime;
		endTime?: EndTime;
		[k: string]: unknown;
	}
}
