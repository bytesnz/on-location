declare namespace OL {
	/**
	 * A wrapper object for storing objects, such as events, in a client
	 * store, so the status of the object can be tracked
	 */
	export interface DataObject<T> {
		/** Current status of the object */
		status?: string;
		/** If the object has not been saved
		unsaved?: boolean;
		/** If the object is marked as unseen */
		unseen: boolean;
		/** The local ID of the object */
		localId?: string;
		/** The ID of the object */
		id?: number;
		/** The object */
		data: T;
	}
	export interface RelatedObject {
		relatedTo: string;
	}
}
