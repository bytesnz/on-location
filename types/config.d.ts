import { Server } from 'tile-cacher';

interface MapLayer {
	/// Layer title
	title: string;
	/// Layer type (only 'tile' currently supported)
	type: string;
	/// Leaflet URL template for map tiles
	url: string;
	/// Options to pass to leaflet for layer
	options?: { [key: string]: any };
	/// Caching options
	cache?: Server;
}

interface MapType {
	/// Label of type
	name: string;
	/// Icon to use
	icon: string;
	/**
	 * Text for the icon. If given, it will replace the icon. Only
	 * the first two characters will be used
	 */
	text?: string;
	/// Whether the tooltip should always be displayed
	tooltipPermanent?: boolean;
	/// Direction tooltip should be displayed
	tooltipDirection?: 'left' | 'right' | 'bottom';
	/// zIndex of icon - higher is more on top
	zIndex?: number;
	/// Color of icon
	color?: string;
}

export interface Config {
	/// Name of app. Will be used for page title and in header
	name?: string;
	/// Separator string for using in title
	titleSeparator?: string;
	/// Database config
	database: {
		/// Database to use
		database?: string;
		/// Collections to use to store data (recommend not to set)
		collections?: {
			/// Name of incidents collection
			incidents: string;
			/// Name of events collection
			events: string;
			/// Name of geometries collection
			personnel: string;
			/// Name of groups collection
			groups: string;
			/// Name of relationships collection
			relationships: string;
		};
		/// Graphs to use for relating data (recommend not to set)
		graphs?: {
			/// Name of relationships graph
			relationships: string;
		};
		/// Address of server
		host?: string;
		/// Database username
		username: string;
		/// Database password
		password: string;
	};
	/// Map configuration
	map?: {
		/// Minimum zoom level for the map, default 0
		minZoom?: number;
		/// Maximum zoom level for the map, default 20
		maxZoom?: number;
		/// Initial center of map if nothing else is being shown ([lat, lon])
		initalCenter?: [number, number];
		/// Initial zoom of the map if nothing else is being shown
		initialZoom?: number;
		/** Initial bounds of the map if nothing else is being shown (will be
		 *  ignored if initialZoom and initalCenter set)
		 *  ([[min_lat, min_lon], [max_lat, max_lon]])
		 */
		initialBounds?: [[number, number], [number, number]];
		/// Ordered coordinate format IDs to include on the map
		coordinateFormats?: Array<'nztm' | 'dms' | 'dmm' | 'ddd'>;
		/// Layers to be displayed on map and cache configuration
		layers?: Array<MapLayer>;
		/// Additional map layers that will be added to the default layers
		additionalLayers?: Array<MapLayer>;
		/// Predefined marker types
		types?: {
			[id: string]: MapType;
		};
		/// Additional marker types that will be added to the default marker types
		additionalTypes?: {
			[id: string]: MapType;
		};
		/// Position marker configuration
		positions?: {
			/// Static color for the position markers
			color?: string;
			/// Position marker icon color
			iconColor?: string;
			/// HSL color array for new positions
			newColor?: [number, number, number];
			/// HSL color arary for old positions
			oldColor?: [number, number, number];
			/// Time in minutes before a position becomes old
			oldTimeout?: number;
			/// Default icon for position markers
			icon?: string;
			/// Size for position markers
			size?: number;
			/// Custom config for use for callsigns
			callsignIcons?: {
				[callsign: string]: {
					/// Custom icon to use for callsign position marker
					icon?: string;
					/**
					 * Custom text to use for callsign marker. If given, text will
					 * replace the icon in the position marker. Only the first two
					 * characters will be used.
					 */
					text?: string;
				};
			};
		};
	};
	/// Datalists used in forms
	lists?: {
		/// Age preset values
		ageRanges?: string[];
	};
}
