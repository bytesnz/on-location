declare namespace OL {
	/* tslint:disable */
	/* This file was automatically generated from src/schemas/asset.js.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source and run
	 * $ npm run types
	 */

	export type ID = string;
	export type Name = string;
	export type Type = string[];

	/**
	 * An asset
	 */
	export interface Asset {
		id?: ID;
		name?: Name;
		type: Type;
		[k: string]: unknown;
	}
}
