declare namespace OL {
	/* tslint:disable */
	/* This file was automatically generated from src/schemas/geometry.js.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source and run
	 * $ npm run types
	 */

	export type ID = string;
	export type Type = string;
	export type Name = string;
	export type Description = string;

	/**
	 * A geometry object
	 */
	export interface Geometry {
		id?: ID;
		type?: Type;
		name?: Name;
		description?: Description;
		geometry: unknown;
		[k: string]: unknown;
	}
}
