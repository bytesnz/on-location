declare namespace OL {
	/* tslint:disable */
	/* This file was automatically generated from src/schemas/incidentType.js.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source and run
	 * $ npm run types
	 */

	export type IncidentID = string;
	export type Name = string;
	export type Description = string;
	export type StartTime = string;
	export type EndTime = string;

	/**
	 * Phase of an Incident
	 */
	export interface IncidentPhase {
		incident: IncidentID;
		name: Name;
		description?: Description;
		startTime: StartTime;
		endTime?: EndTime;
	}
}
