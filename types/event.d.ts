declare namespace OL {
	/* tslint:disable */
	/* This file was automatically generated from src/schemas/event.js.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source and run
	 * $ npm run types
	 */

	/**
	 * An event
	 */
	export type Event = Event1 & Event2;
	export type ID = string;
	export type Type = string;
	export type Event2 =
		| {
				type?: 'log';
				details: string;
				[k: string]: unknown;
		  }
		| (
				| {
						type?: 'radio';
						channel: Channel;
						transmitter: Transmitter;
						transmitterRadio?: TransmitterRadioID;
						receiver: Receiver;
						receiverRadio?: ReceiverRadioID;
						details?: Details;
						[k: string]: unknown;
				  }
				| {
						type?: 'telephone';
						direction: 'incoming' | 'outgoing';
						number: TelephoneNumber;
						communicator?: Communicator;
						personnel?: Personnel;
						details?: Details1;
						[k: string]: unknown;
				  }
				| {
						type?: 'otherCommunication';
						direction: 'incoming' | 'outgoing';
						communicator: Communicator1;
						personnel: Personnel1;
						details?: Details2;
						[k: string]: unknown;
				  }
				| {
						type?: 'tasking';
						assignee: Assignee;
						deadline?: Deadline;
						details?: Details3;
						[k: string]: unknown;
				  }
				| {
						type?: 'patient';
						carer?: CarerS;
						identifier?: Identifier;
						name?: Name;
						age?: Age;
						access?: Access;
						signs?: Signs;
						symptoms?: Symptoms;
						mechanismOfInjuries?: MechanismOfInjuries;
						treatments?: Treatments;
						allergies?: Treatments1;
						medicalConditions?: MedicalConditions;
						medications?: Medications;
						lastIntake?: LastIntake;
						[k: string]: unknown;
				  }
		  );
	export type Channel = string;
	export type Transmitter = string;
	export type TransmitterRadioID = string;
	export type Receiver = string;
	export type ReceiverRadioID = string;
	export type Details = string;
	export type TelephoneNumber = string;
	export type Communicator = string;
	export type Personnel = string;
	export type Details1 = string;
	export type Communicator1 = string;
	export type Personnel1 = string;
	export type Details2 = string;
	export type Assignee = string;
	export type Deadline = string;
	export type Details3 = string;
	export type CarerS = unknown[];
	export type Identifier = string;
	export type Name = string;
	export type Age = string;
	/**
	 * Access to patient
	 */
	export type Access = string;
	export type Details4 = string;
	export type FractureType = 'closed' | 'open' | 'complex';
	export type Location = string;
	export type BleedingType = 'capillary' | 'venous' | 'arterial';
	export type InjuryType = 'open' | 'closed';
	export type Location1 = string;
	export type BleedingType1 = 'capillary' | 'venous' | 'arterial';
	export type Location2 = string;
	export type BleedingType2 = 'capillary' | 'venous' | 'arterial';
	export type Type1 = 'first' | 'second' | 'third' | 'fourth';
	export type Location3 = string;
	export type Signs = (
		| {
				type?: string;
				details: Details4;
				[k: string]: unknown;
		  }
		| {
				type?: 'fracture';
				injuryType: FractureType;
				location: Location;
				bleeding?: BleedingType;
				[k: string]: unknown;
		  }
		| {
				type?: 'headSpineInjury';
				injuryType: InjuryType;
				location?: Location1;
				bleeding?: BleedingType1;
				[k: string]: unknown;
		  }
		| {
				type?: 'softTissueInjury';
				location: Location2;
				bleeding?: BleedingType2;
				[k: string]: unknown;
		  }
		| {
				type?: 'burn';
				injuryType: Type1;
				location?: Location3;
				[k: string]: unknown;
		  }
	)[];
	export type Symptom = string;
	export type Symptoms = {
		details?: Symptom;
		[k: string]: unknown;
	}[];
	export type MechanismOfInjuries = string;
	export type Time = string;
	export type Administerer = string;
	export type Details5 = string;
	export type Treatments = {
		time: Time;
		administerer?: Administerer;
		details: Details5;
		[k: string]: unknown;
	}[];
	export type Treatments1 = string[];
	export type MedicalConditions = string[];
	export type Medications = string[];
	export type LastIntake = string;

	export interface Event1 {
		id?: ID;
		type: Type;
		[k: string]: unknown;
	}
}
