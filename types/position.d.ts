declare namespace OL {
	/* tslint:disable */
	/* This file was automatically generated from src/schemas/position.js.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source and run
	 * $ npm run types
	 */

	export type ID = string;
	export type Subject = string;
	export type Source = string;
	export type Time = string;
	export type Latitude = number;
	export type Longitude = number;
	export type Altitude = number;
	export type Accuracy = number;
	export type AltitudeAccuracy = number;
	export type Bearing = number;
	export type Speed = number;

	/**
	 * Position of an asset or person at a specific time
	 */
	export interface Position {
		id?: ID;
		subject: Subject;
		source: Source;
		time: Time;
		latitude: Latitude;
		longitude: Longitude;
		altitude?: Altitude;
		accuracy?: Accuracy;
		altitudeAccuracy?: AltitudeAccuracy;
		heading?: Bearing;
		speed?: Speed;
		[k: string]: unknown;
	}
}
