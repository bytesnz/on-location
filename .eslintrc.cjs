module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
	plugins: ['svelte3', '@typescript-eslint'],
	ignorePatterns: ['*.cjs'],
	overrides: [{ files: ['*.svelte'], processor: 'svelte3/svelte3' }],
	settings: {
		'svelte3/typescript': () => require('typescript')
	},
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2020
	},
	env: {
		browser: true,
		es2017: true,
		node: true
	},
	rules: {
		'no-console': [1, { allow: ['warn', 'error'] }],
		'no-debugger': 1,
		'no-warning-comments': [
			1,
			{ terms: ['xxx', 'todo', 'fixme', 'todo!!!'], location: 'anywhere' }
		],
		'require-jsdoc': 1
	}
};
