# Development

on-location uses the following technologies:

- [NodeJS][]
- [ArangoDB][]
- [SvelteKit][]
- [ws][] - web sockets
- [json-ld][]
- [GeoJSON][]

In addition, it also using the following development tools:

- [Gitlab][]
- [git][]
- [Husky][]
- [lint-staged][]
- [Prettier][]
- [ESLint][]
- [Typescript][]

## Development Process

The process that will be followed for fixing a bug/creating a new feature will
be:

1. A [Gitlab Issue](https://gitlab.com/bytesnz/on-location/issues)
   is created for the bug/work package, detailing the requirements of what
   needs to be done.
1. The issue is assigned a priority and discussed until is is fully specified
   and ready to be developed.
1. A [Gitlab Merge Request](https://gitlab.com/bytesnz/on-location/merge_requests)
   is with the branch named after the issue number and two or three word
   description, and marked with "WIP:" in the merge request title to symbolise
   it is not yet ready to be merged
1. Work is carried out on the branch, commiting and pushing as required
1. Once work is complete, the "WIP:" is removed from the merge request title
   and the branch will be reviewed.
1. Once reviewed for both functionality and code quality, the merge request
   will be approved
1. The merge request will then be merged into dev, ready for the next
   version release.

## Development Set Up

To set up development environment:

1. Ensure you have [NodeJS][] installed
1. [Clone](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#_git_cloning)
   the [git repository](https://gitlab.com/bytesnz/on-location.git)
1. Go into to cloned folder
1. Install the NPM dependencies
1. Install [Husky][] scripts
   ```
   npx husky install
   ```
1. Start the development server using the dev script
   ```
   npm run dev
   ```
   or
   ```
   yarn run dev
   ```

[nodejs]: https://nodejs.org
[sveltekit]: https://kit.svelte.dev
[ws]: https://github.com/websockets/ws
[arangodb]: https://www.arangodb.com/
[json-ld]: https://json-ld.org/
[geojson]: https://geojson.org/
[gitlab]: https://gitlab.com/
[git]: https://git-scm.com/
[husky]: https://typicode.github.io/husky/
[lint-staged]: https://github.com/okonet/lint-staged
[Prettier]: https://prettier.io/
[ESLint]: https://eslint.org/
[Typescript]: https://www.typescriptlang.org/
