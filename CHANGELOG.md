# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.6.0] - 2024-11-27

### Added

- Ability to bulk edit and select geometries before importing from a file
- Landing zone icon
- Cancel button to unsaved events to remove the event
- Ability to download geometries for event as GPX

## Changed

- Ordering of events to order events as they come in

## [0.5.8] - 2024-04-17

### Added

- Allowed tooltips to be permanent at certain zoom locations

### Fixed

- Removed navigate to link from marker tooltip (still in popup)

## [0.5.7] - 2024-04-10

### Fixed

- Datetime clearing when entering a date
- Drag and drop for map files
- Fix NaN map LineString distance calculations
- Typing for config file

### Added

- Ability to add additional map layer and marker types by using the
  additionalLayers and additionalType map config properties

## [0.5.6] - 2023-12-09

### Fixed

- Marking personnel/equipment as stooddown/unassigned

### Changed

- Made assignee on tasking multiple and made labels more useful

## [0.5.5] - 2023-07-29

### Fixed

- Added missing quotes on string literal

## [0.5.4] - 2023-07-24

### Changed

- Fix patterns on fields
- Change AVPU to select
- Add \* to required field labels to make requirement more obvious (#126)
- Add radio id to radio id unknown error
- Reordered and added labels to some patient fields
- Split soft tissue injury into sprain/strain and cut/laceration

### Added

- Add SpO2 to patient observations (#125)
- Add navigate link to geometry point popup

## [0.5.3] - 2023-06-01

### Changed

- Incident list to show times in local time and format

## [0.5.1] - 2023-05-02

### Added

- The ability to location poll assigned radios that aren't in another asset

## [0.5.0] - 2023-04-12

### Added

- Ability to assign personnel and assets to an event
- Ability to poll assets position using a map27 radio service
- Ability to set marker from current position

### Fixed

- Symptoms field in Chromium-based browsers
- Fixed ordering of incidents
- Issue of logs being overwritten while still editing them

### Changed

- Minor dependency upgrades
- Telephone log
  - Communicator field is now a personnel field
  - Telephone number is now optional (as if communicating with personnel will
    have their number
  - Added ability to have multiple personnel (for conference phone calls)
- Patient log
  - Added details to symptom
  - Reworked treatment so details of treatment are first, time is autopopulated
    and administerer is optional
- Add a temporary log save button as workaround to saving as won't save if
  stay focussed in an input

## [0.4.6] - 2023-02-26

### Fixed

- NZTM2000 parser and formatter

## [0.4.5] - 2022-12-08

### Fixed

- Change patient first aider field back to personnel
- Incidents now ordered correctly on load

### Changed

- Change some fields in logs to be select inputs (limited to specific values)

## [0.4.4] - 2022-12-05

### Changed

- Positions markers so they are above all other markers by default (+1000)
- Name of events to logs and a log to a note
- Patient log so there can be multiple first aiders

## [0.4.3] - 2022-11-22

### Fixed

- Issue causing server to crash. Issue was caused by the request headers
  object now not being included on a spread into a new object (possibly due
  to it becoming a getter

## [0.4.2] - 2022-11-22

### Fixed

- Incidents list so it scrolls if it is larger than the screen

### Changed

- Incident list so newest incidents are shown first

## [0.4.1] - 2022-11-22

### Added

- Marshal and Cordon marker types
- Added additional icons for markers including: barcode, assignment, run-fast,
  bed and traffic-cone

### Fixed

- Error causing fatal error when going to web app with a hash in the URL

## [0.4.0] - 2022-09-13

### Added

- Support for [location-share](https://gitlab.com/bytesnz/location-share)
  endpoints
- Add link to share position in operation user menu

### Fixed

- Hide stats popup when start dragging geometries

## [0.3.0] - 2022-09-01

### Added

- Geometry stats with hovering, creating and clicking on map geometries
- The ability to unselect geometries by pressing Esc

### Changed

- The positions endpoint now by default only returns positions received in the
  last 24 hours

### Fixed

- Parsing of coordinates in DMS and DMm formats
- Creating a new geometry now unselects any selected geometries first

## [0.2.1] - 2022-08-11

### Fixed

- Server not starting with no config.json file due to missing value check

## [0.2.0] - 2022-08-08

### Added

- Positions collection for recording asset positions
- Asset position markers to the map
- Assets and asset types API
- Add ability to configure text characters to be used for geometry markers
  instead of an icon

## [0.1.7] - 2022-07-14

### Fixed

- Changing of LineString `track` flag as was not working

## [0.1.6] - 2022-07-13

### Added

- Ability to drag and drop KML, GPX and GeoJSON files over map to import
  geometries for the current incident/event

## [0.1.5] - 2022-07-08

### Added

- `minZoom` and `maxZoom` map configuration to manually set global min and max
  zoom levels for map

### Changed

- Increase maximum zoom level for default maps
- Added more spacing around form elements to make it easier to look at
- Fix size and position of incident menu on small screens
- Fix incident preload issue, so will navigate directly to an incident
  correctly
- Fixed issues around patient signs (not saving as not correctly validated)
- Add requirement of details for signs with no other fields
- Changed sign form to update as soon as a sign is selected

## [0.1.4] - 2022-06-19

### Fixed

- Resubscribing to events when web socket closes

## [0.1.3] - 2022-06-17

### Added

- Background color to invalid and unsaved events

### Changed

- Some edit field labels to make them clearer

### Fixed

- Fixed label on name field
- Add autocomplete to type field

## [0.1.2] - 2022-06-11

### Fixed

- Fix error drawing geometries (`addGeo` scope issue)

## [0.1.1] - 2022-06-10

### Fixed

- Cache URLs being `localhost:3001` due to `NODE_ENV=production` not being
  set on the docker containers
- Calculation of geometries bounds for LineStrings and Polygons
- Tile caching with tile-cacher upgrade
- Errors during LineString editing
- Creation of geometries after incident creation

### Changed

- Geometries toolbar isn't shown until an incident is selected

## [0.1.0] - 2022-06-03

### Added

- Combined (ArangoDB and on-location) docker image
- Creation of geometries for incidents and event
- Built-in [tile-cacher](https://gitlab.com/bytesnz/tile-cacher/) for caching
  map tiles

## Changed

- Tidied events list
- Finished summaries and adding ability to collapse events in list
- Reordered list so newest events are at top
- Cleaned displaying of event dates and times
- Tidied event form
- Tidied header so it displays better on small screens
- Tidied other styling

## [0.0.1] - 2022-05-20

### Added

- Basic event functionality
- Basic map
- Basic incident creation

[0.6.0]: https://gitlab.com/bytesnz/on-location/compare/v0.5.7...v0.6.0
[0.5.7]: https://gitlab.com/bytesnz/on-location/compare/v0.5.6...v0.5.7
[0.5.6]: https://gitlab.com/bytesnz/on-location/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/bytesnz/on-location/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/bytesnz/on-location/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/bytesnz/on-location/compare/v0.5.1...v0.5.3
[0.5.1]: https://gitlab.com/bytesnz/on-location/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/bytesnz/on-location/compare/v0.4.6...v0.5.0
[0.4.6]: https://gitlab.com/bytesnz/on-location/compare/v0.4.5...v0.4.6
[0.4.5]: https://gitlab.com/bytesnz/on-location/compare/v0.4.4...v0.4.5
[0.4.4]: https://gitlab.com/bytesnz/on-location/compare/v0.4.3...v0.4.4
[0.4.3]: https://gitlab.com/bytesnz/on-location/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/bytesnz/on-location/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/bytesnz/on-location/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/bytesnz/on-location/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/bytesnz/on-location/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/bytesnz/on-location/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/bytesnz/on-location/compare/v0.1.7...v0.2.0
[0.1.7]: https://gitlab.com/bytesnz/on-location/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/bytesnz/on-location/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/bytesnz/on-location/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/bytesnz/on-location/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/bytesnz/on-location/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/bytesnz/on-location/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bytesnz/on-location/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/on-location/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/bytesnz/on-location/tree/v0.0.1
