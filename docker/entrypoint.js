/*eslint-disable no-console*/
import { env, exit } from 'process';
import { writeFileSync, readFileSync, statSync } from 'fs';
import { spawn } from 'child_process';

const arango = spawn('/entrypoint.sh', ['arangod'], {
	cwd: '/',
	env: {
		...env,
		ARANGO_RANDOM_ROOT_PASSWORD: '1'
	}
});

let rootPassword;

let arangoBuffer = '';
arango.stdout.on('data', (data) => {
	arangoBuffer += data.toString();

	const lines = arangoBuffer.split('\n');

	while (lines.length > 1) {
		const line = lines.shift();
		console.log('arango: ' + line);
		const passwordMatch = line.match(/^GENERATED ROOT PASSWORD: (.*)$/);
		if (passwordMatch) {
			rootPassword = passwordMatch[1];
			console.log(`got root password`);
		}
		if (line.match(/ArangoDB .* is ready for business/)) {
			console.log('ArangoDB is ready. Starting on-location');
			startOnLocation();
		}
	}

	arangoBuffer = lines[0];
});

let arangoErrorBuffer = '';
arango.stderr.on('data', (data) => {
	arangoErrorBuffer += data.toString();

	const lines = arangoErrorBuffer.split('\n');

	while (lines.length > 1) {
		const line = lines.shift();
		console.error('arango: ' + line);
	}

	arangoErrorBuffer = lines[0];
});

arango.on('close', (code) => {
	console.log(`Arango exited with code ${code}. Exiting`);
	exit(code);
});

const startOnLocation = () => {
	if (rootPassword) {
		let config;
		try {
			statSync('config.json');
			config = JSON.parse(readFileSync('config.json'));
		} catch (e) {
			if (e.code !== 'ENOENT') {
				console.error('Error trying to read config.json file', e);
				exit(1);
			}
			config = {};
		}
		writeFileSync(
			'config.json',
			JSON.stringify(
				{
					...config,
					database: {
						...(config.database || {}),
						username: 'root',
						password: rootPassword
					}
				},
				null,
				2
			)
		);
	}

	const onLocation = spawn('node', ['src/server/index.js'], {
		env: {
			...env,
			PROTOCOL_HEADER: env.PROTOCOL_HEADER || 'x-forwarded-proto'
		}
	});

	let onLocationBuffer = '';
	onLocation.stdout.on('data', (data) => {
		onLocationBuffer += data.toString();

		const lines = onLocationBuffer.split('\n');

		while (lines.length > 1) {
			const line = lines.shift();
			console.log('onLocation: ' + line);
		}

		onLocationBuffer = lines[0];
	});

	let onLocationErrorBuffer = '';
	onLocation.stderr.on('data', (data) => {
		onLocationErrorBuffer += data.toString();

		const lines = onLocationErrorBuffer.split('\n');

		while (lines.length > 1) {
			const line = lines.shift();
			console.error('onLocation: ' + line);
		}

		onLocationErrorBuffer = lines[0];
	});

	onLocation.on('close', (code) => {
		console.log(`on-location exited with code ${code}. Exiting`);
		exit(code);
	});
};
