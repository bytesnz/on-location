FROM alpine AS build

COPY package.json ./
COPY yarn.lock ./

RUN apk add --no-cache nodejs yarn
RUN yarn install --prod --pure-lockfile

FROM arangodb
EXPOSE 8529
EXPOSE 3000

ENV NODE_ENV=production

RUN mkdir /app
WORKDIR /app

RUN apk add --no-cache nodejs

COPY package.json ./
COPY --from=build node_modules ./node_modules/

COPY src/server ./src/server/
COPY src/schemas ./src/schemas/
COPY src/lists ./src/lists/
COPY src/errors.js ./src/
COPY src/lib/events.js ./src/lib/
COPY src/lib/date.js ./src/lib/
COPY src/lib/utils.js ./src/lib/
COPY src/lib/coordinates.js ./src/lib/
COPY static ./static/
COPY build ./build/

COPY docker/entrypoint.js ./

#USER node

CMD [ "node", "entrypoint.js" ]
