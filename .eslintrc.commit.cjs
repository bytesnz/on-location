module.exports = {
	root: true,
	extends: ['./.eslintrc.cjs'],
	rules: {
		'no-console': [2, { allow: ['warn', 'error'] }],
		'no-debugger': 2,
		'no-warning-comments': [2, { terms: ['xxx', 'todo!!!'], location: 'anywhere' }],
		'require-jsdoc': 2
	}
};
