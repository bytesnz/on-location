module.exports = {
	'{src/README.md,CHANGELOG.md,config.example.js,package.json,types/config.d.ts,src/server/config.js}':
		[() => 'repo-utils/mdFileInclude.cjs src/README.md README.md', 'git add README.md'],
	'*.{css,md,json}': ['prettier --write'],
	'*.{js,svelte,ts,json}': ['prettier --write', 'eslint -c .eslintrc.commit.cjs --fix']
};
