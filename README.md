# on-location 0.6.0

Real time event management system containing mapping, logging and asset
tracking.

[![pipeline status](https://gitlab.com/bytesnz/on-location/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/on-location/commits/main)
[![license](https://bytes.nz/b/on-location/custom?color=yellow&name=license&value=AGPL)](https://gitlab.com/bytesnz/on-location/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/on-location/custom?color=yellowgreen&name=development+time&value=~180+hours)](https://gitlab.com/bytesnz/on-location/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/on-location/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/on-location/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/on-location/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

This project is just starting out and looking for contributions from anyone
with time or an interest in event management.

Check out the
[Initial Design](https://gitlab.com/bytesnz/on-location/-/issues/2) issue or
other [issues](https://gitlab.com/bytesnz/on-location/-/issues) and the
[development board](https://gitlab.com/bytesnz/on-location/-/boards) for
development progress. Feel free to post errors or feature requests to the
project [issue tracker](https://gitlab.com/bytesnz/on-location/-/issues/) or
[email](mailto:contact-project+bytesnz-on-location-33723973-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/on-location/-/issues/?issue[confidential]=true)**

For those interested in technology, the following technologies are going to
be used:

- [node][]
- [ArangoDB][]
- [SvelteKit][]
- [ws][] - web sockets
- [json-ld][]
- [GeoJSON][]

## Installation

### Docker Container

There is a all-in-one docker container that contains both [ArangoDB][] and
on-location, which is based off the
[ArangoDB docker container](https://hub.docker.com/_/arangodb), initiating
the database with a random password if the database does not already exist.
The database password will be written to the docker log.

```shell
docker run -d --name on-location -p 3000:3000 -p 8529:8529 -v /tmp/arangodb:/var/lib/arangdb3 registry.gitlab.com/bytesnz/on-location
```

### Configuration

Basic configuration including map layers and icon type, and database
configuration can be changed by setting them in a _config.json_ file. The
contents should be a JSON object that meets the following Typescript interface

<details>
<summary>Typescript config interface</summary>
```typescript
interface MapLayer {
  /// Layer title
  title: string;
  /// Layer type (only 'tile' currently supported)
  type: string;
  /// Leaflet URL template for map tiles
  url: string;
  /// Options to pass to leaflet for layer
  options?: { [key: string]: any };
  /// Caching options
  cache?: Server;
}

interface MapType {
  /// Label of type
  name: string;
  /// Icon to use
  icon: string;
  /**
   * Text for the icon. If given, it will replace the icon. Only
   * the first two characters will be used
   */
  text?: string;
  /// Whether the tooltip should always be displayed
  tooltipPermanent?: boolean;
  /// Direction tooltip should be displayed
  tooltipDirection?: 'left' | 'right' | 'bottom';
  /// zIndex of icon - higher is more on top
  zIndex?: number;
  /// Color of icon
  color?: string;
}

export interface Config {
  /// Name of app. Will be used for page title and in header
  name?: string;
  /// Separator string for using in title
  titleSeparator?: string;
  /// Database config
  database: {
    /// Database to use
    database?: string;
    /// Collections to use to store data (recommend not to set)
    collections?: {
      /// Name of incidents collection
      incidents: string;
      /// Name of events collection
      events: string;
      /// Name of geometries collection
      personnel: string;
      /// Name of groups collection
      groups: string;
      /// Name of relationships collection
      relationships: string;
    };
    /// Graphs to use for relating data (recommend not to set)
    graphs?: {
      /// Name of relationships graph
      relationships: string;
    };
    /// Address of server
    host?: string;
    /// Database username
    username: string;
    /// Database password
    password: string;
  };
  /// Map configuration
  map?: {
    /// Minimum zoom level for the map, default 0
    minZoom?: number;
    /// Maximum zoom level for the map, default 20
    maxZoom?: number;
    /// Initial center of map if nothing else is being shown ([lat, lon])
    initalCenter?: [number, number];
    /// Initial zoom of the map if nothing else is being shown
    initialZoom?: number;
    /** Initial bounds of the map if nothing else is being shown (will be
     *  ignored if initialZoom and initalCenter set)
     *  ([[min_lat, min_lon], [max_lat, max_lon]])
     */
    initialBounds?: [[number, number], [number, number]];
    /// Ordered coordinate format IDs to include on the map
    coordinateFormats?: Array&lt;'nztm' | 'dms' | 'dmm' | 'ddd'&gt;;
    /// Layers to be displayed on map and cache configuration
    layers?: Array&lt;MapLayer&gt;;
    /// Additional map layers that will be added to the default layers
    additionalLayers?: Array&lt;MapLayer&gt;;
    /// Predefined marker types
    types?: {
      [id: string]: MapType;
    };
    /// Additional marker types that will be added to the default marker types
    additionalTypes?: {
      [id: string]: MapType;
    };
    /// Position marker configuration
    positions?: {
      /// Static color for the position markers
      color?: string;
      /// Position marker icon color
      iconColor?: string;
      /// HSL color array for new positions
      newColor?: [number, number, number];
      /// HSL color arary for old positions
      oldColor?: [number, number, number];
      /// Time in minutes before a position becomes old
      oldTimeout?: number;
      /// Default icon for position markers
      icon?: string;
      /// Size for position markers
      size?: number;
      /// Custom config for use for callsigns
      callsignIcons?: {
        [callsign: string]: {
          /// Custom icon to use for callsign position marker
          icon?: string;
          /**
           * Custom text to use for callsign marker. If given, text will
           * replace the icon in the position marker. Only the first two
           * characters will be used.
           */
          text?: string;
        };
      };
    };
  };
  /// Datalists used in forms
  lists?: {
    /// Age preset values
    ageRanges?: string[];
  };
}

```
</details>

<details>
<summary>The default config is</summary>
```js
/* @type {import('../../types/config.d.ts').Config} */
const defaultConfig = {
  name: 'on-location',
  titleSeparator: ' : ',
  database: {
    database: 'on-location',
    collections: {
      incidents: 'incidents',
      events: 'events',
      geometries: 'geometries',
      positions: 'positions',
      personnel: 'personnel',
      groups: 'groups',
      assets: 'assets',
      assetTypes: 'assetTypes',
      relationships: 'relationships',
      assetRelationships: 'assetRelationships'
    },
    graphs: {
      relationships: 'relationships',
      assetRelationships: 'assetRelationships'
    }
  },
  map: {
    coordinateFormats: ['dms', 'dmm', 'ddd'],
    layers: [
      {
        title: 'OpenTopoMap',
        type: 'tile',
        url: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
        options: {
          maxNativeZoom: 17,
          maxZoom: 19,
          attribution:
            '&copy; &lt;a href="https://openstreetmap.org/copyright" target="_blank"&gt;OpenStreetMap&lt;/a&gt; contributors, SRTM | Map display: &copy; &lt;a href="https://openstreetmap.org/copyright" target="_blank"&gt;OpenTopoMap&lt;/a&gt;'
        }
      },
      {
        title: 'OpenStreetMap',
        type: 'tile',
        url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        options: {
          maxNativeZoom: 19,
          maxZoom: 19,
          attribution:
            '&copy; &lt;a href="https://openstreetmap.org/copyright" target="_blank"&gt;OpenStreetMap contributors&lt;/a&gt;'
        }
      }
    ],
    types: {
      checkpoint: {
        name: 'Checkpoint',
        icon: 'check',
        tooltipPermanent: 11,
        tooltipDirection: 'right',
        zIndex: 200,
        color: '#ecd104'
      },
      base: {
        name: 'Base',
        icon: 'home',
        tooltipPermanent: 7,
        tooltipDirection: 'bottom',
        zIndex: 200,
        color: '#0000ee'
      },
      patient: {
        name: 'Patient',
        icon: 'patient',
        zIndex: 500,
        color: '#e80606'
      },
      tasking: {
        name: 'Tasking',
        icon: 'tasking',
        zIndex: 400,
        color: '#0000ee'
      },
      feature: {
        name: 'Feature',
        icon: 'signs',
        tooltipPermanent: 13,
        tooltipDirection: 'left',
        zIndex: 100,
        color: '#007b00'
      },
      hazard: {
        name: 'Hazard',
        icon: 'hazard',
        zIndex: 300,
        color: '#ea9903'
      },
      fire: {
        name: 'Fire',
        icon: 'fire',
        zIndex: 300,
        color: '#ea9903'
      },
      flood: {
        name: 'Flood',
        icon: 'flood',
        zIndex: 300,
        color: '#ea9903'
      },
      cordon: {
        name: 'Cordon',
        icon: 'traffic-cone',
        zIndex: 150,
        color: '#f9df03'
      },
      marshal: {
        name: 'Marshal',
        icon: 'user',
        zIndex: 120,
        color: '#8848bd',
        tooltipPermanent: 16,
        size: 0.8
      },
      drinkStation: {
        name: 'Drink Station',
        icon: 'cup',
        zIndex: 120,
        tooltipPermanent: 16,
        color: '#1c7ffd',
        size: 0.8
      },
      landingZone: {
        name: 'Landing Zone',
        text: 'H',
        zIndex: 150,
        tooltipPermanent: 13,
        color: '#1c7ffd'
      }
    },
    positions: {
      newColor: [84, 98, 40],
      oldColor: [0, 0, 40],
      oldTimeout: 10,
      icon: 'crosshairs-valid',
      iconColor: '#fff',
      size: 0.8
    }
  },
  lists: {
    ageRanges: ['&lt;10', '10-19', '20-39', '40-59', '60+'],
    icons: [
      'user',
      'rest',
      'me',
      'telephone',
      'radio',
      'tasking',
      'travelling',
      'incident',
      'event',
      'exercise',
      'log',
      'otherCommunication',
      'patient',
      'check',
      'home',
      'signs',
      'asterisk',
      'hazard',
      'flood',
      'fire',
      'cup',
      'barcode',
      'assignment',
      'run-fast',
      'bed',
      'traffic-cone'
    ],
    eventTypes: [
      { id: 'radio', label: 'Radio Communication' },
```
</details>

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/on-location/-/issues/) or
[email](mailto:contact-project+bytesnz-on-location-33723973-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/on-location/-/issues/?issue[confidential]=true)**

The source is hosted on [Gitlab](https://gitlab.com/bytesnz/on-location.git) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

[node]: https://nodejs.org
[sveltekit]: https://kit.svelte.dev
[ws]: https://github.com/websockets/ws
[arangodb]: https://www.arangodb.com/
[json-ld]: https://json-ld.org/
[geojson]: https://geojson.org/
[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.6.0] - 2024-11-27

### Added

- Ability to bulk edit and select geometries before importing from a file
- Landing zone icon
- Cancel button to unsaved events to remove the event
- Ability to download geometries for event as GPX

## Changed

- Ordering of events to order events as they come in

## [0.5.8] - 2024-04-17

### Added

- Allowed tooltips to be permanent at certain zoom locations

### Fixed

- Removed navigate to link from marker tooltip (still in popup)

## [0.5.7] - 2024-04-10

### Fixed

- Datetime clearing when entering a date
- Drag and drop for map files
- Fix NaN map LineString distance calculations
- Typing for config file

### Added

- Ability to add additional map layer and marker types by using the
  additionalLayers and additionalType map config properties

## [0.5.6] - 2023-12-09

### Fixed

- Marking personnel/equipment as stooddown/unassigned

### Changed

- Made assignee on tasking multiple and made labels more useful

## [0.5.5] - 2023-07-29

### Fixed

- Added missing quotes on string literal

## [0.5.4] - 2023-07-24

### Changed

- Fix patterns on fields
- Change AVPU to select
- Add \* to required field labels to make requirement more obvious (#126)
- Add radio id to radio id unknown error
- Reordered and added labels to some patient fields
- Split soft tissue injury into sprain/strain and cut/laceration

### Added

- Add SpO2 to patient observations (#125)
- Add navigate link to geometry point popup

## [0.5.3] - 2023-06-01

### Changed

- Incident list to show times in local time and format

## [0.5.1] - 2023-05-02

### Added

- The ability to location poll assigned radios that aren't in another asset

## [0.5.0] - 2023-04-12

### Added

- Ability to assign personnel and assets to an event
- Ability to poll assets position using a map27 radio service
- Ability to set marker from current position

### Fixed

- Symptoms field in Chromium-based browsers
- Fixed ordering of incidents
- Issue of logs being overwritten while still editing them

### Changed

- Minor dependency upgrades
- Telephone log
  - Communicator field is now a personnel field
  - Telephone number is now optional (as if communicating with personnel will
    have their number
  - Added ability to have multiple personnel (for conference phone calls)
- Patient log
  - Added details to symptom
  - Reworked treatment so details of treatment are first, time is autopopulated
    and administerer is optional
- Add a temporary log save button as workaround to saving as won't save if
  stay focussed in an input

## [0.4.6] - 2023-02-26

### Fixed

- NZTM2000 parser and formatter

## [0.4.5] - 2022-12-08

### Fixed

- Change patient first aider field back to personnel
- Incidents now ordered correctly on load

### Changed

- Change some fields in logs to be select inputs (limited to specific values)

## [0.4.4] - 2022-12-05

### Changed

- Positions markers so they are above all other markers by default (+1000)
- Name of events to logs and a log to a note
- Patient log so there can be multiple first aiders

## [0.4.3] - 2022-11-22

### Fixed

- Issue causing server to crash. Issue was caused by the request headers
  object now not being included on a spread into a new object (possibly due
  to it becoming a getter

## [0.4.2] - 2022-11-22

### Fixed

- Incidents list so it scrolls if it is larger than the screen

### Changed

- Incident list so newest incidents are shown first

## [0.4.1] - 2022-11-22

### Added

- Marshal and Cordon marker types
- Added additional icons for markers including: barcode, assignment, run-fast,
  bed and traffic-cone

### Fixed

- Error causing fatal error when going to web app with a hash in the URL

## [0.4.0] - 2022-09-13

### Added

- Support for [location-share](https://gitlab.com/bytesnz/location-share)
  endpoints
- Add link to share position in operation user menu

### Fixed

- Hide stats popup when start dragging geometries

## [0.3.0] - 2022-09-01

### Added

- Geometry stats with hovering, creating and clicking on map geometries
- The ability to unselect geometries by pressing Esc

### Changed

- The positions endpoint now by default only returns positions received in the
  last 24 hours

### Fixed

- Parsing of coordinates in DMS and DMm formats
- Creating a new geometry now unselects any selected geometries first

## [0.2.1] - 2022-08-11

### Fixed

- Server not starting with no config.json file due to missing value check

## [0.2.0] - 2022-08-08

### Added

- Positions collection for recording asset positions
- Asset position markers to the map
- Assets and asset types API
- Add ability to configure text characters to be used for geometry markers
  instead of an icon

## [0.1.7] - 2022-07-14

### Fixed

- Changing of LineString `track` flag as was not working

## [0.1.6] - 2022-07-13

### Added

- Ability to drag and drop KML, GPX and GeoJSON files over map to import
  geometries for the current incident/event

## [0.1.5] - 2022-07-08

### Added

- `minZoom` and `maxZoom` map configuration to manually set global min and max
  zoom levels for map

### Changed

- Increase maximum zoom level for default maps
- Added more spacing around form elements to make it easier to look at
- Fix size and position of incident menu on small screens
- Fix incident preload issue, so will navigate directly to an incident
  correctly
- Fixed issues around patient signs (not saving as not correctly validated)
- Add requirement of details for signs with no other fields
- Changed sign form to update as soon as a sign is selected

## [0.1.4] - 2022-06-19

### Fixed

- Resubscribing to events when web socket closes

## [0.1.3] - 2022-06-17

### Added

- Background color to invalid and unsaved events

### Changed

- Some edit field labels to make them clearer

### Fixed

- Fixed label on name field
- Add autocomplete to type field

## [0.1.2] - 2022-06-11

### Fixed

- Fix error drawing geometries (`addGeo` scope issue)

## [0.1.1] - 2022-06-10

### Fixed

- Cache URLs being `localhost:3001` due to `NODE_ENV=production` not being
  set on the docker containers
- Calculation of geometries bounds for LineStrings and Polygons
- Tile caching with tile-cacher upgrade
- Errors during LineString editing
- Creation of geometries after incident creation

### Changed

- Geometries toolbar isn't shown until an incident is selected

## [0.1.0] - 2022-06-03

### Added

- Combined (ArangoDB and on-location) docker image
- Creation of geometries for incidents and event
- Built-in [tile-cacher](https://gitlab.com/bytesnz/tile-cacher/) for caching
  map tiles

## Changed

- Tidied events list
- Finished summaries and adding ability to collapse events in list
- Reordered list so newest events are at top
- Cleaned displaying of event dates and times
- Tidied event form
- Tidied header so it displays better on small screens
- Tidied other styling

## [0.0.1] - 2022-05-20

### Added

- Basic event functionality
- Basic map
- Basic incident creation

[0.6.0]: https://gitlab.com/bytesnz/on-location/compare/v0.5.7...v0.6.0
[0.5.7]: https://gitlab.com/bytesnz/on-location/compare/v0.5.6...v0.5.7
[0.5.6]: https://gitlab.com/bytesnz/on-location/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/bytesnz/on-location/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/bytesnz/on-location/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/bytesnz/on-location/compare/v0.5.1...v0.5.3
[0.5.1]: https://gitlab.com/bytesnz/on-location/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/bytesnz/on-location/compare/v0.4.6...v0.5.0
[0.4.6]: https://gitlab.com/bytesnz/on-location/compare/v0.4.5...v0.4.6
[0.4.5]: https://gitlab.com/bytesnz/on-location/compare/v0.4.4...v0.4.5
[0.4.4]: https://gitlab.com/bytesnz/on-location/compare/v0.4.3...v0.4.4
[0.4.3]: https://gitlab.com/bytesnz/on-location/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/bytesnz/on-location/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/bytesnz/on-location/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/bytesnz/on-location/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/bytesnz/on-location/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/bytesnz/on-location/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/bytesnz/on-location/compare/v0.1.7...v0.2.0
[0.1.7]: https://gitlab.com/bytesnz/on-location/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/bytesnz/on-location/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/bytesnz/on-location/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/bytesnz/on-location/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/bytesnz/on-location/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/bytesnz/on-location/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bytesnz/on-location/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/on-location/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/bytesnz/on-location/tree/v0.0.1

