# on-location <!=package.json version>

Real time event management system containing mapping, logging and asset
tracking.

[![pipeline status](https://gitlab.com/bytesnz/on-location/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/on-location/commits/main)
[![license](https://bytes.nz/b/on-location/custom?color=yellow&name=license&value=AGPL)](https://gitlab.com/bytesnz/on-location/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/on-location/custom?color=yellowgreen&name=development+time&value=~180+hours)](https://gitlab.com/bytesnz/on-location/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/on-location/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/on-location/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/on-location/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

This project is just starting out and looking for contributions from anyone
with time or an interest in event management.

Check out the
[Initial Design](https://gitlab.com/bytesnz/on-location/-/issues/2) issue or
other [issues](https://gitlab.com/bytesnz/on-location/-/issues) and the
[development board](https://gitlab.com/bytesnz/on-location/-/boards) for
development progress. Feel free to post errors or feature requests to the
project [issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

For those interested in technology, the following technologies are going to
be used:

- [node][]
- [ArangoDB][]
- [SvelteKit][]
- [ws][] - web sockets
- [json-ld][]
- [GeoJSON][]

## Installation

### Docker Container

There is a all-in-one docker container that contains both [ArangoDB][] and
on-location, which is based off the
[ArangoDB docker container](https://hub.docker.com/_/arangodb), initiating
the database with a random password if the database does not already exist.
The database password will be written to the docker log.

```shell
docker run -d --name on-location -p 3000:3000 -p 8529:8529 -v /tmp/arangodb:/var/lib/arangdb3 registry.gitlab.com/bytesnz/on-location
```

### Configuration

Basic configuration including map layers and icon type, and database
configuration can be changed by setting them in a _config.json_ file. The
contents should be a JSON object that meets the following Typescript interface

<details>
<summary>Typescript config interface</summary>
```typescript
<!=types/config.d.ts 3->
```
</details>

<details>
<summary>The default config is</summary>
```js
<!=src/server/config.js 11-185>
```
</details>

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

[node]: https://nodejs.org
[sveltekit]: https://kit.svelte.dev
[ws]: https://github.com/websockets/ws
[arangodb]: https://www.arangodb.com/
[json-ld]: https://json-ld.org/
[geojson]: https://geojson.org/
[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/

<!=CHANGELOG.md>
