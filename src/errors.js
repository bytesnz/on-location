/**
 * A custom validation error.
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#es5_custom_error_object
 *
 * @param {string} message Error message
 * @param {object} errors The validation errors
 */
export function ValidationError(message, errors) {
	if (typeof this === 'undefined') {
		throw new Error('Should be called using new');
	}

	var instance = new Error(message);
	instance.name = 'ValidationError';
	instance.code = 'VALIDATION_ERROR';
	instance.details = errors;
	Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
	if (Error.captureStackTrace) {
		Error.captureStackTrace(instance, ValidationError);
	}
	return instance;
}

ValidationError.prototype = Object.create(Error.prototype, {
	constructor: {
		value: Error,
		enumerable: false,
		writable: true,
		configurable: true
	}
});

if (Object.setPrototypeOf) {
	Object.setPrototypeOf(ValidationError, Error);
} else {
	ValidationError.__proto__ = Error;
}
