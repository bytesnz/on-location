export default {
	type: 'object',
	title: 'Service',
	description: 'An on-location service',
	properties: {
		guid: {
			type: 'string',
			title: 'Service Identifier'
		},
		services: {
			type: 'array',
			title: 'Services',
			description: 'Services provided by service provider',
			items: {
				type: 'object',
				oneOf: [
					{
						properties: {
							id: {
								type: 'string'
							},
							status: {
								type: 'string',
								enum: ['available', 'unavailable', 'offline', 'removed']
							},
							radios: {
								type: 'array',
								items: {
									type: 'object',
									properties: {
										id: {
											type: 'string',
											title: 'Radio ID'
										},
										status: {
											type: 'string',
											enum: ['available', 'unavailable', 'offline', 'removed']
										}
									},
									required: ['id', 'status']
								}
							}
						}
					}
				]
			},
			minItems: 1
		}
	},
	required: ['guid', 'services']
};
