export default {
	type: 'object',
	title: 'Assignment',
	description: 'An assignment',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		subject: {
			type: 'string',
			title: 'Subject',
			description: 'Subject being assigned'
		},
		target: {
			type: 'string',
			title: 'Target',
			description: 'Target being assigned to'
		},
		startTime: {
			type: 'string',
			format: 'date-time',
			title: 'Start Time'
		},
		endTime: {
			type: 'string',
			format: 'date-time',
			title: 'End Time'
		},
		state: {
			type: 'string',
			title: 'Assignment State',
			$oneOf: [
				{
					const: 'moving_to_rest',
					title: 'Moving to rest'
				},
				{
					const: 'resting',
					title: 'Resting'
				},
				{
					const: 'moving_from_rest',
					title: 'Moving from rest'
				},
				{
					const: 'moving_to_standdown',
					title: 'Move to standdown'
				}
			]
		}
	},
	required: ['subject', 'target', 'startTime']
};
