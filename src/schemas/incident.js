export default {
	type: 'object',
	title: 'Incident',
	description: 'An incident',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		type: {
			type: 'string',
			title: 'Type'
		},
		name: {
			type: 'string',
			title: 'Name'
		},
		description: {
			type: 'string',
			title: 'Description'
		},
		startTime: {
			type: 'string',
			format: 'date-time',
			title: 'Start Time'
		},
		endTime: {
			type: 'string',
			format: 'date-time',
			title: 'End Time'
		}
	},
	unevaluatedProperties: false,
	required: ['type', 'name', 'startTime']
};
