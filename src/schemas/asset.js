export default {
	type: 'object',
	title: 'Asset',
	description: 'An asset',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		name: {
			type: 'string',
			title: 'Name'
		},
		model: {
			type: 'string',
			title: 'Model'
		},
		type: {
			type: 'array',
			title: 'Type',
			items: {
				type: 'string'
			}
		},
		fixed: {
			type: 'boolean',
			title: 'Fixed',
			description: 'If fixed, asset cannot be temporarily moved'
		},
		favourite: {
			type: 'boolean',
			title: 'Favourite'
		}
	},
	required: ['type']
};
