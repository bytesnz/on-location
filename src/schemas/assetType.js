export default {
	type: 'object',
	title: 'Asset Type',
	description: 'An asset type',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		name: {
			type: 'string',
			title: 'Name'
		},
		schema: {
			type: 'object',
			properties: {
				properties: {
					type: 'object',
					properties: { $ref: 'https://json-schema.org/draft/2020-12/schema' }
				},
				required: {
					type: 'array',
					items: {
						type: 'string'
					}
				}
			},
			required: ['properties']
		}
	},
	required: ['name']
};
