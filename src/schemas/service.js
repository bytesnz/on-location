export default {
	type: 'object',
	title: 'Service',
	description: 'An on-location service',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		guid: {
			type: 'string',
			title: 'Service Identifier'
		},
		name: {
			type: 'string',
			title: 'Name'
		},
		services: {
			type: 'array',
			title: 'Services',
			description: 'Services provided by service provider',
			items: {
				type: 'object',
				oneOf: [
					{
						properties: {
							type: {
								const: 'map27'
							},
							id: {
								type: 'string'
							},
							status: {
								type: 'string',
								enum: ['available', 'unavailable', 'offline', 'removed']
							},
							radios: {
								type: 'array',
								items: {
									type: 'object',
									properties: {
										id: {
											type: 'string',
											title: 'Radio ID'
										},
										type: {
											type: 'string',
											oneOf: [
												{
													const: 'dmr-radio',
													title: 'DMR radio'
												}
											]
										},
										status: {
											type: 'string',
											enum: ['available', 'unavailable', 'offline', 'removed']
										},
										use: {
											type: 'array',
											items: {
												type: 'string',
												oneOf: [
													{
														const: 'locationPolling'
													}
												]
											}
										}
									},
									required: ['id', 'status', 'use']
								}
							}
						}
					}
				]
			},
			minItems: 1
		}
	},
	required: ['guid', 'services']
};
