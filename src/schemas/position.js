export default {
	type: 'object',
	title: 'Position',
	description: 'Position of an asset or person at a specific time',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		subject: {
			type: 'string',
			title: 'Subject'
		},
		source: {
			type: 'string',
			title: 'Source'
		},
		time: {
			type: 'string',
			title: 'Time',
			format: 'date-time'
		},
		latitude: {
			type: 'number',
			title: 'Latitude',
			unit: '°'
		},
		longitude: {
			type: 'number',
			title: 'Longitude',
			unit: '°'
		},
		altitude: {
			type: 'number',
			title: 'Altitude',
			unit: 'm'
		},
		accuracy: {
			type: 'number',
			title: 'Accuracy',
			unit: 'm'
		},
		altitudeAccuracy: {
			type: 'number',
			title: 'Altitude Accuracy',
			unit: 'm'
		},
		heading: {
			type: 'number',
			title: 'Bearing',
			unit: '°'
		},
		speed: {
			type: 'number',
			title: 'Speed',
			unit: 'm/s'
		}
	},
	required: ['subject', 'source', 'time', 'latitude', 'longitude']
};
