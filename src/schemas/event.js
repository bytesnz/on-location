export default {
	type: 'object',
	title: 'Event',
	description: 'An event',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		type: {
			type: 'string',
			title: 'Type'
		}
	},
	oneOf: [
		{
			$comment: 'Point event',
			properties: {
				time: {
					type: 'string',
					format: 'date-time',
					title: 'Time of Event'
				}
			},
			oneOf: [
				{
					properties: {
						type: {
							const: 'log'
						},
						details: {
							type: 'string'
						}
					},
					required: ['details']
				}
			],
			/*discriminator: {
				propertyName: 'type'
			},*/
			required: ['time', 'type']
		},
		{
			$comment: 'Time range event',
			properties: {
				startTime: {
					type: 'string',
					format: 'date-time',
					title: 'Start Time'
				},
				endTime: {
					type: 'string',
					format: 'date-time',
					title: 'End Time'
				}
			},
			oneOf: [
				{
					$comment: 'Radio Event',
					properties: {
						type: {
							const: 'radio'
						},
						channel: {
							type: 'string',
							title: 'Channel'
						},
						transmitter: {
							type: 'string',
							title: 'Transmitter'
						},
						transmitterRadio: {
							type: 'string',
							title: 'Transmitter Radio ID'
						},
						receiver: {
							type: 'string',
							title: 'Receiver'
						},
						receiverRadio: {
							type: 'string',
							title: 'Receiver Radio ID'
						},
						details: {
							type: 'string',
							title: 'Details'
						}
					},
					required: ['channel', 'transmitter', 'receiver']
				},
				{
					$comment: 'Telephone Event',
					properties: {
						type: {
							const: 'telephone'
						},
						direction: {
							type: 'string',
							enum: ['incoming', 'outgoing']
						},
						number: {
							type: 'string',
							title: 'Telephone Number',
							pattern: '^\\+?[0-9- ]+(\\([0-9]+\\)[0-9- ]+)?$'
						},
						communicator: {
							type: 'string',
							title: 'Communicator'
						},
						personnel: {
							type: 'array',
							title: 'Personnel',
							items: {
								type: 'string'
							}
						},
						details: {
							type: 'string',
							title: 'Details'
						}
					},
					required: ['direction', 'personnel']
				},
				{
					$comment: 'Other Communication Event',
					properties: {
						type: {
							const: 'otherCommunication'
						},
						direction: {
							type: 'string',
							enum: ['incoming', 'outgoing']
						},
						medium: {
							type: 'string',
							title: 'Communication Medium',
							description: 'WhatsApp / Verbal etc'
						},
						communicator: {
							type: 'string',
							title: 'Communicator'
						},
						personnel: {
							type: 'string',
							title: 'Personnel'
						},
						details: {
							type: 'string',
							title: 'Details'
						}
					},
					required: ['direction', 'medium', 'communicator', 'personnel']
				},
				{
					$comment: 'Tasking Event',
					properties: {
						type: {
							const: 'tasking'
						},
						assignees: {
							type: 'array',
							title: 'Assignee(s)',
							items: {
								type: 'string',
								title: 'Assignee'
							},
							minItems: 1
						},
						deadline: {
							type: 'string',
							format: 'date-time',
							title: 'Deadline'
						},
						details: {
							type: 'string',
							title: 'Details'
						},
						callsign: {
							type: 'string',
							title: 'Callsigns'
						}
					},
					required: ['assignees']
				},
				{
					$comment: 'Patient Event',
					properties: {
						type: {
							const: 'patient'
						},
						carer: {
							type: 'array',
							title: 'Carer(s)',
							item: {
								type: 'string'
							}
						},
						identifier: {
							type: 'string',
							title: 'Identifier'
						},
						name: {
							type: 'string',
							title: 'Name'
						},
						age: {
							type: 'string',
							title: 'Age'
						},
						access: {
							type: 'string',
							title: 'Access',
							description: 'Access to patient'
						},
						signs: {
							type: 'array',
							title: 'Signs',
							items: {
								type: 'object',
								properties: {
									type: {
										type: 'string',
										title: 'Type'
									},
									details: {
										type: 'string',
										title: 'Details'
									}
								},
								oneOf: [
									{
										properties: {
											type: {
												type: 'string'
											},
											details: {
												type: 'string',
												title: 'Details'
											}
										},
										required: ['details'],
										unevaluatedProperties: false
									},
									{
										properties: {
											type: {
												const: 'fracture'
											},
											injuryType: {
												type: 'string',
												title: 'Fracture Type',
												enum: ['closed', 'open', 'complex']
											},
											location: {
												type: 'string',
												title: 'Location'
											},
											bleeding: {
												$ref: '#/$defs/bleeding'
											}
										},
										required: ['injuryType', 'location']
									},
									{
										properties: {
											type: {
												const: 'headSpineInjury'
											},
											injuryType: {
												type: 'string',
												title: 'Injury Type',
												enum: ['open', 'closed']
											},
											location: {
												type: 'string',
												title: 'Location'
											},
											bleeding: {
												$ref: '#/$defs/bleeding'
											}
										},
										required: ['injuryType']
									},
									{
										properties: {
											type: {
												const: 'sprainStrain',
												title: 'Sprain/Strain'
											},
											location: {
												type: 'string',
												title: 'Location'
											}
										},
										required: ['location']
									},
									{
										properties: {
											type: {
												const: 'cutLaceration',
												title: 'Cut/Laceration'
											},
											location: {
												type: 'string',
												title: 'Location'
											},
											bleeding: {
												$ref: '#/$defs/bleeding'
											}
										},
										required: ['location']
									},
									{
										properties: {
											type: {
												const: 'burn'
											},
											injuryType: {
												type: 'string',
												title: 'Type',
												enum: ['first', 'second', 'third', 'fourth']
											},
											location: {
												type: 'string',
												title: 'Location'
											}
										},
										required: ['injuryType']
									}
									/* Valid under other
									{
										properties: {
											type: {
												const: 'breathingDifficulties'
											}
										}
									},
									{
										properties: {
											type: {
												const: 'heartIssue'
											}
										}
									}
									*/
								],
								required: ['type'],
								unevaluatedProperties: false
							}
						},
						symptoms: {
							type: 'array',
							title: 'Symptoms',
							items: {
								type: 'object',
								properties: {
									type: {
										type: 'string',
										title: 'Symptom'
									},
									details: {
										type: 'string',
										title: 'Details'
									}
								}
							}
						},
						mechanismOfInjuries: {
							type: 'string',
							title: 'Mechanism of Injuries'
						},
						treatments: {
							type: 'array',
							title: 'Treatments',
							items: {
								type: 'object',
								properties: {
									time: {
										type: 'string',
										format: 'date-time',
										title: 'Time'
									},
									administerer: {
										type: 'string',
										title: 'Administerer'
									},
									details: {
										type: 'string',
										title: 'Details'
									}
								},
								required: ['details', 'time']
							}
						},
						observations: {
							type: 'array',
							title: 'Observations',
							items: {
								type: 'object',
								properties: {
									time: {
										type: 'string',
										format: 'date-time',
										title: 'Time'
									},
									administerer: {
										type: 'string',
										title: 'Administerer'
									},
									heartRate: {
										type: 'number',
										title: 'Heart Rate'
									},
									respirationRate: {
										type: 'number',
										title: 'Respiration Rate'
									},
									bloodPressure: {
										type: 'string',
										pattern: '^\\d{2,3}/(\\d{2,3}|[pP])$',
										title: 'Blood Pressure'
									},
									spo2: {
										type: 'number',
										title: 'SpO2',
										minimum: 0,
										maximum: 100
									},
									pupilDiameter: {
										type: 'string',
										pattern: '^\\d/\\d$',
										title: 'Pupil Diameter'
									},
									avpu: {
										type: 'string',
										title: 'AVPU',
										oneOf: [
											{
												const: 'A',
												title: 'Alert'
											},
											{
												const: 'V',
												title: 'Voice'
											},
											{
												const: 'P',
												title: 'Pain'
											},
											{
												const: 'U',
												title: 'Unresponsive'
											}
										]
									}
								},
								required: ['time']
							}
						},
						medicalHistory: {
							type: 'string',
							title: 'Medical History'
						},
						allergies: {
							type: 'array',
							title: 'Treatments',
							items: {
								type: 'string'
							}
						},
						medicalConditions: {
							type: 'array',
							title: 'Medical Conditions',
							items: {
								type: 'string'
							}
						},
						medications: {
							type: 'array',
							title: 'Medications',
							items: {
								type: 'string'
							}
						},
						lastIntake: {
							type: 'string',
							title: 'Last Intake'
						},
						notes: {
							type: 'string',
							title: 'Notes'
						}
					}
				}
			],
			/*discriminator: {
				propertyName: 'type'
			},*/
			required: ['startTime', 'type']
		}
	],
	unevaluatedProperties: false,
	required: ['type'],
	$defs: {
		bleeding: {
			type: 'string',
			title: 'Bleeding Type',
			enum: ['none', 'bruising', 'stopped', 'capillary', 'venous', 'arterial']
		}
	}
};
