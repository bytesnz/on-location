export default {
	type: 'object',
	title: 'Geometry',
	description: 'A geometry object',
	properties: {
		id: {
			type: 'string',
			title: 'ID'
		},
		type: {
			type: 'string',
			title: 'Type'
		},
		name: {
			type: 'string',
			title: 'Name'
		},
		description: {
			type: 'string',
			title: 'Description'
		},
		geometry: {} // TODO Ref GeoJSON
	},
	required: ['geometry']
};
