import { access, readFile } from 'fs/promises';
import { constants } from 'fs';
import { resolve } from 'path';
import { cwd } from 'process';
import Logger from './log.js';
import patientLists from '../lists/patient.js';
const L = Logger('config');
// TODO Change to better ENV
const path = process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:3001/';

/* @type {import('../../types/config.d.ts').Config} */
const defaultConfig = {
	name: 'on-location',
	titleSeparator: ' : ',
	database: {
		database: 'on-location',
		collections: {
			incidents: 'incidents',
			events: 'events',
			geometries: 'geometries',
			positions: 'positions',
			personnel: 'personnel',
			groups: 'groups',
			assets: 'assets',
			assetTypes: 'assetTypes',
			relationships: 'relationships',
			assetRelationships: 'assetRelationships'
		},
		graphs: {
			relationships: 'relationships',
			assetRelationships: 'assetRelationships'
		}
	},
	map: {
		coordinateFormats: ['dms', 'dmm', 'ddd'],
		layers: [
			{
				title: 'OpenTopoMap',
				type: 'tile',
				url: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
				options: {
					maxNativeZoom: 17,
					maxZoom: 19,
					attribution:
						'&copy; <a href="https://openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors, SRTM | Map display: &copy; <a href="https://openstreetmap.org/copyright" target="_blank">OpenTopoMap</a>'
				}
			},
			{
				title: 'OpenStreetMap',
				type: 'tile',
				url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
				options: {
					maxNativeZoom: 19,
					maxZoom: 19,
					attribution:
						'&copy; <a href="https://openstreetmap.org/copyright" target="_blank">OpenStreetMap contributors</a>'
				}
			}
		],
		types: {
			checkpoint: {
				name: 'Checkpoint',
				icon: 'check',
				tooltipPermanent: 11,
				tooltipDirection: 'right',
				zIndex: 200,
				color: '#ecd104'
			},
			base: {
				name: 'Base',
				icon: 'home',
				tooltipPermanent: 7,
				tooltipDirection: 'bottom',
				zIndex: 200,
				color: '#0000ee'
			},
			patient: {
				name: 'Patient',
				icon: 'patient',
				zIndex: 500,
				color: '#e80606'
			},
			tasking: {
				name: 'Tasking',
				icon: 'tasking',
				zIndex: 400,
				color: '#0000ee'
			},
			feature: {
				name: 'Feature',
				icon: 'signs',
				tooltipPermanent: 13,
				tooltipDirection: 'left',
				zIndex: 100,
				color: '#007b00'
			},
			hazard: {
				name: 'Hazard',
				icon: 'hazard',
				zIndex: 300,
				color: '#ea9903'
			},
			fire: {
				name: 'Fire',
				icon: 'fire',
				zIndex: 300,
				color: '#ea9903'
			},
			flood: {
				name: 'Flood',
				icon: 'flood',
				zIndex: 300,
				color: '#ea9903'
			},
			cordon: {
				name: 'Cordon',
				icon: 'traffic-cone',
				zIndex: 150,
				color: '#f9df03'
			},
			marshal: {
				name: 'Marshal',
				icon: 'user',
				zIndex: 120,
				color: '#8848bd',
				tooltipPermanent: 16,
				size: 0.8
			},
			drinkStation: {
				name: 'Drink Station',
				icon: 'cup',
				zIndex: 120,
				tooltipPermanent: 16,
				color: '#1c7ffd',
				size: 0.8
			},
			landingZone: {
				name: 'Landing Zone',
				text: 'H',
				zIndex: 150,
				tooltipPermanent: 13,
				color: '#1c7ffd'
			}
		},
		positions: {
			newColor: [84, 98, 40],
			oldColor: [0, 0, 40],
			oldTimeout: 10,
			icon: 'crosshairs-valid',
			iconColor: '#fff',
			size: 0.8
		}
	},
	lists: {
		ageRanges: ['<10', '10-19', '20-39', '40-59', '60+'],
		icons: [
			'user',
			'rest',
			'me',
			'telephone',
			'radio',
			'tasking',
			'travelling',
			'incident',
			'event',
			'exercise',
			'log',
			'otherCommunication',
			'patient',
			'check',
			'home',
			'signs',
			'asterisk',
			'hazard',
			'flood',
			'fire',
			'cup',
			'barcode',
			'assignment',
			'run-fast',
			'bed',
			'traffic-cone'
		],
		eventTypes: [
			{ id: 'radio', label: 'Radio Communication' },
			{ id: 'telephone', label: 'Telephone Communication' },
			{ id: 'otherCommunication', label: 'Other Communication' },
			{ id: 'patient', label: 'Patient' },
			{ id: 'tasking', label: 'Tasking' },
			{ id: 'log', label: 'Note' }
		]
	},
	server: {
		jsonUploadLimit: '1mb'
	}
};

const configFile = resolve(cwd(), 'config.json');

/* @type { import('../../types/config.d.ts').Config } */
export let config = access(configFile, constants.R_OK)
	.then(() => {
		return readFile(configFile);
	})
	.catch((error) => {
		if (error.code === 'ENOENT') {
			L.log('No config.json file found. Using defaults');
		} else {
			L.error('Error reading config.json', error.message);
		}
		return null;
	})
	.then((file) => {
		try {
			const fileConfig = file ? JSON.parse(file) : {};

			config = {
				...defaultConfig,
				...fileConfig,
				map: {
					...defaultConfig.map,
					types: {
						...defaultConfig.map.types,
						...(fileConfig.map?.additionalTypes || {})
					},
					layers: {
						...defaultConfig.map.layers,
						...(fileConfig.map?.additionalLayers || {})
					},
					...(fileConfig.map || {}),
					positions: {
						...defaultConfig.map.positions,
						...(fileConfig.map?.positions || {})
					}
				},
				lists: {
					...defaultConfig.lists,
					...(fileConfig.lists || {}),
					...patientLists
				},
				database: {
					...defaultConfig.database,
					...(fileConfig.database || {}),
					collections: {
						...defaultConfig.database.collections,
						...(fileConfig.database?.collections || {})
					}
				},
				mapCaches: []
			};
		} catch (error) {
			L.error('Error parsing config.json', error.message);
			return Promise.reject(error);
		}

		if (config.map.types) {
			config.lists.mapTypes = Object.entries(config.map.types).map(([key, data]) => ({
				id: key,
				label: data.name
			}));
		}

		// Extract maps that are cached
		for (let i = 0; i < config.map.layers.length; i++) {
			if (config.map.layers[i].cache) {
				const { cache: cacheConfig, ...layerConfig } = config.map.layers[i];
				config.map.layers[i] = {
					...layerConfig,
					url: path + 'map/cache/' + cacheConfig.id + '/{z}/{x}/{y}.png'
				};
				config.mapCaches.push({
					...cacheConfig,
					url: layerConfig.url
				});
			}
		}

		L.debug('Using config', config);
		return config;
	}); // TODO exit on error

export let clientConfig = config.then((config) => {
	clientConfig = {
		name: config.name,
		titleSeparator: config.titleSeparator,
		map: config.map,
		lists: config.lists
	};
	return clientConfig;
});
