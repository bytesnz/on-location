import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';

/**
 * Update the given service
 *
 * @param {import('../../../types/service')} service
 * @param {} update
 */
export function updateService(service, update) {
	const updated = {
		...service,
		radios: [...service.radios]
	};

	if (update.status) {
		updated.status = update.status;
	}

	if (update.radios) {
		for (const radio of update.radios) {
			const index = updated.radios.findIndex((r) => r.id === radio.id);

			if (index === -1) {
				throw new Error('Unknown radio in MAP27 service (' + radio.id + ')');
			}

			updated.radios[index] = {
				...updated.radios[index],
				...radio
			};
		}
	}

	return updated;
}

const statuses = ['available', 'unavailable', 'offline'];

/**
 * Generate an object with information on the available MAP27 services
 *
 * @param {Services} services List of current available services
 */
export async function makeServiceData(services) {
	const radios = [];
	const ids = [];

	for (const server of services) {
		for (const service of server.services) {
			if (service.type !== 'map27') {
				continue;
			}

			for (const radio of service.radios) {
				if (radio.status === 'removed') {
					continue;
				}

				ids.push(radio.id);
				radios.push({
					radioId: radio.id,
					serviceId: server.guid,
					status:
						statuses[Math.max(statuses.indexOf(service.status), statuses.indexOf(radio.status))],
					prefix: null,
					ident: null,
					use: radio.use
				});
			}
		}
	}

	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	// Get radio info
	const assetsCollection = db.collection(config.database.collections.assets);
	const result = await assetsCollection.documents(ids);

	const radioData = {};

	for (const r of result) {
		radioData[r._id] = r;
	}

	for (const r of radios) {
		r.prefix = radioData[r.radioId].prefix;
		r.ident = radioData[r.radioId].ident;
	}

	return {
		radios
	};
}
