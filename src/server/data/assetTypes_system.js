/* @type {Array<import('../../../types/assetTypes')>} */
export default [
	{
		id: 'asset',
		title: 'Asset',
		schema: {
			properties: {
				name: {
					type: 'string'
				},
				barcode: {
					type: ['number', 'string'],
					title: 'Barcode'
				}
			}
		},
		system: true
	},
	{
		id: 'consumable',
		title: 'Consumable',
		schema: {
			properties: {
				name: {
					type: 'string',
					title: 'Name'
				}
			}
		},
		system: true
	},
	{
		id: 'radio',
		name: 'Radio',
		parent: ['asset'],
		schema: {
			properties: {
				prefix: {
					type: 'number',
					title: 'MPT1327 Prefix'
				},
				ident: {
					type: 'number',
					title: 'MPT1327 Ident'
				}
			}
		},
		system: true
	},
	{
		id: 'dmr-radio',
		name: 'DMR Radio',
		parent: ['radio'],
		schema: {
			properties: {
				dmrId: {
					type: 'number',
					title: 'DMR ID'
				}
			},
			required: ['dmrId']
		},
		system: true
	}
];
