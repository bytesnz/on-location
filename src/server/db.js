import { config } from './config.js';
import { Database } from 'arangojs';
import { CollectionType } from 'arangojs/collection.js';
import assetTypesSystemData from './data/assetTypes_system.js';
import assetTypesPrePopulateData from './data/assetTypes_prePopulate.js';
import Logger from './log.js';

const L = Logger('db');

const collectionDetails = {
	incidents: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	events: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	geometries: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	positions: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	personnel: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	groups: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	assets: {
		type: CollectionType.DOCUMENT_COLLECTION
	},
	assetTypes: {
		type: CollectionType.DOCUMENT_COLLECTION,
		systemData: assetTypesSystemData,
		prePopulate: assetTypesPrePopulateData
	},
	relationships: {
		type: CollectionType.EDGE_COLLECTION
	},
	assetRelationships: {
		type: CollectionType.EDGE_COLLECTION
	}
};

const graphDetails = {
	relationships: {
		edgeDefinitions: [
			{
				collection: 'relationships',
				from: ['events', 'incidents', 'geometries'],
				to: ['events', 'incidents', 'geometries']
			}
		]
	},
	assetRelationships: {
		edgeDefinitions: [
			{
				collection: 'assetRelationships',
				from: ['assets', 'assetTypes'],
				to: ['assets', 'assetTypes']
			}
		]
	}
};

/**
 * ArangoDB User-defined Function for getting the last
 * vertex of a collection type in a graph path
 *
 * @param path ArangoDB graph path to search
 * @param {string} type Name of collection that the returned
 *   vertex should be from
 */
const lastOf = (path, type) => {
	type += '/';

	for (let i = path.vertices.length - 1; i >= 0; i--) {
		if (path.vertices[i]._id.startsWith(type)) {
			return path.vertices[i];
		}
	}

	return null;
};

/** @type {import('arangojs').Database} */
export let db;

export const initialiseDb = () => {
	db = (config instanceof Promise ? config : Promise.resolve())
		.then(async () => {
			L.debug('Attempting to initialise database');
			const database = new Database({
				url:
					config.database.url ||
					`http://${config.database.host || 'localhost'}:${config.database.port || '8529'}`
			});
			if (config.database.username && config.database.password) {
				L.debug(`Authenticating to database as ${config.database.username}`);
				database.useBasicAuth(config.database.username, config.database.password);
			}
			// Check the database exists
			L.debug('Getting list of avaiable databases');
			let dbs;
			try {
				dbs = await database.listUserDatabases();
			} catch (e) {
				const error = 'Could not connect to database';
				L.error(e.toString(), e.code);
				return Promise.reject(new Error(error));
			}

			L.debug(`Checking '${config.database.database}' database exists: ${dbs}`);
			if (dbs.indexOf(config.database.database) === -1) {
				try {
					await database.createDatabase(config.database.database);
				} catch (e) {
					const error = `Could not create database ${config.database.database}`;
					L.error(error, e.code);
					return Promise.reject(new Error(error));
				}
			}
			database.useDatabase(config.database.database);

			// Check collections exist
			const collections = (await database.listCollections()).reduce((arr, collection) => {
				arr[collection.name] = collection;
				return arr;
			}, {});

			L.debug('database has collections', collections);

			for (const collection in collectionDetails) {
				const details = collectionDetails[collection];
				const collectionName = config.database.collections[collection];
				if (Object.prototype.hasOwnProperty.call(collections, collectionName)) {
					L.info(`Checking ${collectionName} collection for ${collection}`);
					// Check is correct type
					if (collections[collectionName].type !== details.type) {
						return Promise.reject(
							new Error(`Collection for ${collection} (${collectionName} is not correct type`)
						);
					}

					if (details.systemData) {
						await database.collection(collectionName).replaceAll(
							details.systemData.map((doc) => {
								const { id, ...rest } = doc;
								return {
									...rest,
									_key: id
								};
							})
						);
					}

					continue;
				}

				L.info(`Creating ${collectionName} collection for ${collection}`);
				// Create the collection
				const dbCollection = await database.createCollection(collectionName, {
					...(details.options || {}),
					type: details.type
				});

				if (details.prePopulate) {
					await dbCollection.saveAll(
						details.prePopulate.map((doc) => {
							const { id, ...rest } = doc;
							return {
								...rest,
								_key: id
							};
						})
					);
				}

				if (details.systemData) {
					await dbCollection.saveAll(
						details.systemData.map((doc) => {
							const { id, ...rest } = doc;
							return {
								...rest,
								_key: id
							};
						})
					);
				}
			}

			// Check graphs exist
			const graphs = (await database.listGraphs()).reduce((arr, graph) => {
				arr[graph.name] = graph;
				return arr;
			}, {});

			L.debug('database has graphs', JSON.stringify(graphs, null, 2));

			for (const graph in graphDetails) {
				const details = graphDetails[graph];
				const graphName = config.database.graphs[graph];

				// Map names to configured names
				const edgeDefinitions = [...details.edgeDefinitions];
				for (let i = 0; i < edgeDefinitions.length; i++) {
					const edge = edgeDefinitions[i];
					edgeDefinitions[i] = {
						collection: config.database.collections[edge.collection],
						from: [],
						to: []
					};
					for (let j = 0; j < edge.from.length; j++) {
						edgeDefinitions[i].from[j] = config.database.collections[edge.from[j]];
					}
					for (let j = 0; j < edge.to.length; j++) {
						edgeDefinitions[i].to[j] = config.database.collections[edge.to[j]];
					}
				}

				if (Object.prototype.hasOwnProperty.call(graphs, graphName)) {
					L.info(`Checking ${graphName} collection for ${graph}`);
					// TODO Check edge definitions correct
					continue;
				}

				L.info(`Creating ${graphName} collection for ${graph}`);
				// Create the collection
				await database.createGraph(graphName, edgeDefinitions, {
					...(details.options || {})
				});
			}

			// Register user functions
			await database.createFunction('ON_LOCATION::LAST_OF', lastOf.toString(), true);

			db = database;
			return db;
		})
		.catch((error) => {
			db = error;
			return Promise.reject(error);
		});
};

initialiseDb();
