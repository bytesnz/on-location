import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import { aql } from 'arangojs';
import Logger from '../log.js';
import * as assignments from '../store/assignments.js';
import { addConnectionSubscription, removeConnectionSubscription } from './connections.js';

const L = Logger('socket:assignments');

export default (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action, ...parameters] = type.split(':');

	switch (action) {
		case 'subscribe':
			L.debug('Adding subscription from', connection.ip);

			addConnectionSubscription(connection, {
				type: 'assignments',
				id,
				data
			});

			if (!reconnect) {
				assignments.getAssignments(data).then((assignments) => {
					connection.socket.send(
						JSON.stringify({
							t: 'assignments:list',
							i: id,
							d: assignments
						})
					);
				});
			}

			break;
	}
};

/**
 * Get the current/future incidents from the database.
 *
 * @returns Promise<OL.Incident[]>
 */
export const getAssignments = async () => {
	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error retrieving eventss'
			}
		};
	}

	const cursor = await db.query({
		query: `
			let users = (FOR doc IN @@userCollection
			RETURN {
				type: "user",
				id: doc._id,
				name: CONCAT_SEPARATOR(" ", doc.givenName, doc.familyName)
			})

			let groups = (FOR doc in @@groupsCollection
			RETURN {
				type: "group",
				id: doc._id,
				name: doc.name
			})

			RETURN APPEND(users, groups)
		`,
		bindVars: {
			'@userCollection': config.database.collections.personnel,
			'@groupsCollection': config.database.collections.groups
		}
	});

	let assignments = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			assignments.push(batch[i]);
		}
	}

	return assignments;
};

export const getCallsigns = async () => {
	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error retrieving eventss'
			}
		};
	}

	const cursor = await db.query({
		query: `
			let users = (FOR doc IN @@userCollection
			RETURN {
				type: "user",
				id: doc._id,
				callsigns: [ doc.id ],
				name: CONCAT(doc.id, " (", doc.givenName, " ", doc.familyName, ")")
			})

			let groups = (FOR doc in @@groupsCollection
			RETURN {
				type: "group",
				id: doc._id,
				callsigns: doc.callsigns,
				name: CONCAT(CONCAT_SEPARATOR("/", doc.callsigns), " (", doc.name, ")")
			})

			RETURN APPEND(users, groups)
		`,
		bindVars: {
			'@userCollection': config.database.collections.personnel,
			'@groupsCollection': config.database.collections.groups
		}
	});

	let callsigns = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			callsigns.push(batch[i]);
		}
	}

	return callsigns;
};
