import Logger from '../log.js';
import * as positions from '../store/positions.js';
import { addConnectionSubscription, removeConnectionSubscription } from './connections.js';

const L = Logger('socket:positions');

export default async (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action, ...parameters] = type.split(':');

	switch (action) {
		case 'subscribe':
			L.debug('Adding subscription from', connection.ip);
			// Add subscription to connection
			addConnectionSubscription(connection, {
				type: 'positions',
				id,
				data
			});

			// Get the
			if (!reconnect) {
				positions.getPositions(data).then((positions) => {
					connection.socket.send(
						JSON.stringify({
							t: 'positions:list',
							i: id,
							d: positions
						})
					);
				});
			}

			break;
		case 'create': // Submit a new position
			L.debug('Creating new position', data);
			try {
				let localId;

				if (data._localId) {
					localId = data._localId;
					delete data._localId;
				}

				const result = await positions.createPosition(data);

				connection.socket.send(
					JSON.stringify({
						t: 'positions:result',
						i: id,
						d: {
							...data,
							id: result._key,
							_localId: localId
						}
					})
				);
			} catch (error) {
				let errorData;
				L.error('Got error', error.code, error);
				switch (error.code) {
					case 'VALIDATION_ERROR':
						errorData = {
							error: 'Invalid position',
							errors: error.errors
						};
						break;
					case 'ECONNREFUSED':
					case 401:
						L.error('Could not connect to the database', error);
						errorData = {
							error: 'Server unavailable'
						};
						break;
					default:
						L.error('Unknown error', error);
						errorData = {
							error: 'Server unavailable'
						};
				}

				connection.socket.send(
					JSON.stringify({
						t: 'positions:error',
						i: id,
						d: errorData
					})
				);
			}
			break;
	}
};
