import Logger from '../log.js';
import {
	addConnectionHook,
	addConnectionSubscription,
	removeConnectionSubscription
} from './connections.js';
import {
	connectService,
	disconnectService,
	getServiceConnection,
	getServiceData,
	livenService,
	updateService
} from '../store/services.js';

const L = Logger('socket:services');

// TODO Need better id
let nextId = 0;
const requests = {};

export default async (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action, ...parameters] = type.split(':');

	switch (action) {
		case 'subscribe':
			// Add subscription to connection
			L.debug('Adding subscription from', connection.ip);
			addConnectionSubscription(connection, {
				type: 'services',
				id,
				data
			});

			if (!reconnect) {
				connection.socket.send(
					JSON.stringify({
						t: 'services:update',
						i: id,
						d: await getServiceData()
					})
				);
			}

			break;
		case 'service':
			switch (parameters.shift()) {
				case 'connect': {
					L.info('Service connected');

					try {
						const serviceId = await connectService(data, connection);

						addConnectionHook(connection, 'close', () => {
							disconnectService(serviceId);
						});

						addConnectionHook(connection, 'message', () => {
							livenService(serviceId);
						});
					} catch (error) {
						switch (error.code) {
							case 'VALIDATION_ERROR':
								connection.socket.send(
									JSON.stringify({
										t: 'services:error',
										i: id,
										d: {
											error: 'Invalid Service given',
											details: error.details
										}
									})
								);
								break;
							default:
								L.error('Error connecting service', error);
								break;
						}
					}

					break;
				}
				case 'update': {
					L.info('Service update');

					try {
						await updateService(data);
					} catch (error) {
						switch (error.code) {
							case 'VALIDATION_ERROR':
								connection.socket.send(
									JSON.stringify({
										t: 'services:error',
										i: id,
										d: {
											error: 'Invalid Service given',
											details: error.details
										}
									})
								);
								break;
							default:
								L.error('Error connecting service', error);
								break;
						}
					}

					break;
				}
				case 'request': {
					L.debug('Got service request update', data);

					const type = parameters.shift();

					const request = requests[id];
					if (!request) {
						L.info('Unknown request id', id, 'ignoring');
						break;
					}

					// Forward on to requester
					request.connection.socket.send(
						JSON.stringify({
							t: 'services:request:' + type,
							i: request.id,
							d: data
						})
					);

					switch (type) {
						case 'update':
							if (data.status === 'complete') {
								delete requests[id];
							}
							break;
						case 'error':
							delete requests[id];
							break;
						default:
							L.warn('Unknown service request message', type);
					}
					break;
				}
			}
			break;
		case 'request': {
			L.debug('Got service request', data);

			const serviceConnection = getServiceConnection(data.id);
			L.debug('Got service connection for?', data.id, !!serviceConnection);

			if (serviceConnection) {
				const requestId = nextId++;
				serviceConnection.socket.send(
					JSON.stringify({
						t: 'services:service:request',
						i: requestId,
						d: data.data
					})
				);

				requests[requestId] = {
					connection,
					id,
					data
				};
			}

			break;
		}
	}
};
