import Logger from '../log.js';
import * as geometries from '../store/geometries.js';
import { addConnectionSubscription, removeConnectionSubscription } from './connections.js';

const L = Logger('socket:geometries');

export default (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action, ...parameters] = type.split(':');

	switch (action) {
		case 'subscribe':
			L.debug('Adding subscription from', connection.ip);
			// Add subscription to connection
			addConnectionSubscription(connection, {
				type: 'geometries',
				id,
				data
			});

			// Get the
			if (!reconnect) {
				geometries.getGeometries(data).then((geometries) => {
					connection.socket.send(
						JSON.stringify({
							t: 'geometries',
							i: id,
							d: geometries
						})
					);
				});
			}

			break;
	}
};
