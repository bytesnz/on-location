import { WebSocketServer } from 'ws';
import * as connections from './connections.js';
import Logger from '../log.js';
import handleEventsMessage from './events.js';
import handleIncidentsMessage from './incidents.js';
import handleGeometriesMessage from './geometries.js';
import handlePositionsMessage from './positions.js';
import handleAssetsMessage from './assets.js';
import handleAssignmentsMessage from './assignments.js';
import handleServicesMessage from './services.js';

const L = Logger('socket');
L.setLogLevel('DEBUG');

/**
 * Create the web socket server for on-location
 *
 * @param {import('ws').ServerOptions} serverOptions
 *
 * @returns {WebSocketServer}
 */
export const createServer = (serverOptions) => {
	const wss = new WebSocketServer(serverOptions);

	wss.on('connection', (ws, request) => {
		L.debug('new connection to web socket');

		const connection = {
			socket: ws,
			request,
			ip:
				request.headers['x-real-ip'] ||
				(Array.isArray(request.headers['x-forwarded-for'])
					? request.headers['x-forwarded-for'][0]
					: request.headers['x-forwarded-for']?.replace(/,.*$/, '')) ||
				request.socket.remoteAddress
		};

		connections.addConnection(connection);

		ws.on('message', (data) => {
			try {
				data = JSON.parse(data);
			} catch (error) {
				L.debug(`Received bad data from ${connection.ip}`);
			}

			if (typeof data !== 'object' || typeof data.t !== 'string') {
				L.debug(`Received bad JSON from ${connection.ip}`);
				return;
			}

			L.debug('Received message', data);

			const type = data.t.split(':', 1)[0];

			switch (type) {
				case 'incidents':
					handleIncidentsMessage(connection, data);
					break;
				case 'events':
					handleEventsMessage(connection, data);
					break;
				case 'geometries':
					handleGeometriesMessage(connection, data);
					break;
				case 'positions':
					handlePositionsMessage(connection, data);
					break;
				case 'assets':
					handleAssetsMessage(connection, data);
					break;
				case 'assignments':
					handleAssignmentsMessage(connection, data);
					break;
				case 'services':
					handleServicesMessage(connection, data);
					break;
			}
		});

		ws.on('close', () => {
			connections.runConnectionHooks(connection, 'close');
			connections.removeConnection(connection);
		});
	});

	return wss;
};

/**
 * Attach the web socket server to the given http(s) server
 *
 * @param {import('http').Server} server Server to attach socket server to
 */
export const attachServer = (server) => {
	const wss = createServer({
		noServer: true
	});
	server.on('upgrade', (request, socket, head) => {
		if (request.headers['sec-websocket-protocol'] === 'on-location') {
			wss.handleUpgrade(request, socket, head, (ws) => {
				wss.emit('connection', ws, request);
			});
		}
	});
};
