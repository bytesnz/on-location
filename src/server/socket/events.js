import Logger from '../log.js';
import * as events from '../store/events.js';
import { addConnectionSubscription, removeConnectionSubscription } from './connections.js';

const L = Logger('socket:events');

export default (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action, ...parameters] = type.split(':');

	switch (action) {
		case 'subscribe':
			// Add subscription to connection
			L.debug('Adding subscription from', connection.ip);
			addConnectionSubscription(connection, {
				type: 'events',
				id,
				data
			});

			// Get the
			if (!reconnect) {
				events.getEvents(data).then((events) => {
					connection.socket.send(
						JSON.stringify({
							t: 'events',
							i: id,
							d: events
						})
					);
				});
			}

			break;
	}
};
