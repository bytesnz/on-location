import Logger from '../log.js';
import * as assets from '../store/assets.js';
import { addConnectionSubscription, removeConnectionSubscription } from './connections.js';

const L = Logger('socket:assets');

export default (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action, ...parameters] = type.split(':');

	switch (action) {
		case 'subscribe':
			// Add subscription to connection
			L.debug('Adding subscription from', connection.ip);
			addConnectionSubscription(connection, {
				type: 'assets',
				id,
				data
			});

			// Get the
			if (!reconnect) {
				L.debug('Getting assets to send to subscription');
				assets.getAssets(data).then(
					(assets) => {
						connection.socket.send(
							JSON.stringify({
								t: 'assets:list',
								i: id,
								d: assets
							})
						);
					},
					(error) => {
						L.debug('Got error retrieving assets for socket subscription', error);
						connection.socket.send(
							JSON.stringify({
								t: 'assets:error',
								i: id,
								data: {
									error: error.message
								}
							})
						);
					}
				);
			}

			break;
	}
};
