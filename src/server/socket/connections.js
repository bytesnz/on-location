/**
 * @template [D=object]
 * @typedef {object} Subscription
 * @prop {string} type Type of object subscribed to
 * @prop {string | number} id ID of subscription
 * @prop {D} data Data for connection
 */

/**
 * @typedef {object} Connection
 * @prop {import('ws').WebSocket} socket
 * @prop {import('http').IncomingMessage} request
 * @prop {string} ip IP address
 * @prop {{[hook: string]: Array<Function>} hooks
 */

/**
 * @typedef {object} ConnectionData
 * @prop {Subscription[]} subscriptions
 * @prop {null} user
 */

/** @type {Map<Connection, ConnectionData>} */
const connections = new Map();

/**
 * Add a socket connection to the connections list
 *
 * @param {Connection} connection
 */
export const addConnection = (connection) => {
	if (!connections.has(connection)) {
		connections.set(connection, {
			user: null,
			subscriptions: [],
			hooks: {}
		});
	}
};

/**
 * Remove a socket connection from the connections list
 *
 * @param {Connection} connection
 */
export const removeConnection = (connection) => {
	if (connections.has(connection)) {
		connections.delete(connection);
	}
};

/**
 * Add a subscription to connection
 *
 * @param {Connection} connection Connection to add to
 * @param {Subscription} subscription Subscription to add
 *
 * @returns {boolean} If the connection exists
 */
export const addConnectionSubscription = (connection, subscription) => {
	const details = connections.get(connection);

	if (details) {
		details.subscriptions.push(subscription);
		return true;
	}

	return false;
};

/**
 * Remove a subscription to connection
 *
 * @param {Connection} connection Connection to remove from
 * @param {Subscription} subscription Subscription to remove
 *
 * @returns {boolean} If the connection exists
 */
export const removeConnectionSubscription = (connection, subscription) => {
	const details = connections.get(connection);

	if (details) {
		for (let i = 0; i < details.subscriptions.length; i++) {
			if (
				details.subscriptions[i].type === subscription.type &&
				details.subscriptions[i].id === subscription.id
			) {
				details.subscriptions.splice(i, 1);
				return true;
			}
		}

		return true;
	}

	return false;
};

/**
 * Add a hook function to a connection
 *
 * @param {Connection} connection Connection to add the hook to
 * @param {string} hook Hook
 * @param {function} callback Callback function for hook
 */
export const addConnectionHook = (connection, hook, callback) => {
	if (!connection.hooks[hook]) {
		connection.hooks[hook] = [callback];
		return;
	}

	if (connection.hooks[hook].indexOf(callback) === -1) {
		connection.hooks[hook].push(callback);
	}
};

/**
 * Run connection hooks on a connection
 *
 * @param {Connection} connection Connection to run hooks for
 * @param {string} hook Hook to run hooks for
 */
export const runConnectionHooks = (connection, hook) => {
	if (!connection.hooks?.[hook]) {
		return;
	}

	for (let callback of connection.hooks[hook]) {
		callback();
	}
};

/**
 * Get all connections the have a relevant subscription to the given type
 *
 * @param {string} type Type of object subscribed to
 *
 * @returns {{
 *   connection: Connection,
 *   details: ConnectionData,
 *   subscription: Subscription
 *  }[]} Connections that have a related subscription
 */
export const getConnectionsWithSubscription = (type) => {
	const result = [];

	for (const [connection, details] of connections) {
		for (let i = 0; i < details.subscriptions.length; i++) {
			if (details.subscriptions[i].type === type) {
				result.push({
					connection,
					details,
					subscription: details.subscriptions[i]
				});
			}
		}
	}

	return result;
};
