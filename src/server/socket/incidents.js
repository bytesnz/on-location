import Logger from '../log.js';
import * as incidents from '../store/incidents.js';
import { addConnectionSubscription, removeConnectionSubscription } from './connections.js';

const L = Logger('socket:incidents');

export default (connection, { t: type, d: data, i: id, r: reconnect }) => {
	const [, action] = type.split(':');

	switch (action) {
		case 'subscribe':
			L.debug('Adding subscription from', connection.ip);
			// Add subscription to connection
			addConnectionSubscription(connection, {
				type: 'incidents',
				id,
				data
			});

			// Get the
			if (!reconnect) {
				incidents.getIncidents().then((incidents) => {
					connection.socket.send(
						JSON.stringify({
							t: 'incidents',
							i: id,
							d: incidents
						})
					);
				});
			}

			break;
	}
};
