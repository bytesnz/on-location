import Logger from '../../log.js';
import { getIncidents, createIncident, updateIncident } from '../../store/incidents.js';
import { appise } from '../index.js';
import { getConnectionsWithSubscription } from '../../socket/connections.js';

const L = Logger('api:incidents');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function get() {
	L.debug('Got request for incidents');
	try {
		const incidents = await getIncidents();

		return {
			status: 200,
			body: incidents
		};
	} catch (error) {
		return makeError(
			500,
			{
				error: 'Error retrieving incidents'
			},
			`Error retreiveing incidents: ${error.message}`,
			'ERROR'
		);
	}
}

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function put({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json incident put request'
		);
	}

	let incident;
	try {
		incident = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new incident failed to parse'
		);
	}

	let localId;

	if (incident._localId) {
		localId = incident._localId;
		delete incident._localId;
	}

	try {
		const result = await createIncident(incident);

		// Emit to subscribed connections
		const connections = getConnectionsWithSubscription('incidents');

		if (connections.length) {
			const fullIncident = {
				...incident,
				id: result._key,
				_localId: localId
			};
			setTimeout(() => {
				L.debug('Updating subscriptions listening', connections);
				for (let i = 0; i < connections.length; i++) {
					if (connections[i].subscription.data.incident === incident.id) {
						L.debug('sending update to', connections[i].connection.ip);
						connections[i].connection.socket.send(
							JSON.stringify({
								t: 'incident',
								i: connections[i].subscription.id,
								d: fullIncident
							})
						);
					}
				}
			}, 0);
		}

		return {
			status: 201,
			body: {
				id: result._key
			}
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Incident Given',
						details: error.details
					},
					'Request to create new incident was invalid'
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Patch update incident
 */
export async function patch({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json incident put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json incident put request'
		);
	}

	let incident;
	try {
		incident = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new incident failed to parse'
		);
	}

	let localId;

	if (incident._localId) {
		localId = incident._localId;
		delete incident._localId;
	}

	try {
		const result = await updateIncident(incident);

		// Emit to subscribed connections
		const connections = getConnectionsWithSubscription('incidents');

		if (connections.length) {
			const fullIncident = {
				...incident,
				id: result._key,
				_localId: localId
			};
			setTimeout(() => {
				for (let i = 0; i < connections.length; i++) {
					if (connections[i].subscription.data.incident === incident.id) {
						L.debug('sending update to', connections[i].connection.ip);
						connections[i].connection.socket.send(
							JSON.stringify({
								t: 'incident',
								i: connections[i].subscription.id,
								d: fullIncident
							})
						);
					}
				}
			}, 0);
		}

		return {
			status: 200
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Incident given',
						details: error.details
					},
					'Request to create new incident was invalid'
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding incidents api to app');
	app.get(base + 'incidents', appise(get));
	app.put(base + 'incidents', appise(put));
	app.patch(base + 'incidents', appise(patch));
};
