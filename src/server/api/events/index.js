import Logger from '../../log.js';
import { getEvents, createEvent, updateEvent } from '../../store/events.js';
import { appise } from '../index.js';
import { getConnectionsWithSubscription } from '../../socket/connections.js';
import { getRelatedIncidents } from '../../store/incidents.js';
import { config } from '../../config.js';

const L = Logger('api:events');
L.setLogLevel('DEBUG');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

const cleanForApi = (event) => {
	const { _id, _key, _rev, ...clean } = event;
	clean.id = _key;

	return clean;
};

/** @type {import('@sveltejs/kit').RequestHandler}
 * Get a list of events
 */
export async function get({ url }) {
	if (!url.searchParams.has('incident') || !url.searchParams.get('incident')) {
		return {
			status: 400,
			body: {
				error: 'Incident required'
			}
		};
	}

	try {
		const events = await getEvents({
			incident: url.searchParams.get('incident'),
			all: true
		});

		return {
			status: 200,
			body: events
		};
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error retrieving events'
			}
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Create a new event
 */
export async function put({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json event put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json event put request'
		);
	}

	let event;
	try {
		event = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new event failed to parse'
		);
	}

	let localId;

	if (event._localId) {
		localId = event._localId;
		delete event._localId;
	}

	try {
		const result = await createEvent(event);

		// Emit to subscribed connections
		const connections = getConnectionsWithSubscription('events');

		if (connections.length) {
			const fullEvent = {
				...event,
				id: result._key,
				_localId: localId
			};
			const incidentsPromise = getRelatedIncidents(
				`${config.database.collections.events}/${result._key}`
			);
			setTimeout(async () => {
				const incidents = await incidentsPromise;
				L.debug('Updating subscriptions listening for incidents', incidents, connections);
				for (let i = 0; i < connections.length; i++) {
					if (incidents.indexOf(connections[i].subscription.data.incident) !== -1) {
						L.debug('sending update to', connections[i].connection.ip);
						connections[i].connection.socket.send(
							JSON.stringify({
								t: 'event',
								i: connections[i].subscription.id,
								d: fullEvent
							})
						);
					}
				}
			}, 0);
		}

		return {
			status: 201,
			body: {
				id: result._key
			}
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Event given',
						details: error.details
					},
					'Request to create new event was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Patch update event
 */
export async function patch({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json event put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json event put request'
		);
	}

	let event;
	try {
		event = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new event failed to parse'
		);
	}

	let localId;

	if (event._localId) {
		localId = event._localId;
		delete event._localId;
	}

	try {
		const result = await updateEvent(event);

		// Emit to subscribed connections
		const connections = getConnectionsWithSubscription('events');

		if (connections.length) {
			const fullEvent = {
				...event,
				id: result._key,
				_localId: localId
			};
			const incidentsPromise = getRelatedIncidents(
				`${config.database.collections.events}/${result._key}`
			);
			setTimeout(async () => {
				const incidents = await incidentsPromise;
				L.debug('Updating subscriptions listening for incidents', incidents, connections);
				for (let i = 0; i < connections.length; i++) {
					if (incidents.indexOf(connections[i].subscription.data.incident) !== -1) {
						L.debug('sending update to', connections[i].connection.ip);
						connections[i].connection.socket.send(
							JSON.stringify({
								t: 'event',
								i: connections[i].subscription.id,
								d: fullEvent
							})
						);
					}
				}
			}, 0);
		}

		return {
			status: 200
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Event given',
						details: error.details
					},
					'Request to create new event was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);
		L.debug(error);

		return {
			status: 418
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding events api to app');
	app.get(base + 'events', appise(get));
	app.put(base + 'events', appise(put));
	app.patch(base + 'events', appise(patch));
};
