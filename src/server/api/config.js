import { readFile } from 'fs/promises';
import { clientConfig } from '../config.js';
import { appise } from './index.js';

/** @type {import('@sveltejs/kit').RequestHandler}
 *
 * GET request handler for getting config
 */
export async function get() {
	if (clientConfig instanceof Promise) {
		await clientConfig;
	}

	return {
		body: clientConfig
	};
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	app.get(base + 'config', appise(get));
};
