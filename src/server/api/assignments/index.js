import Logger from '../../log.js';
import { appise } from '../index.js';
import { createAssignment, getAssignments, getCallsigns } from '../../store/assignments.js';
import { searchParamsToObject } from '../../../lib/utils.js';

const L = Logger('api:assignments');
L.setLogLevel('DEBUG');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function get({ url }) {
	const params = searchParamsToObject(url.searchParams, {
		booleanKeys: ['coinciding', 'currentOnly']
	});
	L.debug('got params', params);

	try {
		const assignments = await getAssignments(params);

		return {
			status: 200,
			body: assignments
		};
	} catch (error) {
		L.error(error);
		if (error.code === 'VALIDATION_ERROR') {
			return {
				status: 400,
				body: {
					error: 'Invalid parameters',
					details: error.details
				}
			};
		}

		return makeError(
			500,
			{
				error: 'Error retrieving assignments'
			},
			`Error retreiveing assignments: ${error.message}`,
			'ERROR'
		);
	}
}

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function getCallsignsApi() {
	try {
		const callsigns = await getCallsigns();

		return {
			status: 200,
			body: callsigns
		};
	} catch (error) {
		if (error.code === 'VALIDATION_ERROR') {
			return {
				status: 400,
				body: {
					error: 'Invalid parameters',
					details: error.details
				}
			};
		}

		return makeError(
			500,
			{
				error: 'Error retrieving callsigns'
			},
			`Error retreiveing callsigns: ${error.message}`,
			'ERROR'
		);
	}
}

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function put({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json assignment put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json assignment put request'
		);
	}

	let assignment;
	try {
		assignment = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new assignment failed to parse'
		);
	}

	// Check if item currently assigned
	try {
		const result = await createAssignment(assignment);

		const body = {};

		if (result?._key) {
			body.id = result._key;
		}

		return {
			status: 201,
			body
		};
	} catch (error) {
		console.error(error);
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: error.message,
						details: error.details
					},
					'Request to create new assignment was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding assignments api to app');
	app.get(base + 'assignments', appise(get));
	app.put(base + 'assignments', appise(put));
	//app.patch(base + 'assignments', appise(patch));
	app.get(base + 'callsigns', appise(getCallsignsApi));
};
