import Logger from '../../log.js';
import { getGeometries, createGeometry, updateGeometry } from '../../store/geometries.js';
import { appise } from '../index.js';
import { getConnectionsWithSubscription } from '../../socket/connections.js';
import { getRelatedIncidents } from '../../store/incidents.js';
import { config } from '../../config.js';

const L = Logger('api:geometries');
L.setLogLevel('DEBUG');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

const cleanForApi = (geometry) => {
	const { _id, _key, _rev, ...clean } = geometry;
	clean.id = _key;

	return clean;
};

/** @type {import('@sveltejs/kit').RequestHandler}
 * Get a list of geometries
 */
export async function get({ url, request }) {
	if (!url.searchParams.has('incident') || !url.searchParams.get('incident')) {
		return {
			status: 400,
			body: {
				error: 'Incident required'
			}
		};
	}

	try {
		const geometries = await getGeometries({
			incident: url.searchParams.get('incident'),
			all: true
		});

		switch (request.headers.get('content-type')) {
			case 'application/geo+json':
				// Rewrite geometries to be Features and add to FeatureCollection
				for (let i = 0; i < geometries.length; i++) {
					const { geometry, ...properties } = geometries[i];
					geometries[i] = {
						type: 'Feature',
						geometry,
						properties
					};
				}

				return {
					status: 200,
					headers: {
						'Content-Type': 'application/geo+json'
					},
					body: {
						type: 'FeatureCollection',
						features: geometries
					}
				};
			case 'application/json':
			default:
				break;
		}

		return {
			status: 200,
			body: geometries
		};
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error retrieving geometries'
			}
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Create a new geometry
 */
export async function put({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json geometry put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json geometry put request'
		);
	}

	let geometry;
	try {
		geometry = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new geometry failed to parse'
		);
	}

	let localId;

	if (geometry._localId) {
		localId = geometry._localId;
		delete geometry._localId;
	}

	try {
		const result = await createGeometry(geometry);

		// Emit to subscribed connections
		const connections = getConnectionsWithSubscription('geometries');

		if (connections.length) {
			const fullGeometry = {
				...geometry,
				id: result._key,
				_localId: localId
			};
			const incidentsPromise = getRelatedIncidents(
				`${config.database.collections.geometries}/${result._key}`
			);
			setTimeout(async () => {
				const incidents = await incidentsPromise;
				L.debug('Updating subscriptions listening for incidents', incidents, connections);
				for (let i = 0; i < connections.length; i++) {
					if (incidents.indexOf(connections[i].subscription.data.incident) !== -1) {
						L.debug('sending update to', connections[i].connection.ip);
						connections[i].connection.socket.send(
							JSON.stringify({
								t: 'geometry',
								i: connections[i].subscription.id,
								d: fullGeometry
							})
						);
					}
				}
			}, 0);
		}

		return {
			status: 201,
			body: {
				id: result._key
			}
		};
	} catch (error) {
		console.error(error);
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: error.message,
						details: error.details
					},
					'Request to create new geometry was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Patch update geometry
 */
export async function patch({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json geometry put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json geometry put request'
		);
	}

	let geometry;
	try {
		geometry = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new geometry failed to parse'
		);
	}

	let localId;

	if (geometry._localId) {
		localId = geometry._localId;
		delete geometry._localId;
	}

	try {
		const result = await updateGeometry(geometry);

		// Emit to subscribed connections
		const connections = getConnectionsWithSubscription('geometries');

		if (connections.length) {
			const fullGeometry = {
				...geometry,
				id: result._key,
				_localId: localId
			};
			const incidentsPromise = getRelatedIncidents(
				`${config.database.collections.geometries}/${result._key}`
			);
			setTimeout(async () => {
				const incidents = await incidentsPromise;
				L.debug('Updating subscriptions listening for incidents', incidents, connections);
				for (let i = 0; i < connections.length; i++) {
					if (incidents.indexOf(connections[i].subscription.data.incident) !== -1) {
						L.debug('sending update to', connections[i].connection.ip);
						connections[i].connection.socket.send(
							JSON.stringify({
								t: 'geometry',
								i: connections[i].subscription.id,
								d: fullGeometry
							})
						);
					}
				}
			}, 0);
		}

		return {
			status: 200
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Geometry given',
						details: error.details
					},
					'Request to create new geometry was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding geometries api to app');
	app.get(base + 'geometries', appise(get));
	app.put(base + 'geometries', appise(put));
	app.patch(base + 'geometries', appise(patch));
};
