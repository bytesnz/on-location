import Logger from '../../log.js';
import { getAssets, getAssetTypes, createAsset, updateAsset } from '../../store/assets.js';
import { appise } from '../index.js';
import { searchParamsToObject } from '../../../lib/utils.js';

const L = Logger('api:assets');
L.setLogLevel('DEBUG');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

const cleanForApi = (asset) => {
	const { _id, _key, _rev, ...clean } = asset;
	clean.id = _key;

	return clean;
};

/** @type {import('@sveltejs/kit').RequestHandler}
 * Get a list of assets
 */
export async function get({ url }) {
	const params = searchParamsToObject(url.searchParams, {
		arrayKeys: ['types'],
		booleanKeys: ['callsigns', 'fixed', 'favourite', 'useId', 'radios']
	});

	try {
		const assets = await getAssets(params);

		return {
			status: 200,
			body: assets
		};
	} catch (error) {
		L.error(error);
		return {
			status: 500,
			body: {
				error: 'Error retrieving assets'
			}
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Get a list of assetTypes
 */
export async function assetTypesGet({ url }) {
	const params = searchParamsToObject(url.searchParams, {
		arrayKeys: ['types']
	});

	try {
		const assetTypes = await getAssetTypes(params);

		return {
			status: 200,
			body: assetTypes
		};
	} catch (error) {
		L.error(error);
		return {
			status: 500,
			body: {
				error: 'Error retrieving assetTypes'
			}
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Create a new asset
 */
export async function put({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json asset put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json asset put request'
		);
	}

	let asset;
	try {
		asset = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new asset failed to parse'
		);
	}

	let localId;

	if (asset._localId) {
		localId = asset._localId;
		delete asset._localId;
	}

	try {
		const result = await createAsset(asset);

		return {
			status: 201,
			body: {
				id: result._key
			}
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Asset given',
						details: error.details
					},
					'Request to create new asset was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Patch update asset
 */
export async function patch({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json asset put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json asset put request'
		);
	}

	let asset;
	try {
		asset = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new asset failed to parse'
		);
	}

	let localId;

	if (asset._localId) {
		localId = asset._localId;
		delete asset._localId;
	}

	try {
		const result = await updateAsset(asset);

		return {
			status: 200
		};
	} catch (error) {
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: 'Invalid Asset given',
						details: error.details
					},
					'Request to create new asset was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);
		L.debug(error);

		return {
			status: 418
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding assets api to app');
	app.get(base + 'assets', appise(get));
	app.put(base + 'assets', appise(put));
	app.patch(base + 'assets', appise(patch));
	app.get(base + 'assetTypes', appise(assetTypesGet));
};
