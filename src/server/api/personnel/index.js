import Logger from '../../log.js';
import { getPersonnelList } from '../../store/personnel.js';
import { appise } from '../index.js';
import { getConnectionsWithSubscription } from '../../socket/connections.js';

const L = Logger('api:personnel');
L.setLogLevel('DEBUG');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

const cleanForApi = (event) => {
	const { _id, _key, _rev, ...clean } = event;
	clean.id = _key;

	return clean;
};

/** @type {import('@sveltejs/kit').RequestHandler} */
export async function getList({ request }) {
	try {
		const events = await getPersonnelList();

		return {
			status: 200,
			body: events
		};
	} catch (error) {
		L.error('Gor error while getting personnel list', error);
		return {
			status: 500,
			body: {
				error: 'Error retrieving personnel list'
			}
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding personnel api to app');
	app.get(base + 'personnel/list', appise(getList));
};
