/**
 * Make a http request handler out of a SvelteKit RequestHandler

 * @param {import('@sveltejs/kit').RequestHandler} svelteHandler
 */
export const appise = (svelteHandler) => {
	/**
	 * @param {import('http').IncomingMessage} request
	 * @param {import('http').ServerResponse} response
	 */
	return async (request, response) => {
		const result = await svelteHandler({
			request: {
				...request,
				headers: new Map(Object.entries(request.headers)),
				json: () => request.body
			},
			url: new URL(request.url, `http://${request.headers.host}`)
		});

		response.statusCode = result.status || 200;
		if (result.headers) {
			for (const header in result.headers) {
				response.setHeader(header, result.headers[header]);
			}
		}
		if (typeof result.body !== 'string') {
			if (!response.hasHeader('content-type'))
				response.setHeader('Content-Type', 'application/json');
			response.end(JSON.stringify(result.body));
		} else {
			response.end(result.body);
		}
	};
};
