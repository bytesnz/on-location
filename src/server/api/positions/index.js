import Logger from '../../log.js';
import { getPositions, createPosition, handlePositionShare } from '../../store/positions.js';
import { appise } from '../index.js';
import { getConnectionsWithSubscription } from '../../socket/connections.js';
import { getRelatedIncidents } from '../../store/incidents.js';
import { config } from '../../config.js';
import { searchParamsToObject } from '../../../lib/utils.js';
import { firstHeaderMatch } from 'http-header-list';

const L = Logger('api:positions');
L.setLogLevel('DEBUG');

const makeError = (status, body, message, level) => {
	level = level || 'DEBUG';
	L(level, message);

	return {
		status,
		body
	};
};

const cleanForApi = (position) => {
	const { _id, _key, _rev, ...clean } = position;
	clean.id = _key;

	return clean;
};

/** @type {import('@sveltejs/kit').RequestHandler}
 * Get a list of positions
 */
export async function get({ url, request }) {
	const params = searchParamsToObject(url.searchParams, {
		arrayKeys: ['subjects']
	});

	try {
		const positions = await getPositions(params);

		const match = firstHeaderMatch(request.headers.get('accept'), [
			'application/geo+json',
			'application/json'
		]);

		switch (match?.value) {
			case 'application/geo+json':
				// Rewrite positions to be Features and add to FeatureCollection
				for (let i = 0; i < positions.length; i++) {
					const { latitude, longitude, ...properties } = positions[i];
					positions[i] = {
						type: 'Feature',
						geometry: {
							type: 'Point',
							coordinates: [longitude, latitude]
						},
						properties
					};
				}

				return {
					status: 200,
					headers: {
						'Content-Type': 'application/geo+json'
					},
					body: {
						type: 'FeatureCollection',
						features: positions
					}
				};
			case 'application/json':
			default:
				break;
		}

		return {
			status: 200,
			body: positions
		};
	} catch (error) {
		if (error.code === 'VALIDATION_ERROR') {
			return {
				status: 400,
				body: {
					error: 'Invalid parameters',
					details: error.details
				}
			};
		}

		return {
			status: 500,
			body: {
				error: 'Error retrieving positions'
			}
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Create a new position
 */
export async function put({ request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		L.debug('Received non-json position put request');
		return makeError(
			400,
			{
				error: 'Invalid Content-Type'
			},
			'Received non-json position put request'
		);
	}

	let position;
	try {
		position = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new position failed to parse'
		);
	}

	let localId;

	if (position._localId) {
		localId = position._localId;
		delete position._localId;
	}

	try {
		const result = await createPosition(position);

		return {
			status: 201,
			body: {
				id: result._key
			}
		};
	} catch (error) {
		console.error(error);
		switch (error.code) {
			case 'VALIDATION_ERROR':
				return makeError(
					400,
					{
						error: error.message,
						details: error.details
					},
					'Request to create new position was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Get share settings and check authorisation
 */
export async function getShare({ url, request }) {
	// TODO Add authorisation check

	const params = searchParamsToObject(url.searchParams);

	if (!params.id) {
		return {
			status: 403,
			body: {
				error: 'Position submission not authenticated'
			}
		};
	}

	// TODO Make incident-specific
	return {
		status: 200,
		body: {
			settings: {
				accuracy: 'high',
				updateTime: 300000,
				updateDistance: 10,
				maximumAge: 300000,
				timeout: 10000
			}
		}
	};
}

/** @type {import('@sveltejs/kit').RequestHandler}
 * Submit a shared position
 */
export async function postShare({ url, request }) {
	if (request.headers.get('content-type') !== 'application/json') {
		return {
			status: 400,
			body: {
				error: 'Invalid request, must be application/json'
			}
		};
	}

	// TODO Add authorisation check

	const params = searchParamsToObject(url.searchParams);

	if (!params.id) {
		return {
			status: 403,
			body: {
				error: 'Position submission not authenticated'
			}
		};
	}

	const token = params.id;

	let position;
	try {
		position = await request.json();
	} catch (error) {
		return makeError(
			400,
			{
				error: 'Invalid JSON'
			},
			'Request to create new position failed to parse'
		);
	}

	try {
		const result = await handlePositionShare(token, position);

		return {
			status: 200,
			body: {
				id: result._key
			}
		};
	} catch (error) {
		console.error(error);
		switch (error.code) {
			case 'VALIDATION_ERROR':
				if (error.message === 'Invalid token') {
					return makeError(
						403,
						{
							error: error.message,
							details: error.details
						},
						'Invalid token'
					);
				}

				return makeError(
					400,
					{
						error: error.message,
						details: error.details
					},
					'Request to create new position was invalid: ' + error.message
				);
			case 'ECONNREFUSED':
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not connect to the database',
					'ERROR'
				);
			case 401:
				return makeError(
					500,
					{
						message: 'Server unavailable'
					},
					'Could not authenticate to the database',
					'ERROR'
				);
		}

		L.error('error saving', error.toString(), error.code);

		return {
			status: 418
		};
	}
}

/**
 * Attach the routes to the given app
 *
 * @param {import('polka').Polka} app Server app
 * @param {string} base / ended base for urls
 */
export default (app, base) => {
	L.debug('Adding positions api to app');
	app.get(base + 'positions', appise(get));
	app.put(base + 'positions', appise(put));
	app.get(base + 'positions/share', appise(getShare));
	app.post(base + 'positions/share', appise(postShare));
};
