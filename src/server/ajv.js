import Ajv from 'ajv/dist/2019.js';

// TODO import addFormats from 'ajv-formats';
// See:
//   https://github.com/vitejs/vite/discussions/2074#discussioncomment-381633
//   https://github.com/vitejs/vite/issues/2890#issuecomment-819923122
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const addFormats = require('ajv-formats');

// TODO Add strictSchema log when in dev mode
const ajv = new Ajv({
	discriminator: true,
	strictSchema: false
});
addFormats(ajv);

export default ajv;
