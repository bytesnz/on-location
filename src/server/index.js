import { createServer } from 'http';
import { env } from 'process';
import polka from 'polka';
import Logger from './log.js';
import { setDefaultInspectOptions } from './log.js';
import { attachServer as attachSocketServer } from './socket/index.js';
import attachIncidents from './api/incidents/index.js';
import attachEvents from './api/events/index.js';
import attachPersonnel from './api/personnel/index.js';
import attachGeometries from './api/geometries/index.js';
import attachPositions from './api/positions/index.js';
import attachAssets from './api/assets/index.js';
import attachAssignments from './api/assignments/index.js';
import attachConfig from './api/config.js';
import cors from 'cors';
import bodyParser from 'body-parser';
import { handler } from '../../build/handler.js';
import Trouter from 'trouter';
import parser from '@polka/url';
import makeTileCacher from 'tile-cacher';
import { config } from './config.js';

setDefaultInspectOptions({
	depth: 10,
	colors: true
});

const L = Logger('server');

const port = 3000;
const base = '/api/';

const server = createServer({
	keepAlive: true
});
const app = polka({
	server
});
const router = new Trouter();

config.then((config) => {
	app.use(
		cors(),
		bodyParser.json({
			limit: config.server?.jsonUploadLimit || '1mb'
		})
	);

	app.use((request, response, next) => {
		L.debug(`request for ${request.url}`);
		next();
	});

	try {
		attachSocketServer(server);
		attachConfig(router, base);
		attachIncidents(router, base);
		attachEvents(router, base);
		attachPersonnel(router, base);
		attachGeometries(router, base);
		attachPositions(router, base);
		attachAssets(router, base);
		attachAssignments(router, base);
	} catch (error) {
		L.error(`Exiting due to error`, error);
		process.exit(error.code || 1);
	}

	if (config.mapCaches.length) {
		L.debug('Adding tile-cacher to app');
		router.get(
			'/map/cache/:id/:z/:x/:y.png',
			makeTileCacher({
				servers: config.mapCaches,
				logger: Logger('tile-cacher')
			})
		);
	}

	app.use((request, response, next) => {
		const urlInfo = parser(request);
		const handlers = router.find(request.method, urlInfo.pathname);
		L.debug(`checking api for ${request.url}`);

		if (!handlers || !handlers.handlers.length) {
			next();
			return;
		}

		handlers.handlers.forEach((fn) => {
			fn(
				{
					...request,
					headers: request.headers, // Needs to be specifically included
					params: handlers.params
				},
				response
			);
		});
	});

	// Default to http instead of https
	app.use((request, response, next) => {
		L.debug(`falling to svelte ${request.url}`);
		if (env['PROTOCOL_HEADER']) {
			if (!request.headers['x-forwarded-proto']) {
				request.headers['x-forwarded-proto'] = 'http';
			}
		}
		next();
	});

	app.use(handler);

	app.listen(port);
	L.log(`Server listening on port ${port}`);
});
