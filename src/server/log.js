import chalk from 'chalk';
import { inspect } from 'node:util';

const levels = ['ERROR', 'WARN', 'LOG', 'INFO', 'DEBUG'];

let logLevel = levels.indexOf('LOG');
let defaultInspectOptions = {};

const setLogLevel = (level) => {
	if (typeof level === 'number') {
		logLevel = Math.max(0, Math.min(level, levels.length - 1));
	} else {
		level = levels.indexOf(level);
		if (level !== -1) {
			logLevel = level;
		}
	}

	return levels[logLevel];
};

/*TODO let givenLevel = process.env.LOG_LEVEL;
if (givenLevel) {
	const num = Number(givenLevel);
	if (!isNaN(num)) {
		setLogLevel(num);
	} else {
		setLogLevel(givenLevel);
	}
}
*/

/**
 * @callback LogFunction
 * @param {any[]} ...message Message to log
 */

/**
 * @typedef {Object} Logger
 * @param {string} level Log level
 * @param {any[]} ...message Message to log
 * @property {LogFunction} error Log error message
 * @property {LogFunction} warn Log warn message
 * @property {LogFunction} log Log log message
 * @property {LogFunction} info Log info message
 * @property {LogFunction} debug Log debug message
 */

/**
 * Set default optionst to use for inspect
 *
 * @param {object} [inspectOptions] Options for inspect()
 */
export function setDefaultInspectOptions(inspectOptions) {
	defaultInspectOptions = inspectOptions;
}

/**
 * Convert object items into inspected strings
 *
 * @param {Array<any>} args Array of items to convert
 * @param {object} [inspectOptions] Options for inspect()
 */
function inspectArray(args, inspectOptions) {
	const newArgs = [];

	for (let i = 0; i < args.length; i++) {
		if (typeof args[i] === 'object' && args[i] !== null) {
			newArgs.push(args[i], inspect(args, inspectOptions));
		} else {
			newArgs.push(args[i]);
		}
	}

	return newArgs;
}

/**
 * Create a new logger
 *
 * @param {string} id ID for new logger
 * @param {object} [inspectOptions] Options for inspect()
 *
 * @returns {Logger}
 */
export default (id, inspectOptions) => {
	const error = (...message) => {
		if (logLevel >= levels.indexOf('ERROR')) {
			message = inspectArray(message, inspectOptions || defaultInspectOptions);
			// eslint-disable-next-line no-console
			console.error(new Date().toISOString(), id, chalk.red('ERROR'), ...message);
		}
	};
	const warn = (...message) => {
		if (logLevel >= levels.indexOf('WARN')) {
			message = inspectArray(message, inspectOptions || defaultInspectOptions);
			// eslint-disable-next-line no-console
			console.warn(new Date().toISOString(), id, chalk.yellow('WARN'), ...message);
		}
	};
	const log = (...message) => {
		if (logLevel >= levels.indexOf('LOG')) {
			message = inspectArray(message, inspectOptions || defaultInspectOptions);
			// eslint-disable-next-line no-console
			console.log(new Date().toISOString(), id, chalk.green('LOG'), ...message);
		}
	};
	const info = (...message) => {
		if (logLevel >= levels.indexOf('INFO')) {
			message = inspectArray(message, inspectOptions || defaultInspectOptions);
			// eslint-disable-next-line no-console
			console.info(new Date().toISOString(), id, chalk.magenta('INFO'), ...message);
		}
	};
	const debug = (...message) => {
		if (logLevel >= levels.indexOf('DEBUG')) {
			message = inspectArray(message, inspectOptions || defaultInspectOptions);
			// eslint-disable-next-line no-console
			console.debug(new Date().toISOString(), id, chalk.cyan('DEBUG'), ...message);
		}
	};

	/* @type {Logger} */
	const logger = (level, ...message) => {
		switch (level) {
			case 'ERROR':
				return error(...message);
			case 'WARN':
				return warn(...message);
			case 'INFO':
				return info(...message);
			case 'DEBUG':
				return debug(...message);
			case 'LOG':
			default:
				return log(...message);
		}
	};

	Object.assign(logger, {
		setLogLevel,
		error,
		warn,
		log,
		info,
		debug
	});

	Object.freeze(logger);

	return logger;
};
