import { createServer } from 'http';
import polka from 'polka';
import Logger from './log.js';
import { setDefaultInspectOptions } from './log.js';
import { attachServer } from './socket/index.js';
import attachIncidents from './api/incidents/index.js';
import attachEvents from './api/events/index.js';
import attachPersonnel from './api/personnel/index.js';
import attachGeometries from './api/geometries/index.js';
import attachPositions from './api/positions/index.js';
import attachAssets from './api/assets/index.js';
import attachAssignments from './api/assignments/index.js';
import attachConfig from './api/config.js';
import cors from 'cors';
import bodyParser from 'body-parser';
import makeTileCacher from 'tile-cacher';
import { config } from './config.js';

setDefaultInspectOptions({
	depth: 10,
	colors: true
});

const L = Logger('server');

const port = 3001;
const base = '/';

const onError = (error, request, response, next) => {
	L.error('Error handling request', error);

	if (error.type === 'entity.too.large') {
		response.statusCode = 413;
		response.setHeader('Content-Type', 'application/json');
		response.end(
			JSON.stringify({
				error: 'Payload too large'
			})
		);
	} else {
		response.statusCode = 500;
		response.end();
	}
};

const server = createServer({
	keepAlive: true
});

const app = polka({
	server,
	onError
});

config.then((config) => {
	app.use(
		cors(),
		bodyParser.json({
			limit: config.server?.jsonUploadLimit || '1mb'
		})
	);

	app.use((request, response, next) => {
		L.debug(`request for ${request.url}`);
		next();
	});

	try {
		attachServer(server);
		attachConfig(app, base);
		attachIncidents(app, base);
		attachEvents(app, base);
		attachPersonnel(app, base);
		attachGeometries(app, base);
		attachPositions(app, base);
		attachAssets(app, base);
		attachAssignments(app, base);
	} catch (error) {
		L.error(`Exiting due to error`, error);
		process.exit(error.code || 1);
	}

	if (config.mapCaches.length) {
		app.get(
			base + 'map/cache/:id/:z/:x/:y.png',
			makeTileCacher({
				servers: config.mapCaches,
				logger: Logger('tile-cacher')
			})
		);
	}
	app.listen(port);
	L.log(`Server listening on port ${port}`);
});
