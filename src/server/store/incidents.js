import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import { aql } from 'arangojs';
import incidentSchema from '../../schemas/incident.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import { timedSort } from '../../lib/date.js';

const L = Logger('server:store:incidents');
L.setLogLevel('DEBUG');

const validate = ajv.compile(incidentSchema);

const cleanForApi = (incident) => {
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const { _id, _key, _rev, ...clean } = incident;
	clean.id = _key;

	return clean;
};

/**
 * Check the db and config have been resolved
 */
async function awaitDatabaseConfig() {
	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}
}

/**
 * Get the current/future incidents from the database.
 *
 * @returns Promise<OL.Incident[]>
 */
export const getIncidents = async () => {
	await awaitDatabaseConfig();

	const collection = db.collection(config.database.collections.incidents);

	const cursor = await db.query(aql`
    FOR doc IN ${collection}
    FILTER (
      doc.startTime > DATE_NOW() ||
      (IS_NULL(doc.endTime) || doc.endTime > DATE_NOW())
    )
    RETURN doc
  `);

	const incidents = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			incidents.push(cleanForApi(batch[i]));
		}
	}

	incidents.sort(timedSort);
	incidents.reverse();

	return incidents;
};

/**
 * Get the incidents related to a specific object
 *
 * @param {string} id The _id of the object to get the related incidents for
 *
 * @returns {Promise<OL.Incident[]>} The related incidents
 */
export const getRelatedIncidents = async (id) => {
	await awaitDatabaseConfig();

	if (id.startsWith(config.database.collections.incidents + '/')) {
		return [id.slice(config.database.collections.incidents.length + 1)];
	}

	const collection = db.collection(config.database.collections.incidents);

	const bindVars = {
		'@incidents': config.database.collections.incidents,
		relationships: config.database.graphs.relationships,
		object: id
	};

	const cursor = await db.query({
		query: `
			WITH @@incidents
			FOR vertex, edge, path
			IN 1..10
			OUTBOUND @object
			GRAPH @relationships
			FILTER IS_SAME_COLLECTION(@@incidents, vertex)
			RETURN DISTINCT vertex._key
		`,
		bindVars
	});

	const incidents = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			incidents.push(batch[i]);
		}
	}

	return incidents;
};

/**
 * Get the incidents that overlap with the given timeranges
 *
 * @param {import('../../lib/date.js').Merged} times Timeranges to get the
 *   incidents for
 *
 * @returns {number[]} IDs of overlapping incidents
 */
export async function getOverlappingIncidents(times) {
	await awaitDatabaseConfig();

	if (!times?.length) {
		throw new Error('Require times get overlapping incidents');
	}

	const bindVars = {
		'@incidents': config.database.collections.incidents,
		times: []
	};

	let query = `FOR doc in @@incidents
		FILTER `;

	let added = false;

	for (const time of times) {
		const startIndex = bindVars.times.push(time.startTime);
		if (!time.endTime) {
			query += added
				? 'OR '
				: '' +
				  `(
				doc.startTime >= @times[${startIndex}] OR
				!HAS(doc, 'endTime') OR IS_NULL(doc.endTime) OR
				doc.endTime >= @times[${startIndex}]
			)`;
		} else {
			const endIndex = bindVars.times.push(time.endTime);

			query += added
				? 'OR '
				: '' +
				  `(
				(
					(!HAS(doc, 'endTime') OR IS_NULL(doc.endTime)) AND
					doc.startTime <= @times[${endIndex}]
				) OR
				(
					doc.startTime >= @times[${startIndex}] AND
					doc.startTime <= @times[${endIndex}]
				) OR
				(
					doc.endTime >= @times[${startIndex}] AND
					doc.endTime <= @times[${endIndex}]
				) OR
				(
					doc.startTime <= @times[${startIndex}] AND
					doc.endTime >= @times[${endIndex}]
				)
			)`;
		}
		added = true;
	}

	query += `
		RETURN DISTINCT doc._key`;

	L.debug('Getting overlapping incidents', times, query, bindVars);

	const cursor = await db.query({
		query,
		bindVars
	});

	const incidents = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			incidents.push(batch[i]);
		}
	}

	return incidents;
}

/**
 * Create a new incident in the database/
 *
 * @param {OL.Incident} incident The new incident to create
 *
 * @returns Promise<> Information on the new incident record
 */
export const createIncident = async (incident) => {
	// Check the given incident is valid
	if (!validate(incident)) {
		throw new ValidationError('Invalid incident given', validate.errors);
	}

	// Try to store
	await awaitDatabaseConfig();

	if (incident.id) {
		let id;
		// eslint-disable-next-line prefer-const
		({ id, ...incident } = incident);
		incident._key = id;
	}

	const incidentCollection = db.collection(config.database.collections.incidents);

	const result = await incidentCollection.save(incident);
	L.info(`Created new incident ${result._key} ${incident.name}`);

	return result;
};

/**
 * Update an incident in the database/
 *
 * @param {OL.Incident} incident The incident to update
 *
 * @returns Promise<> Information on the new incident record
 */
export const updateIncident = async (incident) => {
	// Check the given incident is valid
	if (!validate(incident)) {
		throw new ValidationError('Invalid incident given', validate.errors);
	}

	if (!incident.id) {
		// TODO Add error object
		throw new ValidationError('ID require', null);
	}

	// Try to store
	await awaitDatabaseConfig();

	let id;
	// eslint-disable-next-line prefer-const
	({ id, ...incident } = incident);
	incident._key = id;

	const incidentCollection = db.collection(config.database.collections.incidents);

	const result = await incidentCollection.replace(id, incident);
	L.info(`Updated incident ${result._key} ${incident.name}`);

	return result;
};
