import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import AssignementSchema from '../../schemas/assignment.js';
import { getConnectionsWithSubscription } from '../socket/connections.js';
import { getRelatedIncidents, getOverlappingIncidents } from './incidents.js';
import { getObjectNames } from './utils.js';
import { mergeTimes } from '../../lib/date.js';

const L = Logger('server:store:assignments');

export const parametersSchema = {
	type: 'object',
	properties: {
		incident: {
			type: 'string'
		},
		coinciding: {
			type: 'boolean'
		},
		currentOnly: {
			type: 'boolean'
		}
	},
	required: ['incident']
};

/** @typedef {OL.Assignment} AssignmentCreation
 * @property {boolean} [allowJointAssignment] If true, existing assignments
 *   won't be finished
 */

/** */
const parametersValidate = ajv.compile(parametersSchema);
const assignmentValidate = ajv.compile({
	type: 'object',
	properties: {
		allowJointAssignment: {
			type: 'boolean'
		}
	},
	allOf: [AssignementSchema]
});

/**
 * Update assignment subscribers
 *
 * @param {OL.Assignment|OL.Assignment[]} object Object to update subscribers with
 */
const updateSubscribers = async (object) => {
	const connections = getConnectionsWithSubscription('assignments');

	L.debug('Updating subscribers with', object);

	if (connections.length) {
		let incidents;

		// Get incidents that are related to the assignement target
		if (Array.isArray(object)) {
			const targets = [];
			incidents = [];

			for (let i = 0; i < object.length; i++) {
				if (targets.indexOf(object[i].target.id) === -1) {
					targets.push(object[i].target.id);
				}
			}

			await Promise.all(targets.map((target) => getRelatedIncidents(target))).then(
				(targetIncidents) => {
					for (const incidentArray of targetIncidents) {
						for (const incident of incidentArray) {
							if (incidents.indexOf(incident) === -1) {
								incidents.push(incident);
							}
						}
					}
				}
			);
		} else {
			const incidentsPromise = getRelatedIncidents(object.target);

			incidents = await incidentsPromise;
		}

		const mergedTimes = mergeTimes(Array.isArray(object) ? object : [object]);

		const coincidingIncidents = await getOverlappingIncidents(mergedTimes);

		L.debug('Got coinciding incidents', coincidingIncidents);

		for (const incident of coincidingIncidents) {
			if (incidents.indexOf(incident) === -1) {
				incidents.push(incident);
			}
		}

		L.debug(
			'Updating subscriptions listening for incidents',
			incidents,
			connections.map((c) => c.subscription)
		);
		for (let i = 0; i < connections.length; i++) {
			L.debug('checking if', connections[i].subscription.data.incident, 'in', incidents);
			if (
				!connections[i].subscription.data.incident ||
				incidents.indexOf(connections[i].subscription.data.incident) !== -1
			) {
				L.debug('sending update to', connections[i].connection.ip);
				connections[i].connection.socket.send(
					JSON.stringify({
						t: 'assignments:update',
						i: connections[i].subscription.id,
						d: object
					})
				);
			}
		}
	}
};

const edgeDocFormattedReturn = `
	RETURN MERGE(
		{
			id: doc._key,
			subject: MERGE(
				{ id: doc._from },
				KEEP(DOCUMENT(doc._from), 'name', 'givenName', 'familyName')
			),
			target: MERGE(
				{ id: doc._to },
				KEEP(DOCUMENT(doc._to), 'name', 'givenName', 'familyName')
			),
		},
		KEEP(doc, 'startTime', 'endTime', 'state')
	)
`;

/**
 * Get the current/future incidents from the database.
 *
 * @returns Promise<OL.Incident[]>
 */
export const getAssignments = async (parameters) => {
	if (!parameters || !parametersValidate(parameters)) {
		throw new ValidationError('Parameters were not given correctly', parametersValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error retrieving assignments'
			}
		};
	}

	let query = `
			WITH @@personnel, @@groups, @@assets, @@relationships
			FOR vertex, edge, path
			IN 1..1
			INBOUND @incident
			GRAPH @relationships
			FILTER edge.type == "assignedTo"`;
	const bindVars = {
		'@personnel': config.database.collections.personnel,
		'@groups': config.database.collections.groups,
		'@assets': config.database.collections.assets,
		'@relationships': config.database.collections.relationships,
		relationships: config.database.graphs.relationships,
		incident: `${config.database.collections.incidents}/${parameters.incident}`
	};

	if (parameters.currentOnly) {
		query += `
			FILTER edge.startTime > @now || (
				edge.startTime < @now && (
					!HAS(edge, 'endTime') ||
					edge.endTime > @now
				)
			)`;
		bindVars.now = new Date().toISOString();
	}

	query += `
			RETURN DISTINCT {
				id: edge._key,
				startTime: edge.startTime,
				endTime: edge.endTime,
				subject: MERGE(
					{ id: vertex._id },
					KEEP(vertex, "name", "givenName", "familyName")
				),
				state: edge.state
			}`;

	L.debug('current query', query, 'bindVars', bindVars);

	const cursor = await db.query({
		query,
		bindVars
	});

	let assignments = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			assignments = assignments.concat(batch[i]);
		}
	}

	if (parameters.coinciding) {
		const incident = await db
			.collection(config.database.collections.incidents)
			.document(parameters.incident);

		const bindVars = {
			'@relationships': config.database.collections.relationships,
			incident: incident._id
		};

		let query = `
			FOR doc in @@relationships
				FILTER doc._to != @incident
				FILTER doc.type == 'assignedTo'`;

		if (parameters.currentOnly) {
			bindVars.now = new Date().toISOString();
			query += `
				FILTER doc.startTime > @now || (
					doc.startTime < @now && (
						!HAS(doc, 'endTime') ||
						doc.endTime > @now
					)
				)`;
		} else {
			bindVars.startTime = incident.startTime;

			query += `
				FILTER doc.startTime >= @startTime`;

			if (incident.endTime) {
				query += `
					FILTER !HAS(doc, 'endTime') OR doc.endTime <= @endTime`;
				bindVars.endTime = incident.endTime;
			}
		}

		query += edgeDocFormattedReturn;

		L.debug('coinciding query', query, 'bindVars', bindVars);

		const cursor = await db.query({
			query,
			bindVars
		});

		let coinciding = [];

		for await (const batch of cursor.batches) {
			for (let i = 0; i < batch.length; i++) {
				coinciding = coinciding.concat(batch[i]);
			}
		}

		return {
			assignments,
			coinciding
		};
	}

	return assignments;
};

/**
 * Create an assignment
 *
 * @param {AssignmentCreation} assignment Assignment to create
 */
export const createAssignment = async (assignment) => {
	if (!assignment || !assignmentValidate(assignment)) {
		throw new ValidationError('Assignment was not given correctly', assignmentValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error creating assignment'
			}
		};
	}

	const relationshipCollection = db.collection(config.database.collections.relationships);

	// Close any current assignments
	let query = `FOR doc IN @@relationships
		FILTER doc._from == @subject`;
	const bindVars = {
		'@relationships': config.database.collections.relationships,
		subject: assignment.subject
	};
	if (assignment.allowJointAssignment) {
		query += `
		FILTER doc._to == @target`;
		bindVars.target = assignment.target;
	}

	query +=
		`
		FILTER !HAS(doc, 'endTime')` + edgeDocFormattedReturn;

	L.debug('Getting existing assignments', query, bindVars);
	const cursor = await db.query({
		query,
		bindVars
	});

	let modifiedAssignments = [];
	const updatePromises = [];
	for await (const batch of cursor.batches) {
		L.debug('Modifying existing assignments', batch);
		for (let exisitingAssignment of batch) {
			modifiedAssignments.push({
				...exisitingAssignment,
				endTime: assignment.startTime
			});
			updatePromises.push(
				db.collection(config.database.collections.relationships).update(exisitingAssignment.id, {
					endTime: assignment.startTime
				})
			);
		}
	}

	await Promise.all(updatePromises);

	let result = null;
	if (assignment.state !== 'standdown') {
		const record = {
			_from: assignment.subject,
			_to: assignment.target,
			startTime: assignment.startTime,
			type: 'assignedTo'
		};
		if (assignment.state) {
			record.state = assignment.state;
		}
		if (assignment.endTime) {
			record.endTime = assignment.endTime;
		}

		L.debug('creating new assignment', record);

		result = await relationshipCollection.save(record);

		const names = await getObjectNames([assignment.target, assignment.subject]);
		modifiedAssignments.push({
			...assignment,
			target: {
				id: assignment.target,
				name: names[assignment.target]
			},
			subject: {
				id: assignment.subject,
				name: names[assignment.subject]
			},
			id: result._key
		});
	}

	setTimeout(async () => {
		L.debug('updating subscribers');
		updateSubscribers(modifiedAssignments);
	}, 0);

	return result;
};

export const getCallsigns = async () => {
	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		return {
			status: 500,
			body: {
				error: 'Error retrieving eventss'
			}
		};
	}

	const cursor = await db.query({
		query: `
			let users = (FOR doc IN @@userCollection
			RETURN {
				type: "user",
				id: doc._id,
				callsigns: [ doc.id ],
				label: CONCAT(doc.id, " (", doc.givenName, " ", doc.familyName, ")")
			})

			let groups = (FOR doc in @@groupsCollection
			RETURN {
				type: "group",
				id: doc._id,
				callsigns: doc.callsigns,
				label: CONCAT(CONCAT_SEPARATOR("/", doc.callsigns), " (", doc.name, ")")
			})

			RETURN APPEND(users, groups)
		`,
		bindVars: {
			'@userCollection': config.database.collections.personnel,
			'@groupsCollection': config.database.collections.groups
		}
	});

	let callsigns = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			// TODO Currently being returned in a containing array?
			callsigns = callsigns.concat(batch[i]);
		}
	}

	return callsigns;
};
