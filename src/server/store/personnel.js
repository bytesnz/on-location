import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import { aql } from 'arangojs';
import eventSchema from '../../schemas/event.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import { cleanForApi } from '../data.js';

const L = Logger('server:store:events');

const validate = ajv.compile(eventSchema);

export const getPersonnelList = async () => {
	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	const bindVars = {
		'@collection': config.database.collections.personnel
	};

	const cursor = await db.query({
		query: `
      FOR doc IN @@collection
      RETURN {
				id: doc._id,
				number: doc.id,
				label: CONCAT(doc.givenName, " ", doc.familyName)
			}
    `,
		bindVars
	});

	let list = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			list.push(batch[i]);
		}
	}

	return list;
};
