import { ValidationError } from '../../errors.js';
import { config } from '../config.js';
import { db } from '../db.js';

/**
 * Extract relatedTo parts and ensure valid
 *
 * @param {} object Object to check and extract relatedTo from
 * @param {string} relatedField Field containing relatedData
 *
 * @returns {[string, string]} Collection name and id
 */
export const extractRelatedTo = (object, relatedField) => {
	if (!relatedField) {
		relatedField = 'relatedTo';
	}

	if (!object[relatedField]) {
		throw new ValidationError('Requires related object');
	}

	if (object[relatedField].startsWith('local:') || object[relatedField].indexOf('/') === -1) {
		throw new ValidationError('Requires valid related object');
	}

	const parts = object[relatedField].split('/', 2);

	if (!parts[0] || !parts[1]) {
		throw new ValidationError('Requires valid related object');
	}

	if (typeof config.database.collections[parts[0]] === 'undefined') {
		throw new ValidationError('Requires valid related object');
	}

	return parts;
};

/**
 * Get the names of the objects with the given ids
 *
 * @param {string[]} ids IDs to retrieve the names for
 *
 * @returns {Promise<{ [id: string]: string }>} A Promise resolving to a map
 *   of the names of the objects
 */
export const getObjectNames = async (ids) => {
	if (db instanceof Promise) {
		await db;
	}

	if (db instanceof Error) {
		throw new Error('DB in error state');
	}

	if (config instanceof Promise) {
		await config;
	}

	const bindVars = {
		ids
	};

	const query = `let ids = @ids

	for id in ids
		let doc = DOCUMENT(id)
		return {
			id: doc._id,
			name: HAS(doc, 'displayName') ? doc.displayName : HAS(doc, 'givenName') ? CONCAT(doc.givenName, ' ', doc.familyName) : doc.name
		}`;

	const cursor = await db.query({
		query,
		bindVars
	});

	/** @type {{ [id: string]: string }} */
	const map = {};

	for await (const batch of cursor.batches) {
		for (const object of batch) {
			map[object.id] = object.name;
		}
	}

	return map;
};
