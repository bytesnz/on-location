import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import { aql } from 'arangojs';
import assetSchema from '../../schemas/asset.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import { cleanForApi } from '../data.js';
import { extractRelatedTo } from './utils.js';
import { getConnectionsWithSubscription } from '../socket/connections.js';
import { getRelatedIncidents } from './incidents.js';

const L = Logger('server:store:assets');

const validate = ajv.compile(assetSchema);

/**
 * @typedef {Object} Parameters Parameters for retrieving incidents
 * @prop [string] incident Incident ID to return assets for
 * @prop [string] [since] Retrieve only incidents that have been modified since
 *   the given ISO8601 date
 */
const parametersValidate = ajv.compile({
	type: 'object',
	properties: {
		types: {
			type: 'array',
			items: {
				type: 'string'
			}
		},
		query: {
			type: 'string',
			title: 'Search Term'
		},
		callsigns: {
			type: 'boolean',
			title: 'Include current asset callsigns'
		},
		radios: {
			type: 'boolean',
			title: 'Include associated radios'
		},
		useId: {
			type: 'boolean',
			title: 'Use full id'
		},
		fixed: {
			type: 'boolean',
			title: 'Only include or exclude fixed assets'
		},
		favourite: {
			type: 'boolean',
			title: 'Only include or exclude favourite assets'
		}
	},
	required: []
});

export const getAssetTypes = async (parameters) => {
	if (parameters && !parametersValidate(parameters)) {
		throw new ValidationError('Parameters were not given correctly', parametersValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	L.debug('Getting assets from', config.database.collections.assets);

	return _getAssetTypes(parameters);
};

const _getAssetTypes = async (parameters) => {
	let query;

	const bindVars = {
		'@assetTypes': config.database.collections.assetTypes
	};

	if (parameters.types) {
		query = `WITH @@assetRelationships, @@assetTypes
		let typesDocs = (
			FOR doc in @@assetTypes
				FILTER POSITION(@types, doc._id)
				RETURN UNSET(MERGE(doc, { id: doc._key }), '_id', '_key', '_rev')
		)

		let subTypes = (
			FOR type in @types
				FOR vertex
					IN 1..100
					INBOUND type
					GRAPH @assetRelationships
					RETURN DISTINCT UNSET(MERGE(vertex, { id: vertex._key }), '_id', '_key', '_rev')
		)

		RETURN APPEND(typesDocs, subTypes, true)
		`;
		bindVars['types'] = parameters.types.map(
			(type) => config.database.collections.assetTypes + '/' + type
		);
		bindVars['@assetRelationships'] = config.database.collections.assetRelationships;
		bindVars['assetRelationships'] = config.database.collections.assetRelationships;
	} else {
		query = `FOR doc in @@assetTypes
			RETURN UNSET(MERGE(doc, { id: doc._key }), '_id', '_key', '_rev')
		`;
	}

	const cursor = await db.query({
		query,
		bindVars
	});

	let types = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			types = types.concat(batch[i]);
		}
	}

	return types;
};

export const getAssets = async (parameters) => {
	if (parameters && !parametersValidate(parameters)) {
		throw new ValidationError('Parameters were not given correctly', parametersValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	L.debug('Getting assets from', config.database.collections.assets);

	const bindVars = {
		'@assets': config.database.collections.assets
	};

	let query = '';

	// TODO Incident graph

	query += 'FOR doc IN @@assets';

	// Build asset type array if type given
	if (parameters.types) {
		const types = await _getAssetTypes({
			types: parameters.types
		});
		L.debug('types', types);
		bindVars['types'] = types.map((type) => type.id);
		query += `
			FILTER LENGTH(INTERSECTION(@types, doc.type)) != 0`;
	}

	if (parameters.fixed) {
		query += `
			FILTER doc.favourite != true`;
	} else if (parameters.fixed === false) {
		query += `
			FILTER doc.fixed != true`;
	}

	if (parameters.favourite) {
		query += `
			FILTER doc.favourite == true`;
	} else if (parameters.favourite === false) {
		query += `
			FILTER doc.favourite != true`;
	}

	if (parameters.callsigns) {
		query += `
			let callsigns = (
        FOR vertex, edge, path
          IN 1..10
          INBOUND doc._id
          GRAPH 'assetRelationships'
          //PRUNE edge.relation != 'based_in'
          PRUNE has_callsigns = HAS(vertex, "callsigns")
          FILTER IS_SAME_COLLECTION(assets, vertex)
          FILTER edge.relation == 'contains'
          FILTER has_callsigns
          RETURN {
						id: vertex._id,
						name: vertex.name,
						callsigns: vertex.callsigns,
						relation: edge.relation
					}
	    )`;
	}

	if (parameters.radios) {
		query += `
		let radios = (
			FOR path
				IN 1..100 OUTBOUND K_PATHS
				doc._id TO 'assetTypes/radio'
				GRAPH assetRelationships
				let radio = ON_LOCATION::LAST_OF(path, 'assets')
				RETURN UNSET(MERGE(radio, { id: radio._id }), '_id', '_key', '_rev')
		)`;
	}

	query += `
		SORT doc.favourite DESC
		RETURN UNSET(MERGE(doc, {
			id: doc._id,`;

	if (parameters.callsigns) {
		query += `
			callsigns: APPEND(callsigns, doc.callsigns)`;
	}

	if (parameters.radios) {
		query += `
			radios: radios`;
	}

	query += `
		}), '_id', '_key', '_rev')`;

	/*const cursor = await db.query({
		query: `
			WITH @@incidents, @@assets
			FOR vertex, edge, path
			IN 1..10
			INBOUND @incident
			GRAPH @relationships
			FILTER IS_SAME_COLLECTION(@@assets, vertex)
			RETURN DISTINCT MERGE(
				UNSET(vertex, "_key", "_id", "_rev"),
				{ id: vertex._key, relatedTo: edge._to }
			)
		`,
		bindVars
	});*/

	L.debug(query, bindVars);

	const cursor = await db.query({
		query,
		bindVars
	});

	let assets = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			assets.push(batch[i]);
		}
	}

	return assets;
};

const updateSubscribers = async (object) => {
	const connections = getConnectionsWithSubscription('assets');

	if (connections.length) {
		const incidentsPromise = getRelatedIncidents(
			`${config.database.collections.assets}/${object.id}`
		);

		const incidents = await incidentsPromise;
		L.debug('Updating subscriptions listening for incidents', incidents, connections);
		for (let i = 0; i < connections.length; i++) {
			if (
				!connections[i].subscription.data.incident ||
				incidents.indexOf(connections[i].subscription.data.incident) !== -1
			) {
				L.debug('sending update to', connections[i].connection.ip);
				connections[i].connection.socket.send(
					JSON.stringify({
						t: 'asset',
						i: connections[i].subscription.id,
						d: object
					})
				);
			}
		}
	}
};

/**
 * Create an asset
 *
 * @param {OL.Asset} asset Asset to update
 */
export const createAsset = async (asset) => {
	asset = { ...asset };
	const [locationType, locationId] = extractRelatedTo(asset, 'location');
	delete asset.location;

	// Check the given asset is valid
	if (!validate(asset)) {
		throw new ValidationError('Invalid asset given', validate.errors);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	if (asset.id) {
		let id;
		({ id, ...asset } = asset);
		asset._key = id;
	}

	// Check that the location object exists
	const relatedCollection = db.collection(config.database.collections[locationType]);

	if (!(await relatedCollection.documentExists(locationId))) {
		throw new ValidationError('Requires valid related object');
	}

	const assetCollection = db.collection(config.database.collections.assets);

	const result = await assetCollection.save(asset);
	L.info(`Created new asset ${result._key} ${asset.name}`);

	// Create relation
	const relationshipCollection = db.collection(config.database.collections.relationships);

	const relationshipResult = await relationshipCollection.save({
		_from: result._id,
		_to: `${locationType}/${locationId}`,
		type: 'location'
	});

	// Emit to subscribed connections
	setTimeout(() => {
		updateSubscribers({
			...asset,
			id: result._key
		});
	}, 0);

	return result;
};

/**
 * Update an asset
 *
 * @param {OL.Asset} asset Asset to update
 */
export const updateAsset = async (asset) => {
	asset = { ...asset };
	const [relatedToType, relatedToId] = extractRelatedTo(asset);
	delete asset.relatedTo;

	// Check the given asset is valid
	if (!validate(asset)) {
		throw new ValidationError('Invalid asset given', validate.errors);
	}

	if (!asset.id) {
		// TODO Add error object
		throw new ValidationError('ID require', null);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	asset._key = asset.id;
	delete asset.id;

	const assetCollection = db.collection(config.database.collections.assets);

	const result = await assetCollection.replace(asset._key, asset);
	L.info(`Updated asset ${result._key} ${asset.name}`);

	const relationshipCollection = db.collection(config.database.collections.relationships);

	let relationshipResult;

	// Delete current relation
	relationshipResult = await relationshipCollection.removeByExample({
		_from: result._id,
		type: 'relatedTo'
	});

	// Create relation
	relationshipResult = await relationshipCollection.save({
		_from: result._id,
		_to: `${relatedToType}/${relatedToId}`,
		type: 'relatedTo'
	});

	// Emit to subscribed connections
	setTimeout(() => {
		updateSubscribers({
			...asset,
			id: result._key
		});
	}, 0);

	return result;
};
