import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import serviceSchema from '../../schemas/service.js';
import serviceUpdateSchema from '../../schemas/serviceUpdate.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import * as serviceTypes from '../services/index.js';
import { getConnectionsWithSubscription } from '../socket/connections.js';

const L = Logger('server:store:services');

const validateService = ajv.compile(serviceSchema);
const validateServiceUpdate = ajv.compile(serviceUpdateSchema);

// TODO Figure schemas to store in DB
const services = {};

// TODO Figure out somewhere to do this
setInterval(() => {
	const lastMin = new Date();
	lastMin.setMinutes(lastMin.getMinutes() - 1);
	const lastMidIso = lastMin.toISOString();

	for (const id in services) {
		if (services[id].lastMessage < lastMidIso) {
			services[id] = {
				...services[id],
				services: services[id].services.map((s) => ({ ...s, status: 'offline' }))
			};
		}
	}
}, 60000);

/** @typedef {import('../../../types/service').Service} Service */

/**
 * Compile to service data from the service modules
 */
export async function getServiceData() {
	const serviceData = {};

	for (const type of Object.keys(serviceTypes)) {
		serviceData[type] = await serviceTypes[type].makeServiceData(Object.values(services));
	}

	return serviceData;
}

/**
 * Update the subscribers with the given services
 *
 * @param {Service[]} serviceUpdates Service(s) to update subscribers with
 */
async function updateSubscribers(serviceUpdates) {
	const connections = getConnectionsWithSubscription('services');

	// Build service update
	const updatedTypes = [];

	for (const server of serviceUpdates) {
		for (const service of server.services) {
			updatedTypes.push(service.type);
		}
	}

	L.debug('serviceUpdates', JSON.stringify(serviceUpdates, null, 2));
	L.debug('services', services);

	const serviceData = {};

	L.debug('Updating services', updatedTypes);

	for (const type of updatedTypes) {
		serviceData[type] = await serviceTypes[type].makeServiceData(Object.values(services));
	}

	if (connections.length) {
		L.debug('Updating', connections.length, 'connections with services');
		for (let i = 0; i < connections.length; i++) {
			connections[i].connection.socket.send(
				JSON.stringify({
					t: 'services:update',
					i: connections[i].subscription.id,
					d: serviceData
				})
			);
		}
	}
}

/**
 * Add a service to the database
 *
 * @param {import('../../../types/service').Service} service Service to add
 */
export const connectService = async (service, connection) => {
	if (!validateService(service)) {
		throw new ValidationError('Invalid service given', validateService.errors);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	const now = new Date().toISOString();

	services[service.guid] = {
		...service,
		created: now,
		lastMessage: now,
		connection
	};

	setTimeout(() => updateSubscribers([services[service.guid]]));

	return service.guid;

	/*const serviceCollection = db.collection(config.database.collections.relationships);

	const result = await serviceCollection.save({
		...service,
		created: now,
		lastMessage: now,
		status: 'connected'
	});

	return result._key;
	*/
};

/**
 * Update service status
 *
 * @param {import('../../../types/serviceUpdate')} serviceUpdate Update details
 */
export async function updateService(serviceUpdate) {
	if (!validateServiceUpdate(serviceUpdate)) {
		throw new ValidationError('Invalid service update given', validateServiceUpdate.errors);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	// Find the service
	if (!services[serviceUpdate.guid]) {
		throw new Error('Unknown service id');
	}

	// Go through services
	for (const service of serviceUpdate.services) {
		const index = services[serviceUpdate.guid].services.findIndex((s) => s.id === service.id);

		if (index === -1) {
			throw new Error('Unknown service');
		}

		const storedService = services[serviceUpdate.guid].services[index];

		services[serviceUpdate.guid].services[index] = serviceTypes[storedService.type].updateService(
			storedService,
			service
		);
	}

	setTimeout(() => updateSubscribers([services[serviceUpdate.guid]]));
}

/**
 * Mark service as still alive
 *
 * @param {string} id ID of service
 */
export const livenService = async (id) => {
	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	const now = new Date().toISOString();

	services[id] = {
		...services[id],
		lastMessage: now
	};

	/*
	const serviceCollection = db.collection(config.database.collections.relationships);

	const result = await serviceCollection.update(id, {
		lastMessage: now,
		status: 'connected'
	});

	// TODO Check document updated
	*/
};

export const disconnectService = async (id) => {
	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	const now = new Date().toISOString();

	services[id] = {
		...services[id],
		services: services[id].services.map((s) => ({ ...s, status: 'offline' }))
	};

	updateSubscribers();

	/*
	const serviceCollection = db.collection(config.database.collections.relationships);

	const result = await serviceCollection.update(id, {
		status: 'disconnected'
	});

	// TODO Check document updated
	*/
};

/**
 * Get the connection associated with the service with given GUID
 *
 * @param {string} id Service GUID
 */
export function getServiceConnection(id) {
	return services[id]?.connection;
}
