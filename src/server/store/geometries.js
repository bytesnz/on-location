import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import { aql } from 'arangojs';
import geometrySchema from '../../schemas/geometry.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import { cleanForApi } from '../data.js';
import { extractRelatedTo } from './utils.js';

const L = Logger('server:store:geometries');

const validate = ajv.compile(geometrySchema);

/**
 * @typedef {Object} Parameters Parameters for retrieving incidents
 * @prop [string] incident Incident ID to return geometries for
 * @prop [string] [since] Retrieve only incidents that have been modified since
 *   the given ISO8601 date
 */
const parametersValidate = ajv.compile({
	type: 'object',
	properties: {
		incident: {
			type: 'string'
		},
		since: {
			type: 'string',
			format: 'date-time'
		}
	},
	required: ['incident']
});

export const getGeometries = async (parameters) => {
	if (!parameters || !parametersValidate(parameters)) {
		throw new ValidationError('Parameters were not given correctly', parametersValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	L.debug(
		'Getting geometries for incident',
		parameters.incident,
		'from',
		config.database.collections.geometries
	);

	/* : { [key: string]: any } */
	const bindVars = {
		'@geometries': config.database.collections.geometries,
		'@events': config.database.collections.events,
		'@incidents': config.database.collections.incidents,
		relationships: config.database.graphs.relationships,
		incident: `${config.database.collections.incidents}/${parameters.incident}`
	};

	/*TODO if (parameters.since) {
		bindVars.since = parameters.since;
	}*/

	const cursor = await db.query({
		query: `
			WITH @@incidents, @@events, @@geometries
			FOR vertex, edge, path
			IN 1..10
			INBOUND @incident
			GRAPH @relationships
			FILTER IS_SAME_COLLECTION(@@geometries, vertex)
			RETURN DISTINCT MERGE(
				UNSET(vertex, "_key", "_id", "_rev"),
				{ id: vertex._key, relatedTo: edge._to }
			)
		`,
		/* TODO Multiple relatedTo query: `
			WITH @@incidents, @@events, @@geometries
			FOR vertex, edge, path
			IN 1..10
			INBOUND @incident
			GRAPH @relationships
			OPTIONS {
				uniqueEdges: "path",
				uniqueVertices: "global",
				bfs: true
			}
			FILTER IS_SAME_COLLECTION(@@geometries, vertex)
			RETURN DISTINCT MERGE(
				UNSET(vertex, "_key", "_id", "_rev"),
				{ id: vertex._key, relatedTo: path.edges[*]._to }
			)
		`,*/
		bindVars
	});
	/*TODO
      FILTER (
				doc.incident == @incident
				${parameters.all ? '' : `&& IS_NULL(doc.endTime)`}
				${parameters.since ? ' && doc.updated > @since' : ''}
      )
	*/

	let geometries = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			geometries.push(batch[i]);
		}
	}

	return geometries;
};

/**
 * Create an geometry
 *
 * @param {OL.Geometry} geometry Geometry to update
 */
export const createGeometry = async (geometry) => {
	geometry = { ...geometry };
	const [relatedToType, relatedToId] = extractRelatedTo(geometry);
	delete geometry.relatedTo;

	// Check the given geometry is valid
	if (!validate(geometry)) {
		throw new ValidationError('Invalid geometry given', validate.errors);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	if (geometry.id) {
		let id;
		({ id, ...geometry } = geometry);
		geometry._key = id;
	}

	// Check that the relatedTo object exists
	const relatedCollection = db.collection(config.database.collections[relatedToType]);

	if (!(await relatedCollection.documentExists(relatedToId))) {
		throw new ValidationError('Requires valid related object');
	}

	const geometryCollection = db.collection(config.database.collections.geometries);

	const result = await geometryCollection.save(geometry);
	L.info(`Created new geometry ${result._key} ${geometry.name}`);

	// Create relation
	const relationshipCollection = db.collection(config.database.collections.relationships);

	const relationshipResult = await relationshipCollection.save({
		_from: result._id,
		_to: `${relatedToType}/${relatedToId}`,
		type: 'relatedTo'
	});

	return result;
};

/**
 * Update an geometry
 *
 * @param {OL.Geometry} geometry Geometry to update
 */
export const updateGeometry = async (geometry) => {
	geometry = { ...geometry };
	const [relatedToType, relatedToId] = extractRelatedTo(geometry);
	delete geometry.relatedTo;

	// Check the given geometry is valid
	if (!validate(geometry)) {
		throw new ValidationError('Invalid geometry given', validate.errors);
	}

	if (!geometry.id) {
		// TODO Add error object
		throw new ValidationError('ID require', null);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	geometry._key = geometry.id;
	delete geometry.id;

	const geometryCollection = db.collection(config.database.collections.geometries);

	const result = await geometryCollection.replace(geometry._key, geometry);
	L.info(`Updated geometry ${result._key} ${geometry.name}`);

	const relationshipCollection = db.collection(config.database.collections.relationships);

	let relationshipResult;

	// Delete current relation
	relationshipResult = await relationshipCollection.removeByExample({
		_from: result._id,
		type: 'relatedTo'
	});

	// Create relation
	relationshipResult = await relationshipCollection.save({
		_from: result._id,
		_to: `${relatedToType}/${relatedToId}`,
		type: 'relatedTo'
	});

	return result;
};
