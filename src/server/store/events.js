import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import eventSchema from '../../schemas/event.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import { extractRelatedTo } from './utils.js';
import { eventSort } from '../../lib/events.js';

const L = Logger('server:store:events');

const validate = ajv.compile(eventSchema);

/**
 * @typedef {Object} Parameters Parameters for retrieving incidents
 * @prop [string] incident Incident ID to return events for
 * @prop [string] [since] Retrieve only incidents that have been modified since
 *   the given ISO8601 date
 */
const parametersValidate = ajv.compile({
	type: 'object',
	properties: {
		incident: {
			type: 'string'
		},
		since: {
			type: 'string',
			format: 'date-time'
		}
	},
	required: ['incident']
});

export const getEvents = async (parameters) => {
	if (!parameters || !parametersValidate(parameters)) {
		throw new ValidationError('Parameters were not given correctly', parametersValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	L.debug(
		'Getting events for incident',
		parameters.incident,
		'from',
		config.database.collections.events
	);

	/* : { [key: string]: any } */
	const bindVars = {
		'@events': config.database.collections.events,
		'@incidents': config.database.collections.incidents,
		relationships: config.database.graphs.relationships,
		incident: `${config.database.collections.incidents}/${parameters.incident}`
	};

	if (parameters.since) {
		bindVars.since = parameters.since;
	}

	const cursor = await db.query({
		query: `
			WITH @@incidents, @@events
			FOR vertex, edge, path
			IN 1..10
			INBOUND @incident
			GRAPH @relationships
			FILTER IS_SAME_COLLECTION(@@events, vertex)
			RETURN DISTINCT MERGE(
				UNSET(vertex, "_key", "_id", "_rev"),
				{ id: vertex._key, relatedTo: edge._to }
			)
		`,
		bindVars
	});

	let events = [];

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			events.push(batch[i]);
		}
	}

	events.sort(eventSort());

	return events;
};

/**
 * Create an event
 *
 * @param {OL.Event} event Event to update
 */
export const createEvent = async (event) => {
	event = { ...event };
	const [relatedToType, relatedToId] = extractRelatedTo(event);
	delete event.relatedTo;

	// Check the given event is valid
	if (!validate(event)) {
		throw new ValidationError('Invalid event given', validate.errors);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	if (event.id) {
		let id;
		({ id, ...event } = event);
		event._key = id;
	}

	// Check that the relatedTo object exists
	const relatedCollection = db.collection(config.database.collections[relatedToType]);

	if (!(await relatedCollection.documentExists(relatedToId))) {
		throw new ValidationError('Requires valid related object');
	}

	const eventCollection = db.collection(config.database.collections.events);

	const result = await eventCollection.save(event);
	L.info(`Created new event ${result._key} ${event.name}`);

	// Create relation
	const relationshipCollection = db.collection(config.database.collections.relationships);

	const relationshipResult = await relationshipCollection.save({
		_from: result._id,
		_to: `${relatedToType}/${relatedToId}`,
		type: 'relatedTo'
	});

	return result;
};

/**
 * Update an event
 *
 * @param {OL.Event} event Event to update
 */
export const updateEvent = async (event) => {
	event = { ...event };
	const [relatedToType, relatedToId] = extractRelatedTo(event);
	delete event.relatedTo;

	// Check the given event is valid
	if (!validate(event)) {
		throw new ValidationError('Invalid event given', validate.errors);
	}

	if (!event.id) {
		// TODO Add error object
		throw new ValidationError('ID require', null);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	event._key = event.id;
	delete event.id;

	const eventCollection = db.collection(config.database.collections.events);

	const result = await eventCollection.replace(event._key, event);
	L.info(`Updated event ${result._key} ${event.name}`);

	const relationshipCollection = db.collection(config.database.collections.relationships);

	let relationshipResult;

	// Delete current relation
	relationshipResult = await relationshipCollection.removeByExample({
		_from: result._id,
		type: 'relatedTo'
	});

	// Create relation
	relationshipResult = await relationshipCollection.save({
		_from: result._id,
		_to: `${relatedToType}/${relatedToId}`,
		type: 'relatedTo'
	});

	return result;
};
