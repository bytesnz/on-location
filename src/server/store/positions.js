import { db, initialiseDb } from '../db.js';
import { config } from '../config.js';
import { aql } from 'arangojs';
import positionSchema from '../../schemas/position.js';
import positionShareSchema from '../../schemas/positionShare.js';
import Logger from '../log.js';
import ajv from '../ajv.js';
import { ValidationError } from '../../errors.js';
import { cleanForApi } from '../data.js';
import { extractRelatedTo } from './utils.js';
import { getConnectionsWithSubscription } from '../socket/connections.js';

const L = Logger('server:store:positions');

const validate = ajv.compile(positionSchema);
const validateShare = ajv.compile(positionShareSchema);

/* @typedef{import('../../../types/position.d.ts').Position} Position */

export const parametersSchema = {
	type: 'object',
	properties: {
		incident: {
			type: 'string'
		},
		from: {
			type: 'string',
			format: 'date-time'
		},
		to: {
			type: 'string',
			format: 'date-time'
		},
		subjects: {
			type: 'array',
			items: {
				type: 'string'
			}
		},
		since: {
			type: 'string',
			format: 'date-time'
		}
	}
};

/**
 * @typedef {Object} Parameters Parameters for retrieving incidents
 * @prop [string] incident Incident ID to return positions for
 * @prop [string] [since] Retrieve only incidents that have been modified since
 *   the given ISO8601 date
 */
const parametersValidate = ajv.compile(parametersSchema);

/**
 * Fill a position or array of positions source and subjects with the
 * details of the source/subject
 *
 * @param {Position|Position[]} object Position(s) to fill
 */
const fillIdDetails = async (object) => {
	const ids = [];

	if (Array.isArray(object)) {
		for (let i = 0; i < object.length; i++) {
			if (object[i].source && ids.indexOf(object[i].source) === -1) {
				ids.push(object[i].source);
			}
			if (object[i].subject && ids.indexOf(object[i].subject) === -1) {
				ids.push(object[i].subject);
			}
		}
	} else {
		if (object.source && ids.indexOf(object.source) === -1) {
			ids.push(object.source);
		}
		if (object.subject && ids.indexOf(object.subject) === -1) {
			ids.push(object.subject);
		}
	}

	if (!ids.length) {
		return object;
	}

	const details = {};

	const cursor = await db.query({
		query: `FOR id in @ids
			let doc = DOCUMENT(id)
			RETURN {
				id: doc._id,
				name: HAS(doc, "givenName") ? CONCAT(doc.givenName, " ", doc.familyName) : doc.name,
				type: IS_SAME_COLLECTION(@personnel, doc) ? "personnel" : doc.type,
				callsigns: doc.callsigns
			}`,
		bindVars: {
			personnel: config.database.collections.personnel,
			ids
		}
	});

	for await (const batch of cursor.batches) {
		for (let i = 0; i < batch.length; i++) {
			if (batch[i].type === null) {
				batch[i].type = undefined;
			}
			if (batch[i].callsigns === null) {
				batch[i].callsigns = undefined;
			}
			details[batch[i].id] = batch[i];
		}
	}

	if (Array.isArray(object)) {
		object = [...object];
		for (let i = 0; i < object.length; i++) {
			object[i] = {
				...object[i],
				subject: details[object[i].subject] || object[i].subject,
				source: details[object[i].source] || object[i].source
			};
		}
	} else {
		object = {
			...object,
			subject: details[object.subject] || object.subject,
			source: details[object.source] || object.source
		};
	}

	return object;
};

/**
 * Update the current subscribers to the positions
 *
 * @param {Position} object Position to update subscribers with
 * @param {string} updateType Type of update that object is
 */
const updateSubscribers = async (object, updateType) => {
	L.debug('updating subscribers to positions with', object);
	const connections = getConnectionsWithSubscription('positions');

	if (connections.length) {
		/* TODO Once assignments done
		const incidentsPromise = getRelatedIncidents(
			`${config.database.collections.assets}/${object.id}`
		);

		const incidents = await incidentsPromise;
		L.debug('Updating subscriptions listening for incidents', incidents, connections);
		*/
		// Get subject and source details
		if (updateType === 'update') {
			object = await fillIdDetails(object);
		}

		L.debug('Updating connections', connections);
		for (let i = 0; i < connections.length; i++) {
			//TODO if (incidents.indexOf(connections[i].subscription.data.incident) !== -1) {
			L.debug('sending update to', connections[i].connection.ip);
			connections[i].connection.socket.send(
				JSON.stringify({
					t: 'positions:' + updateType,
					i: connections[i].subscription.id,
					d: object
				})
			);
			//}
		}
	}
};

export const getPositions = async (parameters) => {
	if (!parameters || !parametersValidate(parameters)) {
		throw new ValidationError('Parameters were not given correctly', parametersValidate.errors);
	}

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	if (typeof parameters.subjects === 'string') {
		parameters.subjects = [parameters.subjects];
	}

	L.debug(
		'Getting positions for incident',
		parameters.incident,
		'from',
		config.database.collections.positions
	);

	/*TODO if (parameters.since) {
		bindVars.since = parameters.since;
	}*/

	const bindVars = {
		'@positions': config.database.collections.positions
	};

	let subjects = null;
	let query = '';

	if (parameters.subjects) {
		subjects = parameters.subjects;
	}

	if (subjects !== null) {
		bindVars['subjects'] = subjects;
		query += `let subjects = @subjects
		`;
	} else {
		query += `let subjects = (
			FOR doc in @@positions`;

		if (!parameters.to && !parameters.from) {
			query += `
				FILTER DATE_DIFF(doc.time, DATE_NOW(), 'h') < 24`;
		}
		query += `
			RETURN DISTINCT doc.subject
		)`;
	}

	if (!parameters.to && !parameters.from) {
		query += `
			FOR id in subjects
				LET position = (
					FOR doc in @@positions
						FILTER doc.subject == id`;
	} else {
		query = `
			FOR doc IN @@positions`;
	}

	if (parameters.subjects) {
		query += `
			FILTER POSITION(@subjects, doc.subject)`;
	}

	// TODO Add? FILTER DATE_DIFF(doc.time, DATE_NOW(), 'h', true) < 12
	if (!parameters.to && !parameters.from) {
		bindVars['personnel'] = config.database.collections.personnel;
		query += `
				SORT doc.time DESC
				LIMIT 1

				let subjectDocument = DOCUMENT(doc.subject)

				let subject = {
					id: subjectDocument._id,
					name: HAS(subjectDocument, "givenName") ? CONCAT(subjectDocument.givenName, " ", subjectDocument.familyName) : subjectDocument.name,
					type: IS_SAME_COLLECTION(@personnel, subjectDocument) ? "personnel" : subjectDocument.type,
					callsigns: subjectDocument.callsigns
				}

				let sourceDocument = DOCUMENT(doc.source)
				let source = {
					id: sourceDocument._id,
					name: HAS(sourceDocument, "givenName") ? CONCAT(sourceDocument.givenName, " ", sourceDocument.familyName) : sourceDocument.name,
					type: IS_SAME_COLLECTION(@personnel, sourceDocument) ? "personnel" : sourceDocument.type,
					callsigns: sourceDocument.callsigns
				}

				RETURN UNSET(MERGE(doc, {
					id: doc._key,
					source: source,
					subject: subject
				}), '_id', '_key', '_rev')
			)
			return FIRST(position)`;
	} else {
		query += `
			return UNSET(MERGE(doc, {id: doc._key}), '_id', '_key', '_rev')`;
	}

	L.debug('Position query is', parameters, query, bindVars);

	try {
		const cursor = await db.query({
			query,
			bindVars
		});

		let positions = [];

		for await (const batch of cursor.batches) {
			for (let i = 0; i < batch.length; i++) {
				positions.push(batch[i]);
			}
		}

		return positions;
	} catch (error) {
		L.error(error);
		throw error;
	}
};

/**
 * Create an position
 *
 * @param {OL.Position} position Position to update
 */
export const createPosition = async (position) => {
	position = { ...position };
	const [subjectToType, subjectToId] = extractRelatedTo(position, 'subject');
	const [sourceToType, sourceToId] = extractRelatedTo(position, 'source');
	delete position.relatedTo;

	// Check the given position is valid
	if (!validate(position)) {
		throw new ValidationError('Invalid position given', validate.errors);
	}

	// Try to store
	if (db instanceof Promise) {
		await db;
	}

	if (config instanceof Promise) {
		await config;
	}

	if (position.id) {
		let id;
		({ id, ...position } = position);
		position._key = id;
	}

	// Check that the subjectTo object exists
	const subjectCollection = db.collection(config.database.collections[subjectToType]);

	if (!(await subjectCollection.documentExists(subjectToId))) {
		throw new ValidationError('Requires valid subject object');
	}

	// Check that the subjectTo object exists
	const sourceCollection = db.collection(config.database.collections[sourceToType]);

	if (!(await sourceCollection.documentExists(sourceToId))) {
		throw new ValidationError('Requires valid source object');
	}

	// TODO Check the position doesn't already exist

	const positionCollection = db.collection(config.database.collections.positions);

	const result = await positionCollection.save(position);
	L.info(`Created new position ${result._key} ${position.name}`);

	setTimeout(
		() =>
			updateSubscribers(
				{
					id: result._key,
					...position
				},
				'update'
			),
		0
	);

	return result;
};

/**
 * Handle a position share
 *
 * @param {string} token Token that shared the position
 * @param {PositionShare} position Shared position
 *
 */
export const handlePositionShare = async (token, positionShare) => {
	// TODO Validate authentication

	try {
		if (db instanceof Error) {
			initialiseDb();
		}
		if (db instanceof Promise) {
			await db;
		}

		if (config instanceof Promise) {
			await config;
		}
	} catch (error) {
		L.error('Error waiting for database and config', error);
		throw new Error('Error waiting for database and config');
	}

	// Get user
	let user;
	try {
		user = await db.collection(config.database.collections.personnel).document(token);
	} catch (error) {
		throw new ValidationError('Invalid token');
	}

	const position = {
		subject: user._id,
		source: user._id,
		...positionShare
	};

	return await createPosition(position);
};
