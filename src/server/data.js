/**
 * Clean object for returning in API etc by removing db-specific id fields
 *
 * @param {object} object Object to clean
 *
 * @returns {object} Cleaned object
 */
export const cleanForApi = (object) => {
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const { _id, _key, _rev, ...clean } = object;
	clean.id = _key;

	return clean;
};
