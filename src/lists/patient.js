export default {
	patientSignType: [
		{
			id: 'cutLaceration',
			label: 'Cut/Laceration'
		},
		{
			id: 'sprainStrain',
			label: 'Sprain/Strain'
		},
		{
			id: 'fracture',
			label: 'Fracture'
		},
		{
			id: 'breathingDifficulties',
			label: 'Breathing Difficulties'
		},
		{
			id: 'headSpineInjury',
			label: 'Head/Neck/Spine Injury'
		},
		{
			id: 'burn',
			label: 'Burn'
		},
		{
			id: 'heartIssue',
			label: 'Heart Issue'
		}
	],
	patientInjuryLocations: [
		'Head',
		'Neck',
		'Chest',
		'Back',
		'Left shoulder',
		'Left upper arm',
		'Left elbow',
		'Left forearm',
		'Left wrist',
		'Left hand',
		'Right shoulder',
		'Right upper arm',
		'Right elbow',
		'Right forearm',
		'Right wrist',
		'Right hand',
		'Pelvis',
		'Left upper leg',
		'Left knee',
		'Left lower leg',
		'Left ankle',
		'Left foot',
		'Right upper leg',
		'Right knee',
		'Right lower leg',
		'Right ankle',
		'Right foot'
	],
	patientFractureTypes: [
		{
			id: 'open',
			label: 'Open (bone through skin)'
		},
		{
			id: 'closed',
			label: 'Closed (bone not through skin)'
		},
		{
			id: 'complex',
			label: 'Complex (bone through skin and vessels or organs)'
		}
	],
	patientHeadInjuryTypes: [
		{
			id: 'open',
			label: 'Open (skin broken)'
		},
		{
			id: 'closed',
			label: 'Closed'
		}
	],
	patientBurnLevels: [
		{
			id: 'first',
			label: 'First (Superficial)'
		},
		{
			id: 'second',
			label: 'Second (Partial Thickness [dermis / very painful])'
		},
		{
			id: 'third',
			label: 'Third (Full Thickness [subcutaneous / dry and leathery]]'
		},
		{
			id: 'fourth',
			label: 'Fourth (includes bone and organs)'
		}
	],
	patientBleedingTypes: [
		{
			id: 'none',
			label: 'None'
		},
		{
			id: 'bruising',
			label: 'Bruising'
		},
		{
			id: 'stopped',
			label: 'Stopped'
		},
		{
			id: 'capillary',
			label: 'Capillary'
		},
		{
			id: 'venous',
			label: 'Venous (steady flow, dark red)'
		},
		{
			id: 'arterial',
			label: 'Artierial (spurting, bright red)'
		}
	]
};
