const levels = ['ERROR', 'WARN', 'LOG', 'INFO', 'DEBUG'];

let logLevel = levels.indexOf('LOG');

const setLogLevel = (level) => {
	if (typeof level === 'number') {
		logLevel = Math.max(0, Math.min(level, levels.length - 1));
	} else {
		level = levels.indexOf(level);
		if (level !== -1) {
			logLevel = level;
		}
	}

	return levels[logLevel];
};

/*TODO let givenLevel = process.env.LOG_LEVEL;
if (givenLevel) {
	const num = Number(givenLevel);
	if (!isNaN(num)) {
		setLogLevel(num);
	} else {
		setLogLevel(givenLevel);
	}
}
*/

/**
 * @callback LogFunction
 * @param {any[]} ...message Message to log
 */

/**
 * @typedef {Object} Logger
 * @param {string} level Log level
 * @param {any[]} ...message Message to log
 * @property {LogFunction} error Log error message
 * @property {LogFunction} warn Log warn message
 * @property {LogFunction} log Log log message
 * @property {LogFunction} info Log info message
 * @property {LogFunction} debug Log debug message
 */

/**
 * Create a new logger
 *
 * @param {string} id ID for new logger
 *
 * @returns {Logger}
 */
export default (id) => {
	const error = (...message) => {
		if (logLevel >= levels.indexOf('ERROR')) {
			// eslint-disable-next-line no-console
			console.error(id, ...message);
		}
	};
	const warn = (...message) => {
		if (logLevel >= levels.indexOf('WARN')) {
			// eslint-disable-next-line no-console
			console.warn(id, ...message);
		}
	};
	const log = (...message) => {
		if (logLevel >= levels.indexOf('LOG')) {
			// eslint-disable-next-line no-console
			console.log(id, ...message);
		}
	};
	const info = (...message) => {
		if (logLevel >= levels.indexOf('INFO')) {
			// eslint-disable-next-line no-console
			console.info(id, ...message);
		}
	};
	const debug = (...message) => {
		if (logLevel >= levels.indexOf('DEBUG')) {
			// eslint-disable-next-line no-console
			console.debug(id, ...message);
		}
	};

	/* @type {Logger} */
	const logger = (level, ...message) => {
		switch (level) {
			case 'ERROR':
				return error(...message);
			case 'WARN':
				return warn(...message);
			case 'INFO':
				return info(...message);
			case 'DEBUG':
				return debug(...message);
			case 'LOG':
			default:
				return log(...message);
		}
	};

	Object.assign(logger, {
		setLogLevel,
		error,
		warn,
		log,
		info,
		debug
	});

	Object.freeze(logger);

	return logger;
};
