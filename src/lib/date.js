const formatTimeOptions = {
	timeStyle: 'medium',
	hour12: false
};

const formatDateOptions = {
	dateStyle: 'short'
};

/**
 * Format a date to be used in a datetime-local input
 * (yyyy-mm-ddTHH:MM)
 *
 * @param {Date|string|number} date Date to format
 *
 * @returns {string} Formatted date
 */
export const toDatetimeLocal = (date) => {
	if (!(date instanceof Date)) {
		date = new Date(date);
	}

	return (
		date.getFullYear().toString().padStart(4, '0') +
		'-' +
		(date.getMonth() + 1).toString().padStart(2, '0') +
		'-' +
		date.getDate().toString().padStart(2, '0') +
		'T' +
		date.getHours().toString().padStart(2, '0') +
		':' +
		date.getMinutes().toString().padStart(2, '0')
	);
};

const _onSameDay = (date1, date2) => {
	return (
		date1.getFullYear() === date2.getFullYear() &&
		date1.getMonth() === date2.getMonth() &&
		date1.getDate() === date2.getDate()
	);
};

/**
 *
 * @param {Date|null} startTime Start Time
 * @param {Date|null} endTime End Time
 * @param {boolean} [returnArray] If true, will return array containing the
 *   formatted start and end time
 * @param {object} [options]
 * @param {object} [options.timeFormat]
 * @param {object} [options.dateFormat]
 *
 * @returns {string|[string|null, string|null]}
 */
export const formatTimeRange = (startTime, endTime, returnArray, options) => {
	let formatted = '';
	let time;
	let array;
	if (returnArray) {
		array = [];
	}
	let dateFormat = options?.dateFormat
		? {
				...formatDateOptions,
				...options.dateFormat
		  }
		: formatDateOptions;
	let timeFormat = options?.timeFormat
		? {
				...formatTimeOptions,
				...options.timeFormat
		  }
		: formatTimeOptions;
	const now = new Date();
	if (startTime) {
		if (!(startTime instanceof Date)) {
			startTime = new Date(startTime);

			if (isNaN(startTime)) {
				throw new Error('Invalid date');
			}
		}

		if (!_onSameDay(now, startTime)) {
			time = startTime.toLocaleDateString([], dateFormat) + ' ';
		} else {
			time = '';
		}

		time += startTime.toLocaleTimeString([], timeFormat);

		if (returnArray) {
			array.push(time);
		} else {
			formatted += time;
		}
	} else if (returnArray) {
		array.push(null);
	}

	if (!returnArray && startTime && endTime) {
		formatted += ' – ';
	}

	if (endTime) {
		if (!(endTime instanceof Date)) {
			endTime = new Date(endTime);

			if (isNaN(endTime)) {
				throw new Error('Invalid date');
			}
		}

		if (!_onSameDay(startTime, endTime)) {
			time = endTime.toLocaleDateString([], dateFormat) + ' ';
		} else {
			time = '';
		}

		time += endTime.toLocaleTimeString([], timeFormat);

		if (returnArray) {
			array.push(time);
		} else {
			formatted += time;
		}
	} else if (returnArray) {
		array.push(null);
	}

	if (returnArray) {
		return array;
	}

	return formatted;
};

export const formatTime = (date) => {
	let formatted = '';
	const now = new Date();
	if (date) {
		if (!(date instanceof Date)) {
			date = new Date(date);

			if (isNaN(date)) {
				throw new Error('Invalid date');
			}
		}

		if (!_onSameDay(now, date)) {
			formatted += date.toLocaleDateString([], formatDateOptions) + ' ';
		}

		formatted += date.toLocaleTimeString([], formatTimeOptions);
	}

	return formatted;
};

/** @typedef {object} TimedObject
 * @prop {string} [startTime] Start time of timed object
 * @prop {string} [endTime] End time of timed object
 * @prop {string} [time] Time of timed object
 */

/**
 * Sorting function for timed objects.
 *
 * Sorts based on time so more recent timed objects are earlier in array
 * and ongoing objects are first in array
 *
 * @param {TimedObject} a Object to test if should be before or after b object
 * @param {TimedObject} b Object to test against
 *
 * @returns -1 if a should be before b, 1 if a should be after b, 0 if they
 *   are the same (in terms of time)
 */
export const timedSort = (a, b) => {
	const aTime = a.endTime || a.time;
	const bTime = b.endTime || b.time;
	const aSTime = a.startTime || a.endTime || a.time;
	const bSTime = b.startTime || b.endTime || b.time;

	if (!aTime && !bTime) {
		if (bSTime > aSTime) return 1;
		if (bSTime < aSTime) return -1;
		if (bSTime === aSTime) return 0;
	}
	if (!bTime) return 1;
	if (!aTime) return -1;

	if (bTime > aTime) return 1;
	if (bTime < aTime) return -1;
	if (bTime === aTime) return 0;
};

/** @typedef {{ startTime: string, endTime: string}[]} Merged */

/**
 * Check and merge if required the next merged times if they now overlap
 * with the current date
 *
 * @param {Merged} merged Currently merged times
 * @param {number} i Current date index
 */
function checkNextMergedTimes(merged, i) {
	const date = merged[i];
	i++;
	while (i < merged.length) {
		const next = merged[i];

		if (next.startTime > date.endTime) {
			break;
		}

		if (!next.endTime) {
			date.endTime = null;

			merged.splice(i, 1);
			break;
		}

		if (next.endTime > date.endTime) {
			date.endTime = next.endTime;
		}

		merged.splice(i, 1);

		i++;
	}
}

/**
 * Merge times from objects into the minimum amount of time bounds. All times
 * should be ISO8601 formatted times in UTC
 *
 * @param {{ startTime: string, endTime: string, [key: string]: any}[]} objects
 *   Objects to merge the times from
 *
 * @returns {Merged}
 */
export function mergeTimes(objects) {
	const merged = [];

	object_loop: for (const object of objects) {
		let i = 0;
		while (i < merged.length) {
			const date = merged[i];
			// Check if object startTime is within date
			if (
				object.startTime >= date.startTime &&
				((date.endTime && object.startTime <= date.endTime) || !date.endTime)
			) {
				// Check if object extends date
				if (!date.endTime) {
					// Skip as contained
					continue object_loop;
				}
				if (
					(!object.endTime && object.endTime) ||
					(object.endTime && object.endTime > date.endTime)
				) {
					date.endTime = object.endTime;

					checkNextMergedTimes(merged, i);
				}

				continue object_loop;
			}

			// Check if date is within object
			if (
				date.startTime >= object.startTime &&
				((object.endTime && date.startTime <= object.endTime) || !object.endTime)
			) {
				date.startTime = object.startTime;

				// Check if date extends object
				if (!object.endTime) {
					date.endTime = null;
					continue object_loop;
				}

				// Extend endTime if object ends after date
				if (date.endTime && object.endTime > date.endTime) {
					date.endTime = object.endTime;

					// Check future merged dates don't overlapping
					checkNextMergedTimes(merged, i);
				}

				continue object_loop;
			}

			// Add new time range if earlier
			if (object.startTime < date.startTime) {
				merged.splice(i, 0, {
					startTime: object.startTime,
					endTime: object.endTime || null
				});

				continue object_loop;
			}

			i++;
		}

		// Add time to end of merged
		merged.push({
			startTime: object.startTime,
			endTime: object.endTime || null
		});
	}

	return merged;
}
