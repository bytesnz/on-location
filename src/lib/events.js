import { timedSort } from './date.js';

export const pointEvents = ['log'];

/**
 * Inserts event into correct position in event store
 *
 * @param {Event} event Event to insert
 * @param {Stored<Event>[]} eventStore Event store to insert event into
 */
export const insertEvent = (event, eventStore) => {
	const storedEvent = {
		id: event.id,
		localId: event._localId,
		data: event,
		unseen: true
	};
	for (let i = eventStore.length - 1; i >= 0; i--) {
		if (timedSort(event, eventStore[i].data) !== -1) {
			if (i === eventStore.length - 1) {
				eventStore.push(storedEvent);
			} else {
				eventStore.splice(i + 1, 0, storedEvent);
			}
			return;
		}
	}
	eventStore.unshift(storedEvent);
};

/**
 * Sorting function for event store objects from latest to earliest
 *
 * Sorts based on time so more recent timed objects are earlier in array
 * and ongoing objects are first in array
 *
 * @param {OL.DataObject<OL.Event>} a Object to test if should be before or after b object
 * @param {OL.DataObject<OL.Event>} b Object to test against
 *
 * @returns -1 if a should be before b, 1 if a should be after b, 0 if they
 *   are the same (in terms of time)
 */
export function eventSort() {
	const now = new Date().toISOString();

	return (a, b) => {
		const aEndTime = a.endTime || a.time || now;
		const bEndTime = b.endTime || b.time || now;
		const aStartTime = a.startTime || a.time;
		const bStartTime = b.startTime || b.time;
		const aStartsLater = aStartTime >= now;
		const bStartsLater = bStartTime >= now;

		if (aStartsLater && bStartsLater) {
			if (bStartTime > aStartTime) return 1;
			if (bStartTime < aStartTime) return -1;
			if (bEndTime > aEndTime) return 1;
			if (bEndTime < aEndTime) return -1;
			return 0;
		}

		if (aStartsLater) {
			return -1;
		}

		if (bStartsLater) {
			return 1;
		}

		if (bEndTime > aEndTime) return 1;
		if (bEndTime < aEndTime) return -1;
		if (bStartTime > aStartTime) return 1;
		if (bStartTime < aStartTime) return -1;
		return 0;
	};
}
