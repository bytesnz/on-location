const statuses = [];
const subscribers = [];

/**
 * Update subscribers with latest status messages
 */
const updateSubscribers = () => {
	for (let callback of subscribers) {
		callback(statuses.map((s) => s.status));
	}
};

/** @typedef {object} StatusOptions
 *
 * @prop {"error"|"warning"|"success"} [type] Type of status
 * @prop {number} [timeout] Timeout in milliseconds
 */

/** @typedef {object} Status
 *
 * @prop {string} message Status Message
 * @prop {StatusOptions} [options] Status options
 */

/**
 * Update a status message
 *
 * @param statusObject Status to update
 * @param {string} message Message to update the status with
 * @param {StatusOptions} options Options to update the status wi;th
 */
const updateStatus = (statusObject, message, options) => {
	const index = statuses.indexOf(statusObject);

	if (index === -1) {
		return;
	}

	statusObject.status = {
		message,
		options: {
			...(statusObject.status.options || {}),
			...(options || {})
		}
	};

	// Reset timeout
	if (options?.timeout) {
		if (statusObject.timeout) {
			clearTimeout(statusObject.timeout);
		}
		statusObject.timeout = setTimeout(() => {
			clearStatusFromObject(statusObject);
		}, options.timeout);
	}

	updateSubscribers();
};

/**
 * Add a status
 *
 * @param {string} message Status message
 * @param {StatusOptions} options Status options
 */
export const addStatus = (message, options) => {
	const status = {
		message,
		options
	};

	const statusObject = {
		status,
		timeout: null,
		update: null,
		clear: null
	};

	statusObject.update = (newMessage, newOptions) =>
		updateStatus(statusObject, newMessage, newOptions);
	statusObject.clear = () => clearStatusFromObject(statusObject);

	if (options?.timeout) {
		statusObject.timeout = setTimeout(() => {
			clearStatusFromObject(statusObject);
		}, options.timeout);
	}

	statuses.push(statusObject);

	updateSubscribers();

	return statusObject;
};

/**
 * Clear the given status message
 *
 * @param {StatusObject} statusObject Status to clear
 */
function clearStatusFromObject(statusObject) {
	const index = statuses.indexOf(statusObject);

	if (index !== -1) {
		statuses.splice(index, 1);
		updateSubscribers();
	}
}

/**
 * Clear the given status message
 *
 * @param {Status} status Status to clear
 */
export const clearStatus = (status) => {
	const index = statuses.findIndex((s) => s.status === status);

	if (index !== -1) {
		statuses.splice(index, 1);
		updateSubscribers();
	}
};

/** @type {import('svelte/store').Readable */
export const statusMessages = {
	subscribe: (callback) => {
		if (subscribers.indexOf(callback) === -1) {
			subscribers.push(callback);
		}

		callback(statuses.map((s) => s.status));

		return () => {
			const index = subscribers.indexOf(callback);

			if (index !== -1) {
				subscribers.splice(index, 1);
			}
		};
	}
};
