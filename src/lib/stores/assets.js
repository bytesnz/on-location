import Fuse from 'fuse.js';
import { base } from '$lib/api';
import { findFirstMatch } from '$lib/utils.js';

/** @typedef {object} Filter
 * Asset filter
 *
 * @property {string} [term] Fuzzy search term
 * @property {string[]} [type] Asset type
 */

let assetsStore = null;
let filteredAssetsStore = null;
let assetTypesStore = null;
const assetSubscribers = [];
const filteredAssetSubscribers = [];
/// @type {Filter}
let assetFilter = {};
let assetsFetch = null;

const fuseSearch = new Fuse([], {
	keys: ['name', 'type', 'callsigns'],
	threshold: 0.3
});

const updateSubscribers = () => {
	fuseSearch.setCollection(assetsStore);

	if (assetSubscribers.length) {
		for (let i = 0; i < assetSubscribers.length; i++) {
			assetSubscribers[i](assetsStore);
		}
	}

	if (filteredAssetSubscribers.length) {
		if (!assetsStore) {
			filteredAssetsStore = null;
		} else {
			filteredAssetsStore = filterAssets(assetFilter);
		}

		for (let i = 0; i < filteredAssetSubscribers.length; i++) {
			filteredAssetSubscribers[i]({
				assets: filteredAssetsStore,
				filter: assetFilter
			});
		}
	}
};

/**
 * Prefetch assets
 *
 * @param {} fetch Fetch to use to get assets
 */
export const fetchAssets = async (fetch) => {
	if (!assetsFetch) {
		assetsFetch = Promise.all([
			fetch(base + '/assets?radios&fixed=0').then(async (response) => {
				if (response.status !== 200) {
					let error = null;
					try {
						error = await response.json();
					} catch {
						error = null;
					}

					throw new Error(`Error ${response.status} ${error?.error || ''}`);
				}

				assetsStore = await response.json();
				filteredAssetsStore = filterAssets(assetFilter);

				assetsFetch = null;
			}),
			fetch(base + '/assetTypes').then(async (response) => {
				if (response.status !== 200) {
					let error = null;
					try {
						error = await response.json();
					} catch {
						error = null;
					}

					throw new Error(`Error ${response.status} ${error?.error || ''}`);
				}

				const types = await response.json();

				assetTypesStore = {};

				for (let i = 0; i < types.length; i++) {
					assetTypesStore[types[i].id] = types[i];
				}

				assetsFetch = null;
			})
		]);
	}

	return await assetsFetch.then(() => {
		updateSubscribers();
	});
};

/** @type {import('svelte/store').Readable}
 *
 * List of assets
 */
export const assets = {
	subscribe: (callback) => {
		if (assetSubscribers.indexOf(callback) === -1) {
			assetSubscribers.push(callback);
		}

		callback(assetsStore);

		return () => {
			const index = assetSubscribers.indexOf(callback);

			if (index !== -1) {
				assetSubscribers.splice(index, 1);
			}
		};
	}
};

const filterAssets = (filter) => {
	if (!assetsStore) {
		return null;
	}

	let assets = [];

	for (let i = 0; i < assetsStore.length; i++) {
		if (filter.type) {
			if (findFirstMatch(assetsStore[i].type, filter.type) === null) {
				continue;
			}
		}

		assets.push(assetsStore[i]);
	}

	if (filter.term) {
		const results = fuseSearch.search(filter.term);
		assets = [];

		for (let i = 0; i < results.length; i++) {
			assets.push(results[i].item);
		}
	}

	return assets;
};

/**
 * Filtered list of assets
 */
export const filteredAssets = {
	/**
	 * Subscribe to filtered assets list
	 *
	 * @param {(assets: Assets[]) => void} callback Callback to call when
	 *   assets update
	 */
	subscribe: (callback) => {
		if (filteredAssetSubscribers.indexOf(callback) === -1) {
			filteredAssetSubscribers.push(callback);
		}

		callback({
			filter: assetFilter,
			assets: filteredAssetsStore
		});

		return () => {
			const index = filteredAssetSubscribers.indexOf(callback);

			if (index !== -1) {
				filteredAssetSubscribers.splice(index, 1);
			}
		};
	},
	/**
	 * Set the current asset filter
	 *
	 * @param {Filter} filter Asset filter to use
	 */
	set: (filter) => {
		assetFilter = filter;

		if (assetsStore) {
			updateSubscribers();
		}
	},
	/**
	 * Update the current filter using an updater function
	 *
	 * @param {(filter: Filter) => Filter} updater Updater function
	 */
	update: (updater) => {
		assetFilter = updater(assetFilter);

		if (assetsStore) {
			updateSubscribers();
		}
	},
	/**
	 * Reset the current asset filter
	 */
	reset: () => {
		assetFilter = {};

		if (assetsStore) {
			updateSubscribers();
		}
	}
};
