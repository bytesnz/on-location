import { currentIncident } from './incidents';
import { subscribe } from '$lib/socket';
import { browser } from '$app/env';
import { base } from '$lib/api';
import Logger from '$lib/log';
import { makeId, removeEmptyValues } from '$lib/utils';

const L = Logger('store:geometries');

/** @typedef {(geometries: OL.DataObject<OL.Geometry>[]) => void} Subscriber */

/** @type {OL.DataObject<OL.Geometry>[]} */
let geometriesStore = null;
let geometriesFetch = null;
/* @type {Subscriber[]} */
const geometrySubscribers = [];
let geometriesSubscription = null;

/**
 * Create a new geometry
 *
 * @param {OL.DataObject<OL.Geometry>} [geometryObject] New geometry object
 * @param {boolean} [dontSave] If true, geometry will not be submitted to
 *   server
 */
export const updateGeometry = (geometryObject, dontSave) => {
	let index;
	let promise;

	if (!geometryObject.data.relatedTo) {
		throw new Error('Need relation to save the geometry');
	}

	/* TODO Don't add to list if not related to current incident
	if (geometryObject.id) {
		return saveGeometry(geometryObject);
	}*/

	geometryObject.data = removeEmptyValues(geometryObject.data);

	if (!dontSave) {
		promise = saveGeometry(geometryObject);
	}

	let newGeometriesStore;

	if (geometriesStore) {
		newGeometriesStore = [...geometriesStore];

		for (index = 0; index < geometriesStore.length; index++) {
			if (
				(geometryObject.localId && geometriesStore[index].localId === geometryObject.localId) ||
				(geometryObject.id && geometriesStore[index].id === geometryObject.id)
			) {
				break;
			}
		}

		if (index === geometriesStore.length) {
			newGeometriesStore.push(geometryObject);
		} else {
			newGeometriesStore[index] = geometryObject;
		}
	} else {
		newGeometriesStore = [geometryObject];
	}

	geometriesStore = newGeometriesStore;

	updateSubscribers();

	return promise;
};

/**
 * Save an geometry
 *
 * @param {OL.DataObject<OL.Geometry>} geometryObject
 */
const saveGeometry = (geometryObject) => {
	const geometry = geometryObject.data;

	if (geometryObject.localId) {
		geometry._localId = geometryObject.localId;
	}

	const fetchOptions = {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(geometry)
	};

	// TODO Once saved, will show up in $incidentGeometries
	geometryObject.status = 'saving';

	if (geometryObject.id) {
		fetchOptions.method = 'PATCH';
	}

	return fetch(`${base}/geometries`, fetchOptions).then(async (response) => {
		let data;
		try {
			data = await response.json();
		} catch (error) {
			void 1;
		}

		if (response.status >= 400) {
			if (data?.error) {
				return Promise.reject(new Error(data.error));
			}

			return Promise.reject(new Error('Error received from request'));
		}

		if (!geometryObject.id && response.status === 201) {
			geometryObject = {
				...geometryObject,
				id: data.id,
				data: {
					...geometryObject.data,
					id: data.id
				}
			};
			geometryObject.unsaved = false;
			geometryObject.unseen = false;
			if (geometryObject.status === 'saving') {
				geometryObject.status = null;
			}

			updateWithGeometryObject(geometryObject);
		} else if (geometryObject.id && response.status === 200) {
			geometryObject.unsaved = false;
			geometryObject.unseen = false;
			if (geometryObject.status === 'saving') {
				geometryObject.status = null;
			}

			updateWithGeometryObject(geometryObject);
		}
	});
};

/**
 * Add/update an geometryObject to/in the geometries store
 *
 * @param {OL.DataObject<OL.Geometry>} geometryObject
 */
const updateWithGeometryObject = (geometryObject) => {
	/* TODO Need to see if relatedTo values are relate to incident somehow
	if (!incident || geometryObject.data.incident !== incident.id) {
		return;
	}
	*/

	if (!geometriesStore) {
		geometriesStore = [geometryObject];
	} else {
		const newGeometriesStore = [...geometriesStore];
		let i;
		for (i = 0; i < newGeometriesStore.length; i++) {
			if (
				(geometryObject.localId && newGeometriesStore[i].localId === geometryObject.localId) ||
				(geometryObject.id && newGeometriesStore[i].id === geometryObject.id)
			) {
				newGeometriesStore[i] = geometryObject;
				break;
			}
		}

		if (i === newGeometriesStore.length) {
			newGeometriesStore.push(geometryObject);
		}

		geometriesStore = newGeometriesStore;
	}

	updateSubscribers();
};

export const fetchGeometries = (fetch) => {
	L.debug('Fetching geometries');
	if (!incident) {
		return;
	}

	geometriesFetch = fetch(`${base}/geometries?incident=${incident.id}`).then(async (response) => {
		L.debug('Got response', response);
		if (response.status !== 200) {
			let details = null;
			try {
				details = await response.json();
			} catch (error) {
				void 1;
			}

			// TODO Handle error
			geometriesFetch = null;
		}

		const geometries = await response.json();

		geometriesStore = [];

		for (let i = 0; i < geometries.length; i++) {
			geometriesStore.push({
				id: geometries[i].id,
				data: geometries[i]
			});
		}

		updateSubscribers();

		geometriesFetch = null;
		return geometriesStore;
	});
	return geometriesFetch;
};

let incident = null;
let currentIncidentSubscription = null;
const checkCurrentIncident = ($currentIncident) => {
	if (incident !== $currentIncident) {
		if (geometriesStore) {
			geometriesStore = null;
		}
		if (geometriesFetch) {
			geometriesFetch.then(() => {
				geometriesStore = null;
			});
			geometriesFetch = null;
		}
		updateSubscribers();

		if (browser) {
			if (geometriesSubscription) {
				geometriesSubscription();
				geometriesSubscription = null;
			}
			if ($currentIncident) {
				geometriesSubscription = subscribe(
					'geometries:subscribe',
					{
						incident: $currentIncident.id
					},
					handleGeometriesMessage
				);
			}
		}

		incident = $currentIncident;
	}
};

const updateSubscribers = () => {
	if (geometrySubscribers.length) {
		for (let i = 0; i < geometrySubscribers.length; i++) {
			geometrySubscribers[i](geometriesStore);
		}
	}
};

const handleGeometriesMessage = (type, data) => {
	switch (type) {
		case 'geometries': {
			/* @type Geometry[] */
			const geometries = data;
			if (geometriesStore === null) {
				geometriesStore = [];

				for (let i = 0; i < data.length; i++) {
					geometriesStore.push({
						id: data[i].id,
						data: data[i]
					});
				}
			} else {
				geometriesStore = [...geometriesStore];
				// Merge into existing geometries
				for (let i = 0; i < data.length; i++) {
					let j = 0;
					while (j < geometriesStore.length) {
						if (data[i].id === geometriesStore[j].id) {
							geometriesStore[j] = {
								...geometriesStore[j],
								unseen: true,
								data: data[i]
							};
							break;
						}
						j++;
					}
					if (j === geometriesStore.length) {
						geometriesStore.push({
							id: data[i].id,
							unseen: true,
							data: data[i]
						});
					}
				}
			}
			break;
		}
		case 'geometry':
			if (geometriesStore === null) {
				geometriesStore = [
					{
						id: data.id,
						data,
						unseen: true
					}
				];
			} else {
				geometriesStore = [...geometriesStore];
				let j = 0;
				while (j < geometriesStore.length) {
					if (
						(data._localId && data._localId === geometriesStore[j].localId) ||
						data.id === geometriesStore[j].id
					) {
						geometriesStore[j] = {
							...geometriesStore[j],
							id: data.id,
							data,
							unseen: true
						};
						break;
					}
					j++;
				}
				if (j === geometriesStore.length) {
					geometriesStore.push({
						id: data.id,
						data,
						unseen: true
					});
				}
			}
			break;
	}

	updateSubscribers();
};

export const incidentGeometries = {
	get fetch() {
		return geometriesFetch;
	},
	waitForFetch: async () => {
		if (geometriesFetch instanceof Promise) {
			await geometriesFetch;
		}
	},
	/**
	 * Subscribe to geometries
	 *
	 * @param {Subscriber} callback
	 *   Callback to call when geometries are changed
	 */
	subscribe: (callback) => {
		geometrySubscribers.push(callback);
		callback(geometriesStore);
		//if (geometriesStore === null && geometriesFetch === null) {
		//  fetchGeometries();
		//}
		if (!currentIncidentSubscription) {
			currentIncidentSubscription = currentIncident.subscribe(checkCurrentIncident);
		}
		return () => {
			const index = geometrySubscribers.indexOf(callback);
			if (index !== -1) {
				geometrySubscribers.splice(index, 1);
			}
			if (geometrySubscribers.length === 0 && currentIncidentSubscription) {
				currentIncidentSubscription();
				currentIncidentSubscription = null;
			}
		};
	}
};
