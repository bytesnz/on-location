import { currentIncident } from './incidents';
import { base } from '$lib/api';
import { browser } from '$app/env';
import { subscribe } from '$lib/socket';

/** @typedef {object} Assignment
 *
 * @prop {any} subject ID of assigned
 * @prop {OL.Assignment} assignments Array of related assignment history
 * @prop {OL.Assignment} current Current assignment information
 */

const assignmentsSubscribers = [];
const currentIncidentAssignmentsSubscribers = [];
let currentIncidentStore = null;
/** @type {object}
 *
 * @prop {Assignment[]} assignments
 * @prop {Assignment[]} coinciding
 */
let currentIncidentAssignmentsStore = null;

let currentIncidentSubscription = null;
let currentIncidentAssignmentsSubscription = null;

/**
 * Check if an assignment is current
 *
 * @param {OL.Assignment} assignment Assignement to check if it is the
 *   current assignment
 *
 * @param {Date} [time] The "current" time to use
 *
 * @returns {boolean}
 */
export const assignmentIsCurrent = (assignment, time) => {
	if (!time) {
		time = new Date();
	}
	time = time.toISOString();

	return time > assignment.startTime && (!assignment.endTime || time < assignment.endTime);
};

/**
 * Sort assignments based on time
 *
 * @param a Assignment
 * @param b Assignment
 *
 * @returns {number} Sort number
 */
export const sortAssignments = (a, b) => {
	if (!a.endTime && !b.endTime) {
		return a.startTime < b.startTime;
	}
	if (a.endTime & b.endTime) {
		return a.endTime < b.endTime;
	}
	if (b.endTime) {
		return 1;
	}
	if (a.endTime) {
		return -1;
	}
};

/**
 * Sort coinciding assignments based on time
 *
 * @param a Assignment
 * @param b Assignment
 *
 * @returns {number} Sort number
 */
function sortCoincidingAssignments(a, b) {
	return sortAssignments(a.current || a.assignments[0], b.current || b.assignments[0]);
}

const updateCurrentIncidentAssignmentSubscribers = () => {
	if (currentIncidentAssignmentsSubscribers.length) {
		for (let i = 0; i < currentIncidentAssignmentsSubscribers.length; i++) {
			currentIncidentAssignmentsSubscribers[i](currentIncidentAssignmentsStore);
		}
	}
};

const handleAssignmentsMessage = (topic, data) => {
	const [, type] = topic.split(':');
	switch (type) {
		case 'list':
			currentIncidentAssignmentsStore = {
				assignments: [],
				coinciding: []
			};

			for (const assignment of data.assignments) {
				const index = currentIncidentAssignmentsStore.assignments.findIndex(
					(a) => a.subject.id === assignment.subject.id
				);

				if (index === -1) {
					const assignments = {
						subject: assignment.subject,
						assignments: [assignment],
						current: null
					};

					if (assignmentIsCurrent(assignment)) {
						assignments.current = assignment;
					}

					currentIncidentAssignmentsStore.assignments.push(assignments);
				} else {
					currentIncidentAssignmentsStore.assignments[index].assignments.push(assignment);

					if (assignmentIsCurrent(assignment)) {
						currentIncidentAssignmentsStore.assignments[index].current = assignment;
					}
				}
			}

			for (const assignment of currentIncidentAssignmentsStore.assignments) {
				assignment.assignments.sort(sortAssignments);
			}

			for (const assignment of data.coinciding) {
				const index = currentIncidentAssignmentsStore.coinciding.findIndex(
					(a) => a.subject.id === assignment.subject.id
				);

				if (index === -1) {
					const assignments = {
						subject: assignment.subject,
						assignments: [assignment],
						current: null
					};

					if (assignmentIsCurrent(assignment)) {
						assignments.current = assignment;
					}

					currentIncidentAssignmentsStore.coinciding.push(assignments);
				} else {
					currentIncidentAssignmentsStore.coinciding[index].assignments.push(assignment);

					if (assignmentIsCurrent(assignment)) {
						currentIncidentAssignmentsStore.coinciding[index].current = assignment;
					}
				}
			}

			for (const assignment of currentIncidentAssignmentsStore.coinciding) {
				assignment.assignments.sort(sortAssignments);
			}

			updateCurrentIncidentAssignmentSubscribers();
			break;
		case 'update': {
			const newStore = {
				assignments: [...(currentIncidentAssignmentsStore.assignments || [])],
				coinciding: [...(currentIncidentAssignmentsStore.coinciding || [])]
			};

			const objects = Array.isArray(data) ? data : [data];

			for (const object of objects) {
				// Check if targets current incident
				// TODO Include incident phases
				// TODO Need to ensure incidents/ mapped
				if (object.target.id === `incidents/${currentIncidentStore.id}`) {
					const index = newStore.assignments.findIndex((a) => a.subject.id === object.subject.id);

					if (index === -1) {
						const assignment = {
							subject: object.subject,
							assignments: [object],
							current: null
						};

						if (assignmentIsCurrent(object)) {
							assignment.current = object;
						}

						newStore.assignments.push(assignment);
					} else {
						const assignmentIndex = newStore.assignments[index].assignments.findIndex(
							(a) => a.id === object.id
						);

						if (assignmentIndex === -1) {
							newStore.assignments[index].assignments.push(object);
						} else {
							newStore.assignments[index].assignments[assignmentIndex] = object;
						}

						if (assignmentIsCurrent(object)) {
							newStore.assignments[index].current = object;
						} else {
							if (newStore.assignments[index].current.id === object.id) {
								newStore.assignments[index].current = object;
							}
							if (!assignmentIsCurrent(newStore.assignments[index].current)) {
								newStore.assignments[index].current = null;
							}
						}
					}
				} else {
					const index = newStore.coinciding.findIndex((a) => a.subject.id === object.subject.id);

					if (index === -1) {
						const assignment = {
							subject: object.subject,
							assignments: [object],
							current: null
						};

						if (assignmentIsCurrent(object)) {
							assignment.current = object;
						}

						newStore.coinciding.push(assignment);
					} else {
						const assignmentIndex = newStore.coinciding[index].assignments.findIndex(
							(a) => a.id === object.id
						);

						if (assignmentIndex === -1) {
							newStore.coinciding[index].assignments.push(object);
						} else {
							newStore.coinciding[index].assignments[assignmentIndex] = object;
						}

						if (assignmentIsCurrent(object)) {
							newStore.coinciding[index].current = object;
						} else {
							if (newStore.coinciding[index].current.id === object.id) {
								newStore.coinciding[index].current = object;
							}
							if (!assignmentIsCurrent(newStore.coinciding[index].current)) {
								newStore.coinciding[index].current = null;
							}
						}
					}
				}
			}

			for (const assignment of newStore.assignments) {
				assignment.assignments.sort(sortAssignments);
			}

			currentIncidentAssignmentsStore = newStore;
			updateCurrentIncidentAssignmentSubscribers();

			return;
		}
	}
};

const updateCurrentIncidentAssignments = async (incident) => {
	if (incident === currentIncidentStore) {
		return;
	}

	currentIncidentStore = incident;

	if (!incident) {
		currentIncidentAssignmentsStore = null;
	} else {
		// Get assignments for incident
		if (browser) {
			currentIncidentAssignmentsSubscription = subscribe(
				'assignments:subscribe',
				{
					incident: incident.id,
					coinciding: true,
					currentOnly: true
				},
				handleAssignmentsMessage
			);
			const response = await fetch(
				`${base}/assignments?incident=${incident.id}&coinciding&currentOnly`
			);

			if (response.status !== 200) {
				let details = null;
				try {
					details = await response.json();
				} catch (error) {
					void 1;
				}

				return;
			}

			handleAssignmentsMessage('assignments:list', await response.json());
		}
	}

	updateCurrentIncidentAssignmentSubscribers();
};

/** @type {import('svelte/store').Readable} */
export const currentIncidentAssignments = {
	subscribe: (callback) => {
		const index = currentIncidentAssignmentsSubscribers.indexOf(callback);

		if (index === -1) {
			currentIncidentAssignmentsSubscribers.push(callback);
		}

		callback(currentIncidentAssignmentsStore);

		if (currentIncidentSubscription === null) {
			currentIncidentSubscription = currentIncident.subscribe(updateCurrentIncidentAssignments);
		}

		return () => {
			const index = currentIncidentAssignmentsSubscribers.indexOf(callback);

			if (index !== -1) {
				currentIncidentAssignmentsSubscribers.splice(index, 1);
			}

			if (currentIncidentSubscription && currentIncidentAssignmentsSubscribers.length === 0) {
				currentIncidentSubscription();
				currentIncidentSubscription = null;
			}
		};
	}
};
