import { sendMessage, subscribe } from '$lib/socket';
import { browser } from '$app/env';

const subscriptions = [];

// The web socket requests that are waiting
const requests = [];

let servicesStore = null;
let subscription = null;

const updateSubscribers = () => {
	for (let callback of subscriptions) {
		callback(servicesStore);
	}
};

const handleServices = (type, data, id) => {
	switch (type) {
		case 'services:update':
			servicesStore = {
				...(servicesStore || {}),
				...data
			};

			for (const callback of subscriptions) {
				callback(servicesStore);
			}
			break;
		case 'services:request:ack':
		case 'services:request:update':
		case 'services:request:error': {
			const index = requests.findIndex((r) => r.id === id);

			if (index === -1) {
				break;
			}

			const responseType = type.slice(type.lastIndexOf(':') + 1);

			const request = requests[index];

			if (request.callback) {
				request.callback(responseType, data);
			}
			switch (responseType) {
				case 'update':
					if (data.status === 'complete') {
						request.resolve(data.data);
						requests.splice(index, 1);
						return true;
					}
					break;
				case 'error':
					request.reject(data);
					requests.splice(index, 1);
					return true;
			}
		}
	}

	return false;
};

export const services = {
	subscribe: (callback) => {
		if (browser && servicesStore === null) {
			subscription = subscribe('services:subscribe', null, handleServices);
		}

		if (subscriptions.indexOf(callback) === -1) {
			subscriptions.push(callback);
		}

		callback(servicesStore);

		return () => {
			const index = subscriptions.indexOf(callback);

			if (index !== -1) {
				subscriptions.splice(index, 1);
			}
		};
	}
};

/**
 * Make a request to a service
 *
 * @param {object} request Request data
 *
 * @returns {Promise} A Promise that resolves with the result from the request
 *   once it is completed
 */
export function makeServiceRequest(request) {
	return new Promise((resolve, reject) => {
		if (request.serviceType === 'map27') {
			const service = servicesStore?.map27?.radios.find((s) => s.serviceId === request.id);

			if (!service) {
				return reject(new Error('Unknown service'));
			}

			const id = sendMessage(
				'services:request',
				{
					id: request.id,
					data: request.data
				},
				handleServices
			);

			requests.push({
				id,
				resolve,
				reject,
				callback: request.callback
			});
		} else {
			reject('Unknown service type');
		}
	});
}
