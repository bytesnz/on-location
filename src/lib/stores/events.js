import { currentIncident } from './incidents';
import { subscribe } from '$lib/socket';
import { browser } from '$app/env';
import { base } from '$lib/api';
import { insertEvent } from '$lib/events';
import { makeId, removeEmptyValues } from '$lib/utils';
import { writable, derived } from 'svelte/store';
import Logger from '$lib/log';

const L = Logger('store:events');

/** @typedef {(events: OL.DataObject<OL.Event>[]) => void} Subscriber */

/** @type {OL.DataObject<OL.Event>[]} */
let eventsStore = null;
let eventsFetch = null;
/* @type {Subscriber[]} */
const eventSubscribers = [];
let eventsSubscription = null;

/**
 * Sorting function for event store objects from oldest to newest.
 *
 * Sorts based on time so more recent timed objects are earlier in array
 * and ongoing objects are first in array
 *
 * @param {OL.DataObject<OL.Event>} a Object to test if should be before or after b object
 * @param {OL.DataObject<OL.Event>} b Object to test against
 *
 * @returns -1 if a should be before b, 1 if a should be after b, 0 if they
 *   are the same (in terms of time)
 */
export function eventSort() {
	const now = new Date().toISOString();

	return (a, b) => {
		const aEndTime = a.data.endTime || a.data.time || now;
		const bEndTime = b.data.endTime || b.data.time || now;
		const aStartTime = a.data.startTime || a.data.time;
		const bStartTime = b.data.startTime || b.data.time;
		const aStartsLater = aStartTime >= now;
		const bStartsLater = bStartTime >= now;

		if (aStartsLater && bStartsLater) {
			if (bStartTime > aStartTime) return 1;
			if (bStartTime < aStartTime) return -1;
			if (bEndTime > aEndTime) return 1;
			if (bEndTime < aEndTime) return -1;
			return 0;
		}

		if (aStartsLater) {
			return -1;
		}

		if (bStartsLater) {
			return 1;
		}

		if (bEndTime > aEndTime) return 1;
		if (bEndTime < aEndTime) return -1;
		if (bStartTime > aStartTime) return 1;
		if (bStartTime < aStartTime) return -1;
		return 0;
	};
}

/**
 * Create a new incident event
 *
 * @param {OL.DataObject<OL.Event>} [eventObject] New event object
 * @param {boolean} [dontSave] If true, event will not be submitted to
 *   server
 */
export const createEvent = (eventObject, dontSave) => {
	if (!incident) return false;

	if (!eventObject) {
		dontSave = true;

		const id = makeId();
		eventObject = {
			unsaved: true,
			status: 'unsaved',
			localId: id,
			data: {
				_localId: id,
				relatedTo: `incidents/${incident.id}`
			}
		};
	} else if (!eventObject.id && !eventObject.localId) {
		eventObject.localId = makeId();
	}

	/* TODO Don't add to list if not related to current incident
	if (!eventObject.data.incident) {
		eventObject.data.incident = incident.id;
	}
	*/

	eventObject.data = removeEmptyValues(eventObject.data);

	if (!dontSave) {
		saveEvent(eventObject);
	}

	let newEvents = [...(eventsStore || []), eventObject];

	newEvents.sort(eventSort());

	eventsStore = newEvents;

	// Update subscribers
	updateSubscribers();
};

/**
 * Save updated event
 *
 * @param {OL.DataObject<OL.Event>} [eventObject] New event object
 * @param {boolean} [dontSave] If true, event will not be submitted to
 *   server
 */
export const updateEvent = (eventObject, dontSave) => {
	let index;

	/* TODO Need to rewrite to not use .incident
	if (!incident || incident.id != eventObject.data.incident) {
		return saveEvent(eventObject);
	}*/

	if (!eventsStore) {
		return createEvent(eventObject, dontSave);
	}

	eventObject.data = removeEmptyValues(eventObject.data);

	for (index = 0; index < eventsStore.length; index++) {
		if (
			(eventObject.localId && eventsStore[index].localId === eventObject.localId) ||
			(eventObject.id && eventsStore[index].id === eventObject.id)
		) {
			break;
		}
	}

	if (index === eventsStore.length) {
		return createEvent(eventObject, dontSave);
	}

	if (!dontSave) {
		saveEvent(eventObject);
	}

	const newEventsStore = [...eventsStore];

	newEventsStore[index] = {
		...eventObject,
		status: 'saving'
	};

	eventsStore = newEventsStore;

	updateSubscribers();
};

/**
 * Save an event
 *
 * @param {OL.DataObject<OL.Event>} eventObject
 */
const saveEvent = (eventObject) => {
	const event = eventObject.data;

	if (eventObject.localId) {
		event._localId = eventObject.localId;
	}

	const fetchOptions = {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(removeEmptyValues(event))
	};

	// TODO Once saved, will show up in $incidentEvents
	eventObject.status = 'saving';

	if (eventObject.id) {
		fetchOptions.method = 'PATCH';
	}

	fetch(`${base}/events`, fetchOptions).then(async (response) => {
		let data;
		try {
			data = await response.json();
		} catch (error) {
			void 1;
		}

		if (!eventObject.id && response.status === 201) {
			eventObject = {
				...eventObject,
				id: data.id,
				data: {
					...eventObject.data,
					id: data.id
				}
			};
			eventObject.unsaved = false;
			eventObject.unseen = false;
			if (eventObject.status === 'saving') {
				eventObject.status = null;
			}

			updateWithEventObject(eventObject);
		} else if (eventObject.id && response.status === 200) {
			eventObject.unsaved = false;
			eventObject.unseen = false;
			if (eventObject.status === 'saving') {
				eventObject.status = null;
			}

			updateWithEventObject(eventObject);
		}
	});
};

/**
 * Delete a log
 *
 * @param {OL.DataObject<OL.Event> eventObject
 */
export function deleteEvent(eventObject) {
	if (eventObject.localId && !eventObject.id) {
		const newEventStore = [...eventsStore];
		const index = newEventStore.findIndex((e) => e === eventObject);

		if (index !== -1) {
			newEventStore.splice(index, 1);
			eventsStore = newEventStore;

			updateSubscribers();
			return;
		}
		return;
	}

	throw new Error("Can't delete saved events yet");
}

/**
 * Add/update an eventObject to/in the events store
 *
 * @param {OL.DataObject<OL.Event>} eventObject
 */
const updateWithEventObject = (eventObject) => {
	/* TODO Need to see if relatedTo values are relate to incident somehow
	if (!incident || eventObject.data.incident !== incident.id) {
		return;
	}
	*/

	if (!eventsStore) {
		eventsStore = [eventObject];
	} else {
		const newEventsStore = [...eventsStore];
		let i;
		for (i = 0; i < newEventsStore.length; i++) {
			if (
				(eventObject.localId && newEventsStore[i].localId === eventObject.localId) ||
				(eventObject.id && newEventsStore[i].id === eventObject.id)
			) {
				newEventsStore[i] = eventObject;
				break;
			}
		}

		if (i === newEventsStore.length) {
			newEventsStore.push(eventObject);
		}

		eventsStore = newEventsStore;
	}

	updateSubscribers();
};

export const fetchEvents = (fetch) => {
	if (!incident) {
		return;
	}

	eventsFetch = fetch(`${base}/events?incident=${incident.id}`).then(async (response) => {
		L.debug('Got response', response);
		if (response.status !== 200) {
			let details = null;
			try {
				details = await response.json();
			} catch (error) {
				void 1;
			}

			// TODO Handle error
			eventsFetch = null;
		}

		const events = await response.json();

		eventsStore = [];

		for (let i = 0; i < events.length; i++) {
			eventsStore.push({
				id: events[i].id,
				data: events[i]
			});
		}

		updateSubscribers();

		eventsFetch = null;
		return eventsStore;
	});
	return eventsFetch;
};

let incident = null;
let currentIncidentSubscription = null;
const checkCurrentIncident = ($currentIncident) => {
	if (incident !== $currentIncident) {
		if (eventsStore) {
			eventsStore = null;
		}
		if (eventsFetch) {
			eventsFetch.then(() => {
				eventsStore = null;
			});
			eventsFetch = null;
		}
		updateSubscribers();

		if (browser) {
			if (eventsSubscription) {
				eventsSubscription();
				eventsSubscription = null;
			}
			if ($currentIncident) {
				eventsSubscription = subscribe(
					'events:subscribe',
					{
						incident: $currentIncident.id
					},
					handleEventsMessage
				);
			}
		}

		incident = $currentIncident;
	}
};

const updateSubscribers = () => {
	if (eventSubscribers.length) {
		for (let i = 0; i < eventSubscribers.length; i++) {
			eventSubscribers[i](eventsStore);
		}
	}
};

const handleEventsMessage = (type, data) => {
	switch (type) {
		case 'events': {
			/* @type Event[] */
			if (eventsStore === null) {
				eventsStore = [];

				for (let i = 0; i < data.length; i++) {
					eventsStore.push({
						id: data[i].id,
						data: data[i]
					});
				}
			} else {
				eventsStore = [...eventsStore];
				// Merge into existing events
				for (let i = 0; i < data.length; i++) {
					let j = 0;
					while (j < eventsStore.length) {
						if (data[i].id === eventsStore[j].id) {
							eventsStore[j] = {
								...eventsStore[j],
								unseen: true,
								data: data[i]
							};
							break;
						}
						j++;
					}
					if (j === eventsStore.length) {
						insertEvent(data[i], eventsStore);
					}
				}
			}
			break;
		}
		case 'event':
			if (eventsStore === null) {
				eventsStore = [
					{
						id: data.id,
						data,
						unseen: true
					}
				];
			} else {
				eventsStore = [...eventsStore];
				let j = 0;
				while (j < eventsStore.length) {
					if (
						(data._localId && data._localId === eventsStore[j].localId) ||
						data.id === eventsStore[j].id
					) {
						eventsStore[j] = {
							...eventsStore[j],
							id: data.id,
							data,
							unseen: true
						};
						break;
					}
					j++;
				}

				if (j === eventsStore.length) {
					insertEvent(data, eventsStore);
				}
			}
			break;
	}

	updateSubscribers();
};

export const incidentEvents = {
	get fetch() {
		return eventsFetch;
	},
	waitForFetch: async () => {
		if (eventsFetch instanceof Promise) {
			await eventsFetch;
		}
	},
	/**
	 * Subscribe to events
	 *
	 * @param {Subscriber} callback
	 *   Callback to call when events are changed
	 */
	subscribe: (callback) => {
		eventSubscribers.push(callback);
		callback(eventsStore);
		//if (eventsStore === null && eventsFetch === null) {
		//  fetchEvents();
		//}
		if (!currentIncidentSubscription) {
			currentIncidentSubscription = currentIncident.subscribe(checkCurrentIncident);
		}
		return () => {
			const index = eventSubscribers.indexOf(callback);
			if (index !== -1) {
				eventSubscribers.splice(index, 1);
			}
			if (eventSubscribers.length === 0 && currentIncidentSubscription) {
				currentIncidentSubscription();
				currentIncidentSubscription = null;
			}
		};
	}
};

/*TODO Add changing of incident id resets currentEvent value*/
const currentEventIdStore = writable(null);

export const currentEventId = derived(
	currentEventIdStore,
	($currentEventIdStore) => $currentEventIdStore
);

/**
 * Set current event ID
 *
 * @param {string} id ID of current event
 */
export const setCurrentEvent = (id) => {
	currentEventIdStore.set(id);
};

/**
 * Current event
 */
export const currentEvent = derived(
	[currentEventIdStore, incidentEvents],
	([$currentEventIdStore, $incidentEvents]) => {
		if (!$currentEventIdStore || !$incidentEvents) {
			return null;
		}

		for (let i = 0; i < $incidentEvents.length; i++) {
			if (
				($incidentEvents[i].localId && $incidentEvents[i].localId === $currentEventIdStore) ||
				$incidentEvents[i].id === $currentEventIdStore
			) {
				return $incidentEvents[i];
			}
		}

		return null;
	}
);
