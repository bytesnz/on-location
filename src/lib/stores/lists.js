import { derived } from 'svelte/store';

/**
 * @typedef {object} ListItemObject
 *
 * @property {any} value Value
 * @property {string} label Label for value
 */

/** @typedef {ListItemObject|string} ListItem */

/** @typedef {ListItem[]} List */

/** @typedef {{[key]: List}} ListStore */

/** @type {ListStore} */
let listsStore = {};
const subscribers = [];

/** @type {import('svelte/store').Readable<ListStore>} */
export const lists = {
	subscribe: (callback) => {
		subscribers.push(callback);
		callback(listsStore);

		return () => {
			const index = subscribers.indexOf(callback);
			if (index != -1) {
				subscribers.splice(index, 1);
			}
		};
	}
};

/**
 * Set value of list
 *
 * @param {string} name Name of list
 * @param {List[]} list New value for list
 */
export const setList = (name, list) => {
	listsStore = {
		...listsStore,
		[name]: list
	};

	if (subscribers.length) {
		for (let i = 0; i < subscribers.length; i++) {
			subscribers[i](listsStore);
		}
	}
};

/**
 * Get a derived store for a specific list
 *
 * @param {string} list Name of list
 */
export const aList = (list) => derived(lists, ($lists) => $lists[list]);

/**
 * Get label of given list id
 *
 * @param {List} list
 * @param {string} id Id of item in list
 * @param {boolean} [returnId] Whether to return the given id value if the
 *   id value is not in the list (defaults to false)
 *
 * @returns {string|null} The list item label or null if it can't be found
 */
export const getListLabel = (list, id, returnId) => {
	if (!list) {
		return returnId ? id : null;
	}

	for (let i = 0; i < list.length; i++) {
		if (typeof list[i] === 'string') {
			if (list[i] === id) {
				return id;
			}
		} else {
			if (list[i].id === id) {
				return list[i].label;
			}
		}
	}

	return returnId ? id : null;
};

/**
 * Get id of given list label
 *
 * @param {List} list
 * @param {string} label Label of item in list
 *
 * @returns {string|null} The list item id or null if it can't be found
 */
export const getListId = (list, label) => {
	if (!list) {
		return null;
	}

	for (let i = 0; i < list.length; i++) {
		if (typeof list[i] === 'string') {
			if (list[i] === label) {
				return label;
			}
		} else {
			if (list[i].label === label) {
				return list[i].id;
			}
		}
	}

	return null;
};

/**
 * Check if a given value is a list value, optionally a list value or label
 *
 * @param {List} list List to check
 * @param {string} value Value to check if is a list value
 * @param {boolean} orLabel If true, will also check if given value is a list
 *   label
 *
 * @returns {boolean} Whether the value is a list value (or label)
 */
export const isListValue = (list, value, orLabel) => {
	for (let i = 0; i < list.length; i++) {
		if (typeof list[i] === 'string') {
			if (list[i] === value) {
				return true;
			}
		} else {
			if (list[i].id === value || (orLabel && list[i].label === value)) {
				return true;
			}
		}
	}

	return false;
};
