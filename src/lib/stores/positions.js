import { createIncidentObjectStore } from './generic.js';

export const incidentPositions = createIncidentObjectStore('positions', 'subject');
