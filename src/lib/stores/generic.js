import { currentIncident } from './incidents.js';
import { subscribe } from '$lib/socket';
import { base } from '$lib/api';
import fastJsonPatch from 'fast-json-patch';
const { applyPatch } = fastJsonPatch;

/**
 * Create a incident-dependent object store
 *
 * @param {string} collection Name of the collection the objects are from
 * @param {string} [updateKey] Key to use for to check if object already exists
 *   in the store (default is id)
 */
export const createIncidentObjectStore = (collection, updateKey) => {
	let store = null;
	let subscription = null;
	const subscribers = [];
	let incidentId = null;
	let incidentSubscription = null;
	if (!updateKey) {
		updateKey = 'id';
	}

	/**
	 * Update all the store subscribers
	 */
	const updateSubscribers = () => {
		if (!subscribers.length) {
			return;
		}

		for (let i = 0; i < subscribers.length; i++) {
			subscribers[i](store);
		}
	};

	const handleMessage = (topic, data) => {
		const [, type] = topic.split(':');
		switch (type) {
			case 'list': {
				const newStore = [];
				// Remove any saved objects
				if (store?.length) {
					for (let i = 0; i < store.length; i++) {
						if (!store[i].id) {
							newStore.push(store[i]);
						}
					}
				}

				for (let i = 0; i < data.length; i++) {
					newStore.push({
						id: data[i].id,
						data: data[i]
					});
				}

				store = newStore;
				updateSubscribers();
				return;
			}
			case 'update': {
				const newStore = [...(store || [])];

				const objects = Array.isArray(data) ? data : [data];

				for (let i = 0; i < objects.length; i++) {
					let j = 0;

					while (j < newStore.length) {
						if (newStore[j].data[updateKey] === data[updateKey]) {
							newStore[j] = {
								localId: newStore[j].localId,
								id: data.id,
								data
							};
							break;
						}
						j++;
					}

					if (j === newStore.length) {
						newStore.push({
							id: data.id,
							data
						});
					}
				}

				store = newStore;
				updateSubscribers();
				return;
			}
		}
	};

	/**
	 * Handle a change to the current incident
	 */
	const incidentUpdate = ($currentIncident) => {
		if (incidentId === $currentIncident?.id) {
			return;
		}

		store = null;
		updateSubscribers();

		// Unsubscribe current subscription
		if (subscription) {
			subscription();
			subscription = null;
		}

		incidentId = $currentIncident?.id;

		if (!incidentId) {
			return;
		}

		subscription = subscribe(
			collection + ':subscribe',
			{
				incident: $currentIncident?.id
			},
			handleMessage
		);
	};

	/**
	 * Find the current object in the store
	 *
	 * @param {T|string} object Object or ID of object to find in the store
	 *
	 * @returns {[T, number]} The object and the index of the object in the
	 *   store. The index will be -1 and the object null if the object is not
	 *   found in the store
	 */
	const findStoredObject = (object) => {
		if (store === null || store.length === 0) {
			return [null, -1];
		}

		if (typeof object === 'string') {
			for (let i = 0; i < store.length; i++) {
				if (store[i].id === object || store[i].localId === object) {
					return [store[i], i];
				}
			}
		}

		for (let i = 0; i < store.length; i++) {
			if (
				(object.localId && store[i].localId === object.localId) ||
				(object.id && store[i].id === object.id)
			) {
				return [store[i], i];
			}
		}

		return [null, -1];
	};

	const pushObject = async (object) => {
		let url = `${base}/${collection}`;
		const fetchOptions = {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(object.data)
		};

		if (object.id) {
			url += `/${object.id}`;
			fetchOptions.method = 'POST';
		}

		return fetch(url, fetchOptions).then(async (response) => {
			let data;

			try {
				data = await response.json();
			} catch {
				void 1;
			}

			if (response.status >= 400) {
				if (data?.error) {
					return Promise.reject(new Error(data.error));
				}

				return Promise.reject(new Error('Error received from request'));
			}

			const [storedObject, storedIndex] = findStoredObject(object.id);

			if (storedIndex != -1 && storedObject === object) {
				store = [...store];
				store[storedIndex].unsaved = false;

				updateSubscribers();
			}
		});
	};

	/** @type {import('svelte/store').Readable<T> */
	return {
		/**
		 * Subscribe to the store
		 */
		subscribe: (callback) => {
			subscribers.push(callback);

			callback(store);

			if (subscription === null) {
				incidentSubscription = currentIncident.subscribe(incidentUpdate);
			}

			return () => {
				const index = subscribers.indexOf(callback);

				if (index === -1) {
					return;
				}

				subscribers.splice(index, 1);

				if (!subscribers.length) {
					if (incidentSubscription !== null) {
						incidentSubscription();
						incidentSubscription = null;
					}

					if (subscription !== null) {
						subscription();
						subscription = null;
					}
				}
			};
		},
		/**
		 * Save an update to the given object
		 */
		update: async (object, dontPush) => {
			let [, storedIndex] = findStoredObject(object);

			if (storedIndex === -1) {
				if (store === null) {
					store = [object];
					storedIndex = 0;
				} else {
					store = [...store, object];
					storedIndex = store.length - 1;
				}
			} else {
				store = [...store];
				store[storedIndex] = object;
			}

			object.unsaved = true;
			updateSubscribers();

			if (!dontPush) {
				return pushObject(object);
			}
		},
		/**
		 * Patch the object with the given id
		 */
		patch: async (id, patch, dontPush) => {
			const [storedObject, storedIndex] = findStoredObject(id);

			if (storedIndex === -1) {
				throw new Error('Cannot patch an object that is not stored');
			}

			store = [...store];
			store[storedIndex] = {
				...storedObject,
				data: applyPatch(storedObject.data, patch, false, false)
			};
			// TODO Update IDs

			store[storedIndex].unsaved = true;
			updateSubscribers();

			const object = store[storedIndex];

			if (!dontPush) {
				if (!storedObject.id) {
					// Could potentially produce a race condition error
					return pushObject(object);
				}

				const url = `${base}/${collection}/${storedObject.id}`;
				const fetchOptions = {
					method: 'PATCH',
					headers: {
						'Content-Type': 'application/json-patch+json'
					},
					body: JSON.stringify(patch)
				};

				return fetch(url, fetchOptions).then(async (response) => {
					let data;

					try {
						data = await response.json();
					} catch {
						void 1;
					}

					if (response.status >= 400) {
						if (data?.error) {
							return Promise.reject(new Error(data.error));
						}

						return Promise.reject(new Error('Error received from request'));
					}

					const [currentStoredObject, currentStoredIndex] = findStoredObject(id);

					if (currentStoredIndex != -1 && currentStoredObject === object) {
						store = [...store];
						store[currentStoredIndex].unsaved = false;

						updateSubscribers();
					}
				});
			}
		}
	};
};
