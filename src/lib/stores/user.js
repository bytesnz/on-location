//const L = Logger('store:users');
import { subscribe } from '$lib/socket';
import { base } from '$lib/api';
import { browser } from '$app/env';
const L = console;

/*let usersStore = null;
let usersFetch = null;
const userSubscribers = [];


/* @type {import('$lib/socket').SocketSubscription} /
let usersSubscription = null;

const handleUsersMessage = (type, data) => {
	switch (type) {
		case 'user': {
			let i = 0;
			while (i < usersStore.length) {
				if (usersStore[i].id === data.id) {
					usersStore[i] = data;
					break;
				}
				i++;
			}
			if (i === usersStore.length) {
				usersStore.push(data);
			}
			break;
		}
	}

	updateCurrentUserSubscribers();
};*/

let userObject = null;
const userSubscribers = [];

const updateUserSubscribers = () => {
	if (userSubscribers.length) {
		for (let i = 0; i < userSubscribers.length; i++) {
			userSubscribers[i](userObject);
		}
	}
};

export const setUser = (user) => {
	userObject = user;

	updateUserSubscribers();
};

export const user = {
	subscribe: (callback) => {
		userSubscribers.push(callback);
		callback(userObject);
		return () => {
			const index = userSubscribers.indexOf(callback);
			if (index !== -1) {
				userSubscribers.splice(index, 1);
			}
		};
	}
};
