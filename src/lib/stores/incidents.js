//const L = Logger('store:incidents');
import { subscribe } from '$lib/socket';
import { base } from '$lib/api';
import { browser } from '$app/env';
const L = console;

let incidentsStore = null;
let incidentsFetch = null;
const incidentSubscribers = [];

export const fetchIncidents = (fetch) => {
	incidentsFetch = fetch(base + '/incidents').then(async (response) => {
		if (response.status !== 200) {
			let error = null;
			try {
				error = await response.json();
			} catch {
				error = null;
			}

			throw new Error(`Error ${response.status} ${error?.error || ''}`);
		}

		incidentsStore = await response.json();
		if (incidentSubscribers.length) {
			for (let i = 0; i < incidentSubscribers.length; i++) {
				incidentSubscribers[i](incidentsStore);
			}
		}
		incidentsFetch = null;
		return incidentsStore;
	});
	return incidentsFetch;
};

/* @type {import('$lib/socket').SocketSubscription} */
let incidentsSubscription = null;

const handleIncidentsMessage = (type, data) => {
	switch (type) {
		case 'incidents':
			if (incidentsStore === null) {
				incidentsStore = data;
			} else {
				for (let i = 0; i < data.length; i++) {
					let j = 0;
					while (j < incidentsStore.length) {
						if (incidentsStore[j].id === data[i].id) {
							incidentsStore[j] = data[i];
							break;
						}
						j++;
					}
					if (j === incidentsStore.length) {
						incidentsStore.push(data[i]);
					}
				}
			}
			break;
		case 'incident': {
			let i = 0;
			while (i < incidentsStore.length) {
				if (incidentsStore[i].id === data.id) {
					incidentsStore[i] = data;
					break;
				}
				i++;
			}
			if (i === incidentsStore.length) {
				incidentsStore.push(data);
			}
			break;
		}
	}

	updateCurrentIncidentSubscribers();
};

export const incidents = {
	get fetch() {
		return incidentsFetch;
	},
	waitForFetch: async () => {
		if (incidentsFetch instanceof Promise) {
			await incidentsFetch;
		}
	},
	subscribe: (callback) => {
		incidentSubscribers.push(callback);
		callback(incidentsStore);
		//if (incidentsStore === null && incidentsFetch === null) {
		//  fetchIncidents();
		//}
		if (browser && !incidentsSubscription) {
			incidentsSubscription = subscribe('incidents:subscribe', {}, handleIncidentsMessage);
		}

		return () => {
			const index = incidentSubscribers.indexOf(callback);
			if (index !== -1) {
				incidentSubscribers.splice(index, 1);
			}
			if (browser && !incidentSubscribers.length && incidentsSubscription) {
				incidentsSubscription();
				incidentsSubscription = null;
			}
		};
	}
};

let currentIncidentId = null;
let currentIncidentObject = null;
const currentIncidentSubscribers = [];

const updateCurrentIncidentSubscribers = () => {
	if (currentIncidentSubscribers.length) {
		for (let i = 0; i < currentIncidentSubscribers.length; i++) {
			currentIncidentSubscribers[i](currentIncidentObject);
		}
	}
};

export const currentIncident = {
	setId: (incidentId) => {
		if (incidentId === null) {
			currentIncidentId = null;
			currentIncidentObject = null;
			updateCurrentIncidentSubscribers();
		}

		if (currentIncidentId === incidentId) {
			return;
		}

		if (incidentsStore) {
			let newCurrentIncident = null;
			for (let i = 0; i < incidentsStore.length; i++) {
				if (incidentsStore[i].id === incidentId) {
					newCurrentIncident = incidentsStore[i];
					break;
				}
			}

			if (newCurrentIncident) {
				currentIncidentId = incidentId;
				// TODO Should use some sort of store?
				currentIncidentObject = newCurrentIncident;
				updateCurrentIncidentSubscribers();
				return true;
			}
		}

		return false;
	},
	subscribe: (callback) => {
		currentIncidentSubscribers.push(callback);
		callback(currentIncidentObject);
		return () => {
			const index = currentIncidentSubscribers.indexOf(callback);
			if (index !== -1) {
				currentIncidentSubscribers.splice(index, 1);
			}
		};
	}
};
