export const makeTitle = (config, ...parts) => {
	parts = [config.name, ...parts];
	return parts.join(config.titleSeparator);
};

/**
 * Create a random string for IDs
 */
export const makeId = () =>
	btoa(String.fromCharCode(...crypto.getRandomValues(new Uint8Array(21))));

/**
 * Remove null and empty string values from an object
 *
 * @param {object} object Object to remove emtpy and null values from
 */
export const removeEmptyValues = (object) => {
	if (object === null || typeof object !== 'object') {
		return object;
	}

	if (Array.isArray(object)) {
		object = [...object];
	} else {
		object = { ...object };
	}

	for (const key in object) {
		if (object[key] === null || object[key] === '') {
			delete object[key];
		} else {
			if (typeof object[key] === 'object') {
				object[key] = removeEmptyValues(object[key]);
			}
		}
	}

	return object;
};

/**
 * Format the name of the object
 *
 * TODO Use config for personnel name
 *
 * @param object Object to format the name of
 */
export function formatName(object) {
	if (object.label) {
		return object.label;
	}

	if (object.name) {
		return object.name;
	}

	if (object.givenName && object.familyName) {
		return object.givenName + ' ' + object.familyName;
	}
}

/**
 * Check if two variables are deeply equal
 *
 * @param {any} a Variable to test for equality
 * @param {any} b Variable to test for equality
 * @param {string[]} exclude If variables are objects, the keys to ignore
 *
 * @returns {null|string[]} null if equal or a path to the first difference
 */
export const isObjectEqual = (a, b, exclude) => {
	return isObjectEqualIteration(a, b, exclude, null);
};

const isObjectEqualIteration = (a, b, exclude, path) => {
	if (!path) {
		path = [];
	}

	if (a === b) {
		return null;
	}

	if (typeof a !== typeof b) {
		return path;
	}

	if (a === null && b === null) {
		return null;
	}

	if (a === null || b === null) {
		return path;
	}

	if (typeof a === 'object') {
		let result;
		const currentPath = [...path, null];
		const last = currentPath.length - 1;

		if (Array.isArray(a) && Array.isArray(b)) {
			if (a.length != a.length) {
				return path;
			}

			for (let i = 0; i < a.length; i++) {
				currentPath[last] = i;
				result = isObjectEqualIteration(a[i], b[i], null, currentPath);

				if (result) {
					return result;
				}
			}

			return null;
		}

		if (Array.isArray(a) || Array.isArray(b)) {
			return path;
		}

		const aKeys = Object.keys(a);
		const bKeys = Object.keys(b);

		if (aKeys.length != bKeys.length) {
			return path;
		}

		if (exclude) {
			let index;
			for (let i = 0; i < exclude.length; i++) {
				if ((index = bKeys.indexOf(exclude[i])) !== -1) {
					bKeys.splice(index, 1);
				}
				if ((index = aKeys.indexOf(exclude[i])) !== -1) {
					aKeys.splice(index, 1);
				}
			}
		}

		for (let i = 0; i < aKeys.length; i++) {
			if (bKeys.indexOf(aKeys[i]) === -1) {
				return path;
			}

			currentPath[last] = aKeys[i];
			result = isObjectEqualIteration(a[aKeys[i]], b[aKeys[i]], null, currentPath);

			if (result) {
				return result;
			}
		}

		return null;
	}

	if (a !== b) {
		return path;
	}
};

/** @typedef {object} SearchParamOptions
 *
 * @property {string[]} [booleanKeys] If given, the keys in the array will
 *   always be only a boolean value
 * @property {string[]} [arrayKeys] If given and the keys in the array have a
 *   value, the values will always be returned in an array
 */

/**
 * Convert a URL.SearchParams object into a standard object, converting any
 * doubled up parameters to an array of parameters
 *
 * @param {SearchParams} searchParams Search parameters to convert
 * @param {SearchParamOptions} [options] Options for parsing searchParams
 */
export const searchParamsToObject = (searchParams, options) => {
	const params = {};

	const entries = searchParams.entries();

	for (const [key, value] of entries) {
		// Covert blank into boolean
		if (options?.booleanKeys && options.booleanKeys.indexOf(key) !== -1) {
			if (value === '0' || value === 'false') {
				params[key] = false;
			} else {
				params[key] = true;
			}
			continue;
		}
		if (Object.prototype.hasOwnProperty.call(params, key)) {
			if (!Array.isArray(params[key])) {
				params[key] = [params[key]];
			}
			params[key].push(value);
		} else {
			if (options?.arrayKeys && options.arrayKeys.indexOf(key) !== -1) {
				params[key] = [value];
			} else {
				params[key] = value;
			}
		}
	}

	return params;
};

/**
 * Find the first value in the haystack array that matches the values in the
 * needles array
 *
 * @param {any[]} haystack Array to search
 * @param {any[]} needles Array of values to find
 *
 * @returns {any|null} First found value, or null if no matching value found
 */
export const findFirstMatch = (haystack, needles) => {
	for (let i = 0; i < haystack.length; i++) {
		if (needles.indexOf(haystack[i]) !== -1) {
			return haystack[i];
		}
	}

	return null;
};
