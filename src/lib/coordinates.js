/**
 * Convert angle in degrees to an angle in radians
 */
export const deg2rad = (deg) => {
	return deg * (Math.PI / 180);
};

/**
 * Convert angle in radians to an angle in degrees
 */
export const rad2deg = (rad) => {
	return rad / (Math.PI / 180);
};

/**
 * Calcuate the distance between 2 positions using leafet's distanceTo
 * function
 *
 * @returns Distance between the 2 points in meters
 */
export const calculateDistance = (lat1, lon1, lat2, lon2) => {
	/* let R = 6371; // Radius of the earth in km
  let dLat = deg2rad(lat2 - lat1); // deg2rad below
  let dLon = deg2rad(lon2 - lon1);
  let a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = R * c; // Distance in km
  return d * 1000; // Return in meters
	*/
	return calculateNavigation(lat1, lon1, lat2, lon2).distance;
};

/**
 * Calculate the area taken up by the give polygon
 *
 * @param {Array<[number, number]>} coordinates Polygon coordinates to
 *   calculate the area for
 * @param {string|number} latKey The key to latitude for each coordinate.
 *   Defaults to 1 (for an array)
 * @param {string|number} lonKey The key to longitude for each coordinate.
 *   Defaults to 0 (for an array)
 *
 * @returns {number} The area in m2
 *
 * @source L.GeometryUtil.geodesicArea()
 */
export const calculateArea = (coordinates, latKey, lonKey) => {
	if (typeof latKey === 'undefined') {
		latKey = 1;
	}
	if (typeof lonKey === 'undefined') {
		lonKey = 0;
	}

	let start, end;
	let length = coordinates.length;
	let distance = 0;
	const radRatio = Math.PI / 180;
	if (length > 2) {
		for (let i = 0; i < length; i++) {
			start = coordinates[i];
			end = coordinates[(i + 1) % length];
			distance +=
				(end[lonKey] - start[lonKey]) *
				radRatio *
				(2 + Math.sin(start[latKey] * radRatio) + Math.sin(end[latKey] * radRatio));
		}
		distance = (6378137 * distance * 6378137) / 2;
	}

	return Math.abs(distance);
};

/**
 * Calculate the heading from one coordinate to the next
 * @source http://www.geomidpoint.com/destination/calculation.html
 *
 * @param {number} lat1 Starting latitude
 * @param {number} lon1 Starting longitude
 * @param {number} lat2 Target latitude
 * @param {number} lon2 Target longitude
 */
export const calculateHeading = (lat1, lon1, lat2, lon2) => {
	/*const y = Math.sin(lon2 - lon1) * Math.cos(lat2);
  const x =
    Math.cos(lat1) * Math.sin(lat2) -
    Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1);
  const bearing = (Math.atan2(y, x) * 180) / Math.PI;

	console.log('from', lat1, lon1, 'to', lat2, lon2, bearing);
  if (bearing < 0) {
    return 360 + bearing;
  }
	*/

	return calculateNavigation(lat1, lon1, lat2, lon2).bearing;
};

// The following to calculateNavigation is copied from
// https://www.qsl.net/w5uhq/Java_apps.htm
const mod = (x, y) => x - y * Math.floor(x / y);

const modcrs = (x) => mod(x, 2 * Math.PI);

const atan2 = (y, x) => {
	if (x == 0 && y == 0) {
		return 0;
	}

	if (x < 0) {
		return Math.atan(y / x) + Math.PI;
	}
	if (x > 0 && y >= 0) {
		return Math.atan(y / x);
	}
	if (x > 0 && y < 0) {
		return Math.atan(y / x) + 2 * Math.PI;
	}
	if (x == 0 && y > 0) {
		return Math.PI / 2;
	}
	if (x == 0 && y < 0) {
		return (3 * Math.PI) / 2;
	}
};

/**
 * Calculate the bearing a distance from one point to another
 *
 * @param {number} lat1 Latitude of first coordinate
 * @param {number} lon1 Longitude of first coordinate
 * @param {number} lat2 Latitude of second coordinate
 * @param {number} lon2 Longitude of second coordinate
 *
 * @returns {object} An object containing the distance in meters and the
 *   bearing to the second coordinate
 */
export const calculateNavigation = (lat1, lon1, lat2, lon2) => {
	if (lat1 === lat2 && lon1 === lon2) {
		return {
			bearing: 0,
			distance: 0
		};
	}

	// WGS84
	const a = 6378.137 / 1.852;
	const f = 1 / 298.257223563;

	// Convert lat/lon to radian
	lat1 = (lat1 * Math.PI) / 180;
	lon1 = (lon1 * Math.PI) / 180;
	lat2 = (lat2 * Math.PI) / 180;
	lon2 = (lon2 * Math.PI) / 180;

	let r, tu1, tu2, cu1, su1, cu2, s1, b1, f1;
	let x, sx, cx, sy, cy, y, sa, c2a, cz, e, c, d;
	let EPS = 0.005;
	let radBearing, s;
	let iter = 1;
	let MAXITER = 10;

	if (lat1 + lat2 == 0 && Math.abs(lon1 - lon2) == Math.PI) {
		lat1 = lat1 + 0.00001; // allow algorithm to complete
	}
	r = 1 - f;
	tu1 = r * Math.tan(lat1);
	tu2 = r * Math.tan(lat2);
	cu1 = 1 / Math.sqrt(1 + tu1 * tu1);
	su1 = cu1 * tu1;
	cu2 = 1 / Math.sqrt(1 + tu2 * tu2);
	s1 = cu1 * cu2;
	b1 = s1 * tu2;
	f1 = b1 * tu1;
	x = lon2 - lon1;
	d = x + 1; // force one pass
	while (Math.abs(d - x) > EPS && iter < MAXITER) {
		iter = iter + 1;
		sx = Math.sin(x);
		//       alert("sx="+sx)
		cx = Math.cos(x);
		tu1 = cu2 * sx;
		tu2 = b1 - su1 * cu2 * cx;
		sy = Math.sqrt(tu1 * tu1 + tu2 * tu2);
		cy = s1 * cx + f1;
		y = atan2(sy, cy);
		sa = (s1 * sx) / sy;
		c2a = 1 - sa * sa;
		cz = f1 + f1;
		if (c2a > 0) cz = cy - cz / c2a;
		e = cz * cz * 2 - 1;
		c = (((-3 * c2a + 4) * f + 4) * c2a * f) / 16;
		d = x;
		x = ((e * cy * c + cz) * sy * c + y) * sa;
		x = (1 - c) * x * f + lon2 - lon1;
	}
	radBearing = modcrs(atan2(tu1, tu2));
	//baz = modcrs(atan2(cu1 * sx, b1 * cx - su1 * cu2) + Math.PI);
	x = Math.sqrt((1 / (r * r) - 1) * c2a + 1);
	x += 1;
	x = (x - 2) / x;
	c = 1 - x;
	c = ((x * x) / 4 + 1) / c;
	d = (0.375 * x * x - 1) * x;
	x = e * cy;
	s =
		((((((sy * sy * 4 - 3) * (1 - e - e) * cz * d) / 6 - x) * d) / 4 + cz) * sy * d + y) *
		c *
		a *
		r;

	if (Math.abs(iter - MAXITER) < EPS) {
		// TODO did not converge
		console.warn('algorithm did not converge');
	}

	return {
		distance: s * 1852, // from Nmi to m
		bearing: (radBearing * 180) / Math.PI // from rad to deg
	};
};

/**
 * Get the quadrant (tr, br, bl, tl) that the bearing is in
 *
 * @param {number} bearing Bearing (in degrees)
 *
 * @returns {string} 2-letter quadrant
 */
export const getQuadrant = (bearing) => {
	if (bearing < 90) {
		return 'tr';
	} else if (bearing < 180) {
		return 'br';
	} else if (bearing < 270) {
		return 'bl';
	}

	return 'tl';
};

/**
 * Get the closest axis (left, right, top, bottom) and the degrees away from
 * it
 *
 * @param bearing Bearing to get closest axis for
 * @param quadrant Precalculated quadrant for the bearing
 *
 * @returns [ closestAxis, degreesAway ]
 */
export const getClosest = (bearing, quadrant) => {
	let acwm = true;
	let quadrantFigure = bearing % 90;
	if (!quadrant) {
		quadrant = getQuadrant(bearing);
	}

	if (quadrantFigure > 45) {
		quadrantFigure = 90 - quadrantFigure;
		acwm = false;
	}

	let closest;
	switch (quadrant) {
		case 'tr':
			closest = acwm ? 't' : 'r';
			break;
		case 'br':
			closest = acwm ? 'r' : 'b';
			break;
		case 'bl':
			closest = acwm ? 'b' : 'l';
			break;
		case 'tl':
			closest = acwm ? 'l' : 't';
			break;
	}
	return [closest, quadrantFigure];
};
