import { assert, describe, it } from 'vitest';
import * as date from './date.js';

describe('mergeTimes()', () => {
	it('merges overlapping dates together', () => {
		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-03T00:00:00Z' },
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-04T00:00:00Z' },
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-03T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }]
		);
	});

	it('keeps separate times separate', () => {
		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }
			]),
			[
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }
			]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' },
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' }
			]),
			[
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }
			]
		);
	});

	it('merges no endTime correctly', () => {
		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: null },
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-03T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: null }]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: null }
			]),
			[
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: null }
			]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-02T00:00:00Z', endTime: null },
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-03T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: null }]
		);
	});

	it('merges bridged times correctly', () => {
		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-03T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' },
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-03T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }]
		);

		assert.deepEqual(
			date.mergeTimes([
				{ startTime: '2000-01-03T00:00:00Z', endTime: '2000-01-04T00:00:00Z' },
				{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-02T00:00:00Z' },
				{ startTime: '2000-01-02T00:00:00Z', endTime: '2000-01-03T00:00:00Z' }
			]),
			[{ startTime: '2000-01-01T00:00:00Z', endTime: '2000-01-04T00:00:00Z' }]
		);
	});
});
