import { dev, browser } from '$app/env';

// TODO Make better for non-dev
export const base = dev ? 'http://localhost:3001' : browser ? '/api' : 'http://localhost:3000/api';

export const host = dev ? 'localhost' : null;

export const port = dev ? '3001' : null;

export const path = dev ? '/' : null;
