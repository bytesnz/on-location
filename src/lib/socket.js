import { browser, dev } from '$app/env';
import { host, port, path } from '$lib/api';
import { addStatus } from '$lib/stores/status';

/** @typedef {object} Message<T>
 * Web socket message
 *
 * @prop {string} t Message type
 * @prop {string|number} i Message ID
 * @prop {T} d Message data
 *
 * @template {object} T Data object
 */

/** @callback Handler<T>
 * Message handler
 *
 * @param {string} type Message type
 * @param {T} data Message data
 * @param {string|number} id Message ID
 *
 * @returns {boolean} Request complete
 *
 * @template {object} T Data object
 */

/** @typedef {object} AwaitingResponse
 *
 * @prop {Timeout} timeout Timeout for waiting
 * @prop {Message<any>} message Message sent
 * @prop {Handler<any>} callback
 */

const subscriptions = {};
/* @type {{ message: Message<any>, callback?: Handler<any> }[]} */
const queue = [];
/** @type {{ [id: string|number]: AwaitingResponse }} */
const awaitingResponse = {};
let waitTimeout = 1;
const maxWaitTimeout = 20;
let nextId = 0;
let socket = null;
let connected = false;
let status = null;

/* @typedef {() => void} SocketSubscription */

const connectToSocket = () => {
	const url = new URL(location.href);

	url.protocol = url.protocol === 'https:' ? 'wss:' : 'ws:';
	if (host) url.host = host;
	if (url.port) url.port = port;
	url.pathname = path || '/';
	url.hash = '';
	url.search = '';

	if (!status) {
		status = addStatus('Connecting to server', {
			type: 'active'
		});
	} else {
		status.update('Connecting to server', {
			type: 'active'
		});
	}

	socket = new WebSocket(url.href, 'on-location');
	socket.addEventListener('open', (event) => {
		waitTimeout = 1;
		connected = true;
		status.clear();
		status = null;
		while (queue.length) {
			const item = queue.shift();
			if (dev) {
				/* eslint-disable-next-line no-console */
				console.debug('Sending socket message', item.message);
			}
			socket.send(JSON.stringify(item.message));
			if (item.callback) {
				addToResponseQueue(item.message, item.callback);
			}
		}
		for (let id in subscriptions) {
			socket.send(
				JSON.stringify({
					t: subscriptions[id].type,
					i: id,
					d: subscriptions[id].data,
					r: 1
				})
			);
		}
	});
	socket.addEventListener('close', (event) => {
		const statusMessage = `Connection to server lost. Reconnecting in ${waitTimeout}s`;
		if (!status) {
			status = addStatus(statusMessage, {
				type: 'active warning'
			});
		} else {
			status.update(statusMessage, {
				type: 'active warning'
			});
		}

		setTimeout(() => {
			connectToSocket();
		}, waitTimeout * 1000);
		waitTimeout = Math.min(maxWaitTimeout, waitTimeout * 2);
	});
	socket.addEventListener('error', (event) => {
		/*setTimeout(() => {
			connectToSocket();
		}, waitTimeout * 1000);
		waitTimeout = Math.min(maxWaitTimeout, waitTimeout * 2);*/
	});
	socket.addEventListener('message', (event) => {
		try {
			const data = JSON.parse(event.data);

			if (dev) {
				/* eslint-disable-next-line no-console */
				console.debug('Received socket message', data);
			}

			// Check if for subscription
			if (subscriptions[data.i]) {
				subscriptions[data.i].callback(data.t, data.d, data.i);
			}
			// Check for awaiting response
			if (awaitingResponse[data.i]) {
				if (awaitingResponse[data.i].callback(data.t, data.d, data.i)) {
					delete awaitingResponse[data.i];
				}
			}
		} catch (error) {
			console.error('Could not parse socket message', error);
		}
	});
};

if (browser) {
	connectToSocket();
}

/**
 * Add message and callback to awaiting response queue
 *
 * @template {object} T
 * @template {object} R
 * @param {Message<T>} message
 * @param {Handler<R|Error>} callback
 */
function addToResponseQueue(message, callback) {
	awaitingResponse[message.i] = {
		message,
		timeout: setTimeout(() => {
			callback(message.t, new Error('Timeout waiting for response'), message.i);
			delete awaitingResponse[message.i];
		}, 10000),
		callback
	};
}

/**
 * Send data over the web socket. Will return
 *
 * @param {string} type Message type
 * @param {object} data Message data
 * @param {Handler<any>} [callback] Callback to call with responses
 *
 * @returns {number|string} Message ID
 */
export const sendMessage = (type, data, callback) => {
	const id = nextId++;

	setTimeout(() => {
		const message = {
			t: type,
			i: id,
			d: data
		};
		if (socket && connected) {
			if (dev) {
				/* eslint-disable-next-line no-console */
				console.debug('Sending socket message', data);
			}
			socket.send(JSON.stringify(message));
			if (callback) {
				addToResponseQueue(message, callback);
			}
		} else {
			queue.push({
				message,
				callback
			});
		}
	});

	return id;
};

/**
 * Subscribe to
 *
 * @param {string} type Event type for subscription
 * @param {object} data Data for subscription request
 * @param {(event: string, data: object) => void)} callback Callback that will
 *   called when a new subscription message is received
 *
 * @returns {SocketSubscription} An unsubscribe function to call to unsubscribe
 */
export const subscribe = (type, data, callback) => {
	const id = nextId++;

	const message = {
		t: type,
		i: id,
		d: data
	};
	if (socket && connected) {
		socket.send(JSON.stringify(message));
	} else {
		queue.push(message);
	}

	subscriptions[id] = {
		id,
		callback,
		type,
		data
	};

	return () => {
		if (subscriptions[id]) delete subscriptions[id];
	};
};
