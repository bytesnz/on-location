import { assert, describe, expect, it } from 'vitest';
import {
	coordinateFormats,
	formatCoordinates,
	parseCoordinateString
} from './coordinateFormats.js';

describe('dmm format', () => {
	it('parses -41 6.597, 175 5.5302', () => {
		assert.isTrue('-41 6.597'.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue('-41 6.597, 175 5.5302'.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue('-41 6.597, 175 5.5302'.match(coordinateFormats.dmm.full) !== null);
	});

	it('parses 1 1, 1 1', () => {
		assert.isTrue('1 1'.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue('1 1, 1 1'.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue('1 1, 1 1'.match(coordinateFormats.dmm.full) !== null);
	});

	it('parses 89 59.9, 179 59.9', () => {
		assert.isTrue('89 59.9'.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue('89 59.9, 179 59.9'.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue('89 59.9, 179 59.9'.match(coordinateFormats.dmm.full) !== null);
	});

	// TODO
	it('does not parse 90 1, 180 1', () => {
		assert.isTrue('90 1, 180 1'.match(coordinateFormats.dmm.partial) === null);
		assert.isTrue('90 1, 180 1'.match(coordinateFormats.dmm.partial) === null);
		assert.isTrue('90 1, 180 1'.match(coordinateFormats.dmm.full) === null);
	});

	it('returns the correct results', () => {
		assert.deepEqual(parseCoordinateString("41°16.1'N 174°40.6'W"), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [-174.676667, 41.268333]
		});

		assert.deepEqual(parseCoordinateString("41°16.1'n 174°40.6'w"), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [-174.676667, 41.268333]
		});

		assert.deepEqual(parseCoordinateString("41°16.1'S 174°40.6'E"), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [174.676667, -41.268333]
		});

		assert.deepEqual(parseCoordinateString("41°16.1's 174°40.6'e"), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [174.676667, -41.268333]
		});

		assert.deepEqual(parseCoordinateString("S41°16.1' E174°40.6'"), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [174.676667, -41.268333]
		});

		assert.deepEqual(parseCoordinateString("-41°16.1' 174°40.6'"), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [174.676667, -41.268333]
		});

		assert.deepEqual(parseCoordinateString('-41 16.1, 174 40.6'), {
			format: coordinateFormats.dmm,
			valid: true,
			coordinates: [174.676667, -41.268333]
		});

		assert.deepEqual(parseCoordinateString("-41°16.1'S 174°40.6'E"), {
			format: coordinateFormats.dmm,
			valid: false,
			coordinates: null
		});
	});
	/*
	it('parses ', () => {
		assert.isTrue(''.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue(''.match(coordinateFormats.dmm.partial) !== null);
		assert.isTrue(''.match(coordinateFormats.dmm.full) !== null);
	})

	it('does not parse ', () => {
		assert.isTrue(''.match(coordinateFormats.dmm.partial) === null);
		assert.isTrue(''.match(coordinateFormats.dmm.partial) === null);
		assert.isTrue(''.match(coordinateFormats.dmm.full) === null);
	})
	*/
});

describe('dms format', () => {
	it('parses the google format', () => {
		assert.isTrue('41°16\'50.1"S 174°40\'28.6"E'.match(coordinateFormats.dms.partial) !== null);
		assert.isTrue('41°16\'50.1"S 174°40\'28.6"E'.match(coordinateFormats.dms.full) !== null);
	});

	it('returns the correct results', () => {
		assert.deepEqual(parseCoordinateString('41°16\'50.1"S 174°40\'28.6"E'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [174.674611, -41.280583]
		});

		assert.deepEqual(parseCoordinateString('41°16\'50.1"s 174°40\'28.6"e'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [174.674611, -41.280583]
		});

		assert.deepEqual(parseCoordinateString('41°16\'50.1"N 174°40\'28.6"W'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [-174.674611, 41.280583]
		});

		assert.deepEqual(parseCoordinateString('41°16\'50.1"n 174°40\'28.6"w'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [-174.674611, 41.280583]
		});

		assert.deepEqual(parseCoordinateString('S41°16\'50.1" E174°40\'28.6"'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [174.674611, -41.280583]
		});

		assert.deepEqual(parseCoordinateString('-41°16\'50.1" 174°40\'28.6"'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [174.674611, -41.280583]
		});

		assert.deepEqual(parseCoordinateString('-41 16 50.1, 174 40 28.6'), {
			format: coordinateFormats.dms,
			valid: true,
			coordinates: [174.674611, -41.280583]
		});

		assert.deepEqual(parseCoordinateString('-41°16\'50.1"S 174°40\'28.6"E'), {
			format: coordinateFormats.dms,
			valid: false,
			coordinates: null
		});
	});

	it('marks coordinates with double hemisphere specification as invalid', () => {
		let result = parseCoordinateString('-41°16\'50.1"S 174°40\'28.6"E');

		assert.isFalse(result.valid);

		result = parseCoordinateString('41°16\'50.1"S -174°40\'28.6"E');

		assert.isFalse(result.valid);
	});
});

describe('ddd format', () => {
	it('parses the limits correctly', () => {
		assert.deepEqual(parseCoordinateString('90°S 180°E'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [180, -90]
		});

		assert.deepEqual(parseCoordinateString('90°N 180°W'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [-180, 90]
		});
	});

	it('returns the correct results', () => {
		assert.deepEqual(parseCoordinateString('41.123456°S 174.256481°E'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [174.256481, -41.123456]
		});

		assert.deepEqual(parseCoordinateString('41.123456°s 174.256481°e'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [174.256481, -41.123456]
		});

		assert.deepEqual(parseCoordinateString('41.123456°N 174.256481°W'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [-174.256481, 41.123456]
		});

		assert.deepEqual(parseCoordinateString('41.123456°n 174.256481°w'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [-174.256481, 41.123456]
		});

		assert.deepEqual(parseCoordinateString('S41.123456° E174.256481°'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [174.256481, -41.123456]
		});

		assert.deepEqual(parseCoordinateString('-41.123456° 174.256481°'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [174.256481, -41.123456]
		});

		assert.deepEqual(parseCoordinateString('-41.123456, 174.256481'), {
			format: coordinateFormats.ddd,
			valid: true,
			coordinates: [174.256481, -41.123456]
		});

		assert.deepEqual(parseCoordinateString('-41.123456°S 174.256481°E'), {
			format: coordinateFormats.ddd,
			valid: false,
			coordinates: null
		});
	});
});

describe('nztm format', () => {
	it('parses the correct results', () => {
		assert.deepEqual(parseCoordinateString('1769363 5456841'), {
			format: coordinateFormats.nztm,
			valid: true,
			coordinates: [175.014405, -41.022047]
		});
	});

	it('formats the coordinates correctly', () => {
		expect(formatCoordinates([175.014405, -41.022047], 'nztm')).toBe('1769363E 5456841N');
	});
});
