import proj4 from 'proj4';

/**@callback Parser
 * @param {Match} match Match result from coordinate format full regular
 *   expression
 *
 * @returns {[number, number]} The parsed coordinates (long, lat) in decimal
 *   degrees
 */

/**@callback Formatter
 * @param {[number, number]} coordinates The coordinates to format (long, lat)
 *   in decimal degrees
 *
 * @returns {string} The formatted coordinates
 */

/**@callback Validator
 * @param {Match} match Match result from coordinate format full regular
 *   expression
 *
 * @returns {boolean} Whether the full coordinates are valid
 */

/**@typedef {object} CoordinateFormat
 *
 * @property {string} label Label for the coordinate format
 * @property {string} help Help label containing coordinate format
 * @property {RegExp} partial Regular expression for partial coordinate match
 *   for detection of format
 * @property {RegExp} full Regular expression for the complete parsing of the
 *   coordinate format
 * @property {string} projectionId The coordinate format projection ID
 * @property {string} [projection] The projection string for the format
 * @property {Validator} [valid] The Validator function to validate the match
 *   result if additional checking is required
 * @property {Parser} [parse] The parse function to return the coordinates from
 *   the full regular expression.
 * @property {Formatter} [format] The format function to format the coordinates
 *   into the given format
 */

/**@type {{[formatId: string]: CoordinateFormat}} */
export const coordinateFormats = {
	nztm: {
		label: 'NZTM2000',
		help: 'dddddd.dE dddddd.dN',
		partial: /^\d{4}/,
		full: /^[eE]?(?<first>\d{7}(\.\d+)?)[eE]? [nN]?(?<second>\d{7}(\.\d+)?)[nN]?$/,
		projectionId: 'EPSG:2193',
		projection:
			'+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
		valid: (match) => {
			const easting = match.groups.first;
			const northing = match.groups.second;

			if (
				easting > 2117458.3527 ||
				easting < 983515.7211 ||
				northing < 4728776.8709 ||
				northing > 6223676.2306
			) {
				return false;
			}

			return true;
		},
		format: (coordinates) => {
			return `${Math.round(coordinates[0])}E ${Math.round(coordinates[1])}N`;
		}
	},
	dms: {
		label: 'WGS-84 DMSs',
		help: 'dd mm ss.sss dd mm ss.sss (lat long)',
		partial: /^(-|N|n|S|s)?[0-8]?\d(° *| +)[0-5]?\d(' *| +)[0-5]?\d(\.\d+)?"?/,
		full: /^(?<lat_h_p>(N|n|S|s)?)(?<lat_d>-?[0-8]?\d)(° *| +)(?<lat_m>[0-5]?\d)(' *| +)(?<lat_s>[0-5]?\d(\.\d+)?)"?(?<lat_h>(N|n|S|s)?)(, *| +)(?<lon_h_p>(E|e|W|w)?)(?<lon_d>-?(1[0-7]\d|\d?\d)?)(° *| +)(?<lon_m>[0-5]?\d)(' *| +)(?<lon_s>[0-5]?\d(\.\d+)?)"?(?<lon_h>(E|e|W|w)?)$/,
		projectionId: 'EPSG:4326',
		//projection: '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees',
		//projectionId: 'EPSG:4326'
		valid: (match) => {
			if (!match) {
				return false;
			}

			if (
				(Number(match.groups.lat_d) < 0 && (match.groups.lat_h_p || match.groups.lat_h)) ||
				(Number(match.groups.lon_d) < 0 && (match.groups.lon_h_p || match.groups.lon_h))
			) {
				return false;
			}

			return true;
		},
		parse: (match) => {
			let latitude = Number(match.groups.lat_d);
			let longitude = Number(match.groups.lon_d);

			const lat_h = match.groups.lat_h || match.groups.lat_h_p;
			const lon_h = match.groups.lon_h || match.groups.lon_h_p;

			if (lat_h === 'S' || lat_h === 's') {
				latitude *= -1;
			}
			if (lon_h === 'W' || lon_h === 'w') {
				longitude *= -1;
			}

			if (latitude < 0) {
				latitude -= Number(match.groups.lat_m) / 60 + Number(match.groups.lat_s) / 3600;
			} else {
				latitude += Number(match.groups.lat_m) / 60 + Number(match.groups.lat_s) / 3600;
			}

			if (longitude < 0) {
				longitude -= Number(match.groups.lon_m) / 60 + Number(match.groups.lon_s) / 3600;
			} else {
				longitude += Number(match.groups.lon_m) / 60 + Number(match.groups.lon_s) / 3600;
			}

			return [longitude, latitude];
		},
		format: (coordinates) => {
			const lat_m = Math.abs(coordinates[1] % 1) * 60;
			const lon_m = Math.abs(coordinates[0] % 1) * 60;
			return `${coordinates[1] - (coordinates[1] % 1)}° ${lat_m - (lat_m % 1)}' ${(
				Math.abs(lat_m % 1) * 60
			).toFixed(3)}" ${coordinates[0] - (coordinates[0] % 1)}° ${lon_m - (lon_m % 1)}' ${(
				Math.abs(lon_m % 1) * 60
			).toFixed(3)}"`;
		}
	},
	dmm: {
		label: 'WGS-84 DMm',
		help: 'dd mm.mmmmmm dd mm.mmmmmm (lat long)',
		partial: /^(-|N|n|S|s)?[0-8]?\d(° *| +)[0-5]?\d(\.\d+)?'?/,
		full: /^(?<lat_h_p>(N|n|S|s)?)(?<lat_d>-?[0-8]?\d)(° *| +)(?<lat_m>[0-5]?\d(\.\d+)?)'?(?<lat_h>(N|n|S|s)?)(, *| +)(?<lon_h_p>(E|e|W|w)?)(?<lon_d>-?(1[0-7]\d|\d?\d)?)(° *| +)(?<lon_m>[0-5]?\d(\.\d+)?)'?(?<lon_h>(E|e|W|w)?)$/,
		projectionId: 'EPSG:4326',
		//projection: '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees',
		//projectionId: 'EPSG:4326'
		valid: (match) => {
			if (!match) {
				return false;
			}

			if (
				(Number(match.groups.lat_d) < 0 && (match.groups.lat_h_p || match.groups.lat_h)) ||
				(Number(match.groups.lon_d) < 0 && (match.groups.lon_h_p || match.groups.lon_h))
			) {
				return false;
			}

			return true;
		},
		parse: (match) => {
			let latitude = Number(match.groups.lat_d);
			let longitude = Number(match.groups.lon_d);

			const lat_h = match.groups.lat_h || match.groups.lat_h_p;
			const lon_h = match.groups.lon_h || match.groups.lon_h_p;

			if (lat_h === 'S' || lat_h === 's') {
				latitude *= -1;
			}
			if (lon_h === 'W' || lon_h === 'w') {
				longitude *= -1;
			}

			if (latitude < 0) {
				latitude -= Number(match.groups.lat_m) / 60;
			} else {
				latitude += Number(match.groups.lat_m) / 60;
			}

			if (longitude < 0) {
				longitude -= Number(match.groups.lon_m) / 60;
			} else {
				longitude += Number(match.groups.lon_m) / 60;
			}

			return [longitude, latitude];
		},
		format: (coordinates) => {
			return `${coordinates[1] - (coordinates[1] % 1)}° ${(
				Math.abs(coordinates[1] % 1) * 60
			).toFixed(5)}' ${coordinates[0] - (coordinates[0] % 1)}° ${(
				Math.abs(coordinates[0] % 1) * 60
			).toFixed(5)}'`;
		}
	},
	ddd: {
		label: 'WGS-84 Ddd',
		help: 'dd.dddddd dd.dddddd (lat long)',
		partial: /^(-|N|n|S|s)?(90|[0-8]?\d(\.\d+)?)°?/,
		full: /^(?<lat_h_p>(N|n|S|s)?)(?<lat_d>-?(90|[0-8]?\d(\.\d+)?))°?(?<lat_h>(N|n|S|s)?)(, *| +)(?<lon_h_p>(E|e|W|w)?)(?<lon_d>-?(180|(1[0-7]\d|\d?\d)(\.\d+)?))°?(?<lon_h>(E|e|W|w)?)$/,
		valid: (match) => {
			if (!match) {
				return false;
			}

			if (
				(Number(match.groups.lat_d) < 0 && (match.groups.lat_h_p || match.groups.lat_h)) ||
				(Number(match.groups.lon_d) < 0 && (match.groups.lon_h_p || match.groups.lon_h))
			) {
				return false;
			}

			return true;
		},
		parse: (match) => {
			let latitude = Number(match.groups.lat_d);
			let longitude = Number(match.groups.lon_d);

			const lat_h = match.groups.lat_h || match.groups.lat_h_p;
			const lon_h = match.groups.lon_h || match.groups.lon_h_p;

			if (lat_h === 'S' || lat_h === 's') {
				latitude *= -1;
			}
			if (lon_h === 'W' || lon_h === 'w') {
				longitude *= -1;
			}

			return [longitude, latitude];
		},
		projectionId: 'EPSG:4326'
		//projection: '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees',
		//projectionId: 'EPSG:4326'
	}
};

/**@typedef {object} CoordinateParseResult
 *
 * @property {CoordinateFormat} format The detected coordinate format
 * @property {boolean} valid Whether the string if a fully valid coordinate
 *   string
 * @property {[number, number]} [coordinates] The parsed coordinates (long, lat)
 *   in decimal degrees
 */

/**
 * Parse the coordinate string
 *
 * @param {string} coordinateString Coordinates string to parse
 *
 * @returns {CoordinateParseResult|null} null if no matching format found
 *   or the format details on match
 */
export const parseCoordinateString = (coordinateString) => {
	const formatArray = Object.entries(coordinateFormats);

	for (let i = 0; i < formatArray.length; i++) {
		const [formatId, format] = formatArray[i];

		if (coordinateString.match(format.partial)) {
			/**@type {CoordinateParseResult} */
			const result = {
				format,
				valid: false,
				coordinates: null
			};

			const match = coordinateString.match(format.full);
			if (match) {
				if (format.valid) {
					if (!format.valid(match)) {
						return result;
					}
				}

				let coordinates = format.parse
					? format.parse(match)
					: [Number(match.groups.first), Number(match.groups.second)];

				if (formatId !== 'ddd') {
					coordinates = proj4(format.projection || format.projectionId, 'EPSG:4326', coordinates);
				}

				// Round coordinates
				result.coordinates = [Number(coordinates[0].toFixed(6)), Number(coordinates[1].toFixed(6))];
				result.valid = true;
			}

			return result;
		}
	}

	return null;
};

/**
 * Format the given coordinates in the given format
 *
 * @param {[number, number]} coordinates The coordinates to format (long, lat)
 *   in decimal degrees
 * @param {string} formatId The format ID of the format to format the coordinates
 *   in
 *
 * @returns {string} The formatted coordinates
 */
export const formatCoordinates = (coordinates, formatId) => {
	if (!coordinateFormats[formatId]) {
		throw new Error('Unknown format ' + formatId);
	}

	const format = coordinateFormats[formatId];

	if (format.projectionId !== 'EPSG:4326') {
		coordinates = proj4('EPSG:4326', format.projection, coordinates);
	}

	if (format.format) {
		return format.format(coordinates);
	} else if (format.projectionId === 'EPSG:4326') {
		return coordinates[1].toFixed(6) + '° ' + coordinates[0].toFixed(6) + '°';
	}

	return coordinates.join(' ');
};
